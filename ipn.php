<?php
date_default_timezone_set("Europe/Copenhagen");
//TODO: Move this to settings file
$PAYPAL_EMAIL = 'rasmusgreve+mgamers@gmail.com';
$PAYPAL_MERCHANT_ID = 'DT8RMCFS9BLGS';
$PAYMENT_AMOUNT = 100;


// CONFIG: Enable debug mode. This means we'll log requests into 'ipn.log' in the same directory.
// Especially useful if you encounter network errors or other intermittent problems with IPN (validation).
// Set this to 0 once you go live or don't require logging.
define("DEBUG", 1);

// Set to 0 once you're ready to go live
define("USE_SANDBOX", 1);


define("LOG_FILE", "./ipn.log");
error_reporting(E_ALL);
set_error_handler("ipn_handler");
function ipn_handler($error_level,$error_message,$error_file,$error_line,$error_context){
	error_log(date('[Y-m-d H:i e] '). "PHP Error $error_level: $error_message in $error_file on line $error_line. Context: $error_context." . PHP_EOL, 3, LOG_FILE);
}


// Read POST data
// reading posted data directly from $_POST causes serialization
// issues with array data in POST. Reading raw POST data from input stream instead.
$raw_post_data = file_get_contents('php://input');
$raw_post_array = explode('&', $raw_post_data);
$myPost = array();
foreach ($raw_post_array as $keyval) {
	$keyval = explode ('=', $keyval);
	if (count($keyval) == 2)
		$myPost[$keyval[0]] = urldecode($keyval[1]);
}
// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-validate';
if(function_exists('get_magic_quotes_gpc')) {
	$get_magic_quotes_exists = true;
}
foreach ($myPost as $key => $value) {
	if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
		$value = urlencode(stripslashes($value));
	} else {
		$value = urlencode($value);
	}
	$req .= "&$key=$value";
}

// Post IPN data back to PayPal to validate the IPN data is genuine
// Without this step anyone can fake IPN data

if(USE_SANDBOX == true) {
	$paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
} else {
	$paypal_url = "https://www.paypal.com/cgi-bin/webscr";
}

$ch = curl_init($paypal_url);
if ($ch == FALSE) {
	return FALSE;
}

curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);

if(DEBUG == true) {
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
}

curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

$res = curl_exec($ch);
if (curl_errno($ch) != 0) // cURL error
	{
	if(DEBUG == true) {	
		error_log(date('[Y-m-d H:i e] '). "Can't connect to PayPal to validate IPN message: " . curl_error($ch) . PHP_EOL, 3, LOG_FILE);
	}
	curl_close($ch);
	exit;

} else {
		// Log the entire HTTP response if debug is switched on.
		if(DEBUG == true) {
			error_log(date('[Y-m-d H:i e] '). "HTTP request of validation request:". curl_getinfo($ch, CURLINFO_HEADER_OUT) ." for IPN payload: $req" . PHP_EOL, 3, LOG_FILE);
			error_log(date('[Y-m-d H:i e] '). "HTTP response of validation request: $res" . PHP_EOL, 3, LOG_FILE);

			// Split response headers and payload
			list($headers, $res) = explode("\r\n\r\n", $res, 2);
		}
		curl_close($ch);
}

// Inspect IPN validation result and act accordingly

if (strcmp ($res, "VERIFIED") == 0) {
	if ($_POST['receiver_email'] != $PAYPAL_EMAIL || $_POST['mc_gross'] != $PAYMENT_AMOUNT || $_POST['mc_currency'] != 'DKK'){
		if(DEBUG == true) {
			error_log(date('[Y-m-d H:i e] '). "IPN not accepted: Receiver: {$_POST['receiver_email']}, Amount: {$_POST['mc_gross']} {$_POST['mc_currency']}" . PHP_EOL, 3, LOG_FILE);
		}
		return;
	}
	if (!preg_match('/^\d+;[A-Z]\d{4}$/',$_POST['item_number'])){
		if(DEBUG == true) {
			error_log(date('[Y-m-d H:i e] '). "IPN not accepted; invalid identifier: {$_POST['item_number']}" . PHP_EOL, 3, LOG_FILE);
		}
		return;
	}
	
	$item_number = explode(';',$_POST['item_number']);
	
	$user = $item_number[0];
	$season = (substr($item_number[1],0,1) == 'S') ? 'spring' : 'fall' ;
	$year = substr($item_number[1],1);
	$payment_amount = $_POST['mc_gross'];
	$payment_status = $_POST['payment_status'];
	$payer_id = $_POST['payer_id'];
	$txn_id = $_POST['txn_id'];
	$ipn_time = date('Y-m-d H:i:s');
	$note = 'Status: ' . $_POST['payment_status'];
	
	require_once("./php/connectdb.php");
	$check_stmt = $db->prepare("SELECT * FROM `payments` WHERE `user` = :user AND `year` = :year AND `season` = :season LIMIT 1;");
	$check_stmt->bindParam(':user',$user,PDO::PARAM_INT);
	$check_stmt->bindParam(':year',$year,PDO::PARAM_INT);
	$check_stmt->bindParam(':season',$season,PDO::PARAM_STR);
	$check_stmt->execute();
	$check = $check_stmt->fetch();
	if ($check === false)
	{
		$stmt = $db->prepare("INSERT INTO `payments` (`user`,`year`,`season`,`paypal_payer_id`,`paypal_txn_id`,`paypal_status`,`amount`,`time`,`note`) VALUES (:user, :year, :season, :payer_id, :txn_id, :status, :amount, :time, :note);");
	}
	else
	{
		$stmt = $db->prepare("UPDATE `payments` SET `paypal_payer_id` = :payer_id, `paypal_txn_id` = :txn_id, `paypal_status` = :status, `amount` = :amount, `time` = :time, `note` = :note WHERE `user` = :user AND `year` = :year AND `season` = :season;");
	}
	$stmt->bindParam(':user',$user,PDO::PARAM_INT);
	$stmt->bindParam(':year',$year,PDO::PARAM_INT);
	$stmt->bindParam(':season',$season,PDO::PARAM_STR);
	$stmt->bindParam(':amount',$payment_amount,PDO::PARAM_INT);
	$stmt->bindParam(':status',$payment_status,PDO::PARAM_STR);
	$stmt->bindParam(':payer_id',$payer_id,PDO::PARAM_STR);
	$stmt->bindParam(':txn_id',$txn_id,PDO::PARAM_STR);
	$stmt->bindParam(':time',$ipn_time,PDO::PARAM_STR);
	$stmt->bindParam(':note',$note,PDO::PARAM_STR);
	$stmt->execute();
	
	if(DEBUG == true && $stmt->errorCode() != 0 ) {
		error_log(date('[Y-m-d H:i e] '). "PDO error code: " . $stmt->errorCode() . PHP_EOL, 3, LOG_FILE);
		$er = $stmt->errorInfo();
		error_log(date('[Y-m-d H:i e] '). "PDO error msg: " . $er[2] . PHP_EOL, 3, LOG_FILE);
	}
	
	if(DEBUG == true) {
		error_log(date('[Y-m-d H:i e] '). "Verified IPN: $req ". PHP_EOL, 3, LOG_FILE);
	}
} else if (strcmp ($res, "INVALID") == 0) {
	// log for manual investigation
	// Add business logic here which deals with invalid IPN messages
	file_put_contents('output.txt',"Invalid!: \r\n");
	if(DEBUG == true) {
		error_log(date('[Y-m-d H:i e] '). "Invalid IPN: $req" . PHP_EOL, 3, LOG_FILE);
	}
}

?>