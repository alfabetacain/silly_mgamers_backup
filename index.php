<?php
error_reporting(E_ALL);
session_start();

$_SESSION['user_type'] = (isset($_SESSION['user_type']) && ($_SESSION['user_type'] == 'normal' || $_SESSION['user_type'] == 'admin')) ? $_SESSION['user_type'] : 'none';
$_SESSION['user_id'] = (isset($_SESSION['user_id']) && ctype_digit($_SESSION['user_id'])) ? $_SESSION['user_id'] : -1;
$_SESSION['user_name'] = (isset($_SESSION['user_name'])) ? $_SESSION['user_name'] : '';

date_default_timezone_set("Europe/Copenhagen");
ob_start();
require_once("php/util.php");
require_once("php/connectdb.php");
include("php/script_loader.php");

if ($_SESSION['user_type'] == 'none' && isset($_COOKIE['remember_id']) && $_COOKIE['remember_id'] != '')
{
	header("Location: ./login.php?action=cookie_login");
	die();
}

ob_clean();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>MGamers</title>
    <!-- Bootstrap core CSS -->
	<!--<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">	
	<link rel="stylesheet" href="minified/themes/default.min.css" type="text/css" media="all" />

    <!-- Custom styles for this template -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
	
  </head>

  <body>
	<?php
		include("php/navbar.php");
		
		content();
		
		
		include("php/user_dialogs.php");
	?>
	
	
	
	<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="minified/jquery.sceditor.xhtml.min.js"></script>

	<?php
		javascript();
	?>
	<script type="text/javascript">
	if (window.location.hash == "#create_user_modal")
	{
		$('#create_user_modal').modal();
	}
	window.setTimeout(function() {
		$(".message-alert").fadeTo(600, 0).slideUp(600, function(){
			$(this).remove(); 
		});
	}, 8000);
	
	$(function() {
		$("textarea.wysiwyg").sceditor({
			plugins: "xhtml",
			emoticonsEnabled : false,
			width:"100%",
			toolbar: "bold,italic,underline|font,size,color|left,center,right|bulletlist,orderedlist|image,youtube|link,unlink|removeformat|maximize,source",
			style: "minified/jquery.sceditor.default.min.css"
		});
	});
	$(".loading-button").click(function(){
		$(this).html("<img src='img/ajax-loader.gif' />");
	});

	</script>
	
	<div id="preload">
		<img src='img/ajax-loader.gif' alt="Loading"/>
	</div>
  </body>
</html>
