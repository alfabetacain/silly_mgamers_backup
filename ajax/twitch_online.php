<?php
if (!isset($_GET['stream']))
	die("0");
$stream_name = $_GET['stream'];

function TwitchStreamAvailable($stream_name)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://api.twitch.tv/kraken/streams/$stream_name");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$response = curl_exec($ch);
	$json_exp = json_decode($response,true);
	$response = (isset($json_exp['stream']));
	curl_close($ch);
	return $response;
}

echo (TwitchStreamAvailable($stream_name) ? '1' : '0');