<?php
/*
	Search for users with a given name or nickname
	Parameters:
		search 				- A string for which to search. Searches in name and nickname. Perfect matches have precedens. Then substrings match case. Then substrings w/o case.
		exclude_event_id	- An integer event id. Users attending this event will not be included in the search results.


*/
session_start();
chdir("..");
$number_of_results = 5;
if ($_SESSION['user_type'] != 'admin'){
	header('HTTP/1.0 403 Forbidden');
	die("Not authorized!");
}
include('./php/connectdb.php');
include('./php/util.php');

$search = getGET('search');
if ($search == '') die("[]");

$exclude_event_id = getGET('exclude_event_id','/^\d+$/','');

$perfect_matches = array();
$substring_matches = array();
$substring_matches2 = array();
$levenstein_matches = array();
$levenstein_values = array();
if ($exclude_event_id  == '')
	$sm = $db->prepare("SELECT `id`, `name`, `nick` FROM `users` WHERE `active` = 1;");
else
{
	$sm = $db->prepare("SELECT u.`id`, u.`name`, u.`nick` FROM `users` u LEFT JOIN `reservations` r ON r.`user` = u.`id` AND r.`event` = :event WHERE r.`id` IS NULL AND u.`active` = 1;");
	$sm->bindParam(':event',$exclude_event_id,PDO::PARAM_INT);
}	
$sm->execute();
while ($rs = $sm->fetch(PDO::FETCH_ASSOC))
{
	if ($rs['name'] == $search || $rs['nick'] == $search){
		$perfect_matches[] = $rs;
	}
	elseif (strpos($rs['name'],$search) !== false || strpos($rs['nick'],$search) !== false){
		$substring_matches[] = $rs;
	}
	elseif (stripos($rs['name'],$search) !== false || stripos($rs['nick'],$search) !== false ){
		$substring_matches2[] = $rs;
	}
	else{
		
		if ($rs['nick'] == ''){
			$levenstein_values[] = levenshtein($search,$rs['name']);
			$rs['l'] = levenshtein($search,$rs['name']);
		}else{
			$levenstein_values[] = min(levenshtein($search,$rs['name']),levenshtein($search,$rs['nick']));
			$rs['l'] = min(levenshtein($search,$rs['name']),levenshtein($search,$rs['nick']));
		}
		$levenstein_matches[] = $rs;
	}
}
//TODO: Insert cheap. Replace expensive?
array_multisort($levenstein_values,$levenstein_matches);


$result = array();

foreach ($perfect_matches as $user)
	if (count($result) < $number_of_results)
		$result[] = $user;
	else
		break;
		
foreach ($substring_matches as $user)
	if (count($result) < $number_of_results)
		$result[] = $user;
	else
		break;

foreach ($substring_matches2 as $user)
	if (count($result) < $number_of_results)
		$result[] = $user;
	else
		break;

foreach ($levenstein_matches as $user)
	if (count($result) < $number_of_results)
		$result[] = $user;
	else
		break;
		
echo json_encode($result);
