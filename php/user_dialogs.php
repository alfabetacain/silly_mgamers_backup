<script type="text/javascript">
		 var RecaptchaOptions = {
			theme : 'red'
		 };
 </script>
 
<?php 
if ($_SESSION['user_type'] == 'none'){
	
	$query = explode('&', $_SERVER['QUERY_STRING']);
	foreach($query as $k => $part){
		$p = explode('=',$part);
		if ($p[0] == 'message')
			unset($query[$k]);
	}
	$_SESSION['login_return_query'] = implode('&',$query);
?>

<div class="modal fade" id='login_modal'>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Log ind</h4>
      </div>
	  <form method='post' action='./login.php'>
		<input type="hidden" name="action" value="login"/>
		  <div class="modal-body">
			  <div class="form-group">
				<label for="emailinput1">Email adresse</label>
				<input type="email" name="email" class="form-control" id="emailinput1" placeholder="Email adresse">
			  </div>
			  <div class="form-group">
				<label for="passwordinput">Adgangskode</label>
				<input type="password" name="password" class="form-control" id="passwordinput" placeholder="Adganskode">
			  </div>
			  <div class="checkbox">
				<label>
				  <input type="checkbox" name="remember"> Husk mig på denne computer
				  <a data-toggle="modal" href="#forgot_password_modal" class="btn btn-info pull-right">Glemt adgangskode</a>
				</label>
			  </div>
		  </div>
		  <div class="modal-footer">
			<a data-toggle="modal" href="#create_user_modal" class="btn btn-success pull-left">Opret ny bruger</a>
			<button type="button" class="btn btn-default" data-dismiss="modal">Annuller</button>
			<button type="submit" class="btn btn-primary loading-button">Log ind</button>
		  </div>
	  </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 
<?php 
	//Create user modal dialog 
?>
 <div class="modal fade" id='create_user_modal'>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Opret ny bruger</h4>
      </div>
	  <form method='post' action='./login.php'>
	  <input type="hidden" name="action" value="create"/>
      <div class="modal-body">
		<div class="form-horizontal">
		  <div class="form-group">
			<label for="nameinput" class="col-md-4 control-label">Fulde navn</label>
			<div class="col-md-8">
				<input type="text" name="name" class="form-control" id="nameinput" placeholder="Fulde navn" required value='<?=(isset($_GET['name']))?$_GET['name']:''?>'>
			</div>
		  </div>
		  <div class="form-group <?=(isset($_GET['error']) && $_GET['error'] == 1)?'has-error':''?>">
			<label for="emailinput2" class="col-md-4 control-label">Email adresse</label>
			<div class="col-md-8">
				<input type="email" name="email" class="form-control" id="emailinput2" placeholder="Email adresse" required value='<?=(isset($_GET['email']))?$_GET['email']:''?>'>
				<?=(isset($_GET['error']) && $_GET['error'] == 1)?'<br/><strong>Email adressen er allerede i brug!</strong>':''?>
			</div>
		  </div>
		  <div class="form-group <?=(isset($_GET['error']) && $_GET['error'] == 3)?'has-error':''?>">
			<label for="passwordinput1" class="col-md-4 control-label">Adgangskode</label>
			<div class="col-md-8">
				<input type="password" name="password1" class="form-control" id="passwordinput1" placeholder="Adganskode" required>
				<?=(isset($_GET['error']) && $_GET['error'] == 3)?'<br/><strong>Adgangskode skal være mindst 7 karakterer</strong>':''?>
			</div>
		  </div>
		  <div class="form-group <?=(isset($_GET['error']) && $_GET['error'] == 2)?'has-error':''?>">
			<label for="passwordinput2" class="col-md-4 control-label">Gentag kode</label>
			<div class="col-md-8">
				<input type="password" name="password2" class="form-control" id="passwordinput2" placeholder="Gentag adganskode" required>
				<?=(isset($_GET['error']) && $_GET['error'] == 2)?'<br/><strong>De to adgangskoder stemte ikke overens</strong>':''?>
			</div>
		  </div>
		  <div class="form-group">
			<label class="col-md-4">Bevis at du er et menneske</label>
			<div class="col-md-8">
				<script type="text/javascript" src="https://www.google.com/recaptcha/api/challenge?k=6LdIBOkSAAAAABw8cbzCEGwfsRecgy-QUSzhTJDK"></script>
				<noscript>
				 <iframe src="https://www.google.com/recaptcha/api/noscript?k=6LdIBOkSAAAAABw8cbzCEGwfsRecgy-QUSzhTJDK"
					 height="300" width="500" frameborder="0"></iframe><br>
				 <textarea name="recaptcha_challenge_field" rows="3" cols="40">
				 </textarea>
				 <input type="hidden" name="recaptcha_response_field" value="manual_challenge">
				</noscript>
				<?=(isset($_GET['error']) && $_GET['error'] == 'incorrect-captcha-sol')?'<br/><strong>Fejl i CAPTCHA. Prøv igen!</strong>':''?>
			</div>
		  </div>
		  <div class="form-group <?=(isset($_GET['error']) && $_GET['error'] == 4)?'has-error':''?>">
			<label for="passwordinput2" class="col-md-4 control-label">Vælg medlem</label>
			<div class="col-md-8">
				<select class='form-control' name='confirmation_user'>
					<option value='-1' selected>Vælg medlem</option>
					<?php
						$stmt = $db->prepare("SELECT `id`,`nick` FROM `users` WHERE NOT `nick` IS NULL AND `nick` <> '' ORDER BY `nick` ASC;");
						$stmt->execute();
						while ($res = $stmt->fetch()){
							?>
								<option value='<?=$res['id']?>'><?=$res['nick']?></option>
							<?php
						}
					?>
				
					
				</select>
				<?=(isset($_GET['error']) && $_GET['error'] == 4)?'<br/><strong>Vælg et medlem fra listen herover</strong>':''?>
				<span class="help-block">For at blive oprettet som medlem skal et eksisterende medlem bekræfte dig</span>
			</div>
			
		  </div>
		</div>
      </div>
	  
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Annuller</button>
		<button type="submit" class="btn btn-primary loading-button">Opret bruger</button>
      </div>
	  
	  </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 
<?php 
	//Forgot password modal dialog
?>
<div class="modal fade" id='forgot_password_modal'>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Glemt adgangskode</h4>
      </div>
	  <form method='post' action='./login.php'>
	  <input type="hidden" name="action" value="reset"/>
      <div class="modal-body">
		<p>Hvis du har glemt din adgangskode kan du få den nulstillet her</p>
		<p>Du vil modtage en midlertidig adgangskode på mail.</p>
		<p>Indtast din emailadresse herunder for at få nulstillet adgangskoden.</p>
		<p><strong>Husk at tjekke om mailen havner i dit spamfilter!</strong></p>
		<div class="form-horizontal">
		  <div class="form-group">
			<label for="emailinput3" class="col-md-4 control-label">Email adresse</label>
			<div class="col-md-8">
				<input type="email" name="email" class="form-control" id="emailinput3" placeholder="Email adresse" required>
			</div>
		  </div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Annuller</button>
		<button type="submit" class="btn btn-primary loading-button">Nulstil adgangskode</button>
      </div>

	  </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->  


<?php 
 }
?>
