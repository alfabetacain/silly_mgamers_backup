<?php
//Form submission etc.
if (isset($_POST['activate']) || isset($_POST['deactivate'])){
	if (isset($_POST['activate'])){
		$stmt = $db->prepare("UPDATE `users` SET `active` = 1 WHERE `id` = :id LIMIT 1;");
		$user = getPOST('activate','/^\d+$/',-1);
	}
	else{
		$stmt = $db->prepare("UPDATE `users` SET `active` = 0 WHERE `id` = :id LIMIT 1;");
		$user = getPOST('deactivate','/^\d+$/',-1);
	}
	$stmt->bindParam(':id',$user,PDO::PARAM_INT);
	$stmt->execute();
	header("Location: ./?show=members_admin");
	die("Update active state");
}
if (isset($_POST['delete'])){
	$stmt = $db->prepare("DELETE FROM `users` WHERE `id` = :id LIMIT 1;");
	$user = getPOST('delete','/^\d+$/',-1);
	$stmt->bindParam(':id',$user,PDO::PARAM_INT);
	$stmt->execute();
	header("Location: ./?show=members_admin&message=userDeleted");
	die("User deleted");
}


//Content
function content()
{
	global $db;
	
	?>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-body">
			<h1>Administrer medlemmer</h1>
			<form action="./?show=members_admin" method="post">
			<table class="table">
				<thead>
					<tr>
						<th></th>
						<th>Navn</th>
						<th>Nick</th>
						<th>Email</th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
	
	<?php
	$stmt = $db->prepare("SELECT * FROM `users` WHERE `active` = 1;");
	$stmt->execute();
	
	while ($res = $stmt->fetch())
		show_user_line($res);
	
	$stmt = $db->prepare("SELECT * FROM `users` WHERE `active` = 0;");
	$stmt->execute();
	
	if ($stmt->rowCount() > 0) {
		?> <tr><td colspan="7" class="text-center"><h3>Deaktiverede</h3></td></tr> <?php

		while ($res = $stmt->fetch())
			show_user_line($res);
	}
	?>
				</tbody>
			</table>
			</form>
		</div>
	</div>
</div>
	<?php 
	
?>


<?php
}

function show_user_line($res){
	?>
		<tr>
			<td><img src="https://gravatar.com/avatar/<?=md5(strtolower($res['email']))?>?s=32&amp;d=identicon" alt="<?=$res['name']?>'s avatar"></td>
			<td><a href="./?show=member&id=<?=$res['id']?>"><?=$res['name']?></a></td>
			<td><?=$res['nick']?></td>
			<td><?=$res['email']?></td>
			<td><a class="btn btn-xs btn-default" href="./?show=settings&amp;id=<?=$res['id']?>">Rediger</a></td>
			<td><a class="btn btn-xs btn-default" href="./?show=password&amp;id=<?=$res['id']?>">Skift adgangskode</a></td>
			<?php if ($res['active'] == 1){ ?>
				<td><button class="btn btn-xs btn-warning" name="deactivate" value="<?=$res['id']?>">Deaktiver</button></td>
			<?php } else { ?>
				<td><button class="btn btn-xs btn-success" name="activate" value="<?=$res['id']?>">Aktiver</button></td>
			<?php }?>
			<td><button class="btn btn-xs btn-danger" name="delete" value="<?=$res['id']?>" onclick="return confirm('Er du sikker på at du vil slette dette medlem?\nDette kan ikke fortrydes!')">Slet</button></td>
		</tr>
	<?php
}

//Javascript
function javascript(){
?>


<?php
}
?>
