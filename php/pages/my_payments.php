<?php
//Form submission etc.


?>


<?php
//Content
function content()
{
	global $db;
	$member_stmt = $db->prepare("SELECT * FROM `users`  WHERE `id` = :id ;");
	$member_stmt->bindParam(':id',$_SESSION['user_id'],PDO::PARAM_INT);
	$member_stmt->execute();
	$member = $member_stmt->fetch();
	
	$payment_stmt = $db->prepare("SELECT p.*, u.`name` recipient_name FROM `payments` p LEFT JOIN `users` u ON u.`id` = p.`paid_to_user` WHERE p.`user` = :user ORDER BY p.`year` ASC, p.`season` ASC;");
	$payment_stmt->bindParam(':user',$_SESSION['user_id'],PDO::PARAM_INT);
	$payment_stmt->execute();
	
	$payments = array();
	while ($res = $payment_stmt->fetch()){
		$payments[$res['year'].'_'.$res['season']] = $res;
	}
?>

<div class='container'>
	<div class='panel panel-default'>
		<div class='panel-body'>
			<h1>Mine indbetalinger</h1>
			<table class='table'>
				<thead>
					<tr><th>Periode</th><th>Beløb</th><th>Betalingsmetode</th><th>Betalings ID</th><th>Note</th></tr>
				</thead>
				<tbody>
					<?php
					foreach ($payments as $payment){
					?>
						<tr>
							<td>
							<?php
								if ($payment['season'] == 'spring')
									echo '<span class="label label-warning">' . ('Forår ' . $payment['year']) . '</span>';
								else
									echo '<span class="label label-danger">' . ('Efterår ' . $payment['year']) . '</span>';
							?>
							</td>
							<td><?=$payment['amount']?>,- kr</td>
							<td>
							<?php
								if ((isset($payment['paid_to_user']) && $payment['paid_to_user'] != null ))
									echo "Kontant";
								else
									echo "Paypal";
							?>
							</td>
							<td>
							<?php
								if ((isset($payment['paid_to_user']) && $payment['paid_to_user'] != null ))
									echo '#'.$payment['id'] . " (til <a href='./?show=member&amp;id={$payment['paid_to_user']}'>{$payment['recipient_name']}</a>)";
								else
									echo $payment['paypal_txn_id'] . ' / ' . $payment['paypal_payer_id'];
							?>
							</td>
							<td><?=$payment['note']?></td>
						</tr>
					<?php }
					if (count($payments) == 0) {
					?>
						<tr>
							<td colspan='6' class='text-center'>Ingen indbetalinger registreret</td>
						</tr>
					<?php
					}
					?>
				</tbody>
			</table>
			<h1>Manglende indbetalinger</h1>
			<form method="post" action="https://www.sandbox.paypal.com/cgi-bin/webscr" target="_blank">
				<input type="hidden" name="button" value="buynow">
				<input type="hidden" name="quantity" value="1">
				<input type="hidden" name="amount" value="100">
				<input type="hidden" name="currency_code" value="DKK">
				<input type="hidden" name="shipping" value="0">
				<input type="hidden" name="tax" value="0">
				<input type="hidden" name="no_shipping" value="1">
				<input type="hidden" name="rm" value="1">
				<input type="hidden" name="lc" value="DK">
				<input type="hidden" name="notify_url" value="http://mgamers.dk/ipn.php">
				<input type="hidden" name="env" value="www.sandbox">
				<input type="hidden" name="cmd" value="_xclick">
				<input type="hidden" name="business" value="rasmusgreve+mgamers@gmail.com"><?php // DT8RMCFS9BLGS ?>
				<input type="hidden" name="item_number" value="" id="item_number">
			
				<table class='table'>
					<thead>
						<tr><th>Periode</th><th>Pris</th><th></th></tr>
					</thead>
					<tbody>
						<?php
						$php_date = strtotime($member['registration_date']);
						$year = date('Y',$php_date);
						$season = season($php_date);
						$season_val = ($season == 'spring') ? 0 : 0.5;
						$cur_season_val = (season() == 'spring') ? 0 : 0.5;
						$missing = 0;
						
						while (($year + $season_val) <= (date('Y') + $cur_season_val))
						{
							$skip = false;
							if (!isset($payments[$year.'_'.$season])){
								if ($season == 'spring'){
									$season_name = 'Forår ' . $year;
									$season_id = 'S' . $year;
									$period = '<span class="label label-warning season_label" data-season="spring" data-year="'.$year.'">' . $season_name . '</span>';
								}
								else{
									$season_name = 'Efterår ' . $year;
									$season_id = 'F' . $year;
									$period = '<span class="label label-danger season_label" data-season="fall" data-year="'.$year.'">' . $season_name . '</span>';
								}
								$missing++;
						?>
							<tr>
								<td>
									<?=$period?>
								</td>
								
								<td>
									100,- kr
								</td>
								
								<td>
									<button name="item_name" 
											value="MGamers medlemskab <?=$season_name?> for <?=$member['name']?>" 
											onclick="$('#item_number').val('<?=($member['id'].';'.$season_id)?>');" 
											class="paypal-button large pull-right">Betal med PayPal</button>
								</td>
							</tr>
							<?php 
							}
							if ($season == 'spring'){$season = 'fall';}else{$season = 'spring';$year++;}
							$season_val = ($season == 'spring') ? 0 : 0.5;
						}
						if ($missing == 0) {
						?>
							<tr>
								<td colspan='2' class='text-center'>Ingen manglende indbetalinger</td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
			</form>
		</div>
	</div>
</div>



<?php
}
?>


<?php
//Javascript
function javascript(){
?>


<?php
}
?>