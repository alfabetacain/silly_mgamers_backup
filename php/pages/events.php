<?php
//Form submission etc.

?>


<?php
//Content
function content()
{
	global $db;
	
	//Select current event (if any)
	$current_event_stmt = $db->prepare("SELECT e.*, COUNT(r.`id`) as attendance, CONCAT(r2.`table`,'.',r2.`seat`) as seat 
										FROM `events` e 
										LEFT JOIN `reservations` r ON e.`id` = r.`event` 
										LEFT JOIN `reservations` r2 ON e.`id` = r2.`event` AND r2.`user` = :user
										WHERE e.`start` < NOW() AND e.`end` > NOW() 
										GROUP BY e.`id`");
	$current_event_stmt->bindParam(':user',$_SESSION['user_id'],PDO::PARAM_INT);
	$current_event_stmt->execute();
	
	$current_event = $current_event_stmt->fetch();
	
	if ($current_event != null)
		currentEvent($current_event);
	
	//Select next events
	$event_stmt = $db->prepare("SELECT e.*, COUNT(r.`id`) as attendance, CONCAT(r2.`table`,'.',r2.`seat`) as seat 
										FROM `events` e 
										LEFT JOIN `reservations` r ON e.`id` = r.`event` 
										LEFT JOIN `reservations` r2 ON e.`id` = r2.`event` AND r2.`user` = :user
										WHERE e.`start` > NOW()
										GROUP BY e.`id`
										ORDER BY e.`start` ASC");
	$event_stmt->bindParam(':user',$_SESSION['user_id'],PDO::PARAM_INT);
	$event_stmt->execute();
	
	nextEvents($event_stmt);
}

?>

<?php
function nextEvents($event_stmt){
?>
	<div class="container">
		<div class="well">
			<center><h1>Kommende arrangementer</h1></center>
			<?php
				while ($event = $event_stmt->fetch()){
					drawNextEvent($event);
				}
			?>
		</div>

		<div class="col-md-12 text-center">
			<a href="./?show=oldevents" class="btn btn-lg btn-default">Se tidligere arrangementer</a>
		</div>
	</div>

<?php
}
?>


<?php
function drawNextEvent($event){

?>

<div class='panel panel-default'>
	<div class='panel-body'>
		<div class="row">
			<div class="col-md-10">
					<h2 class="media-heading"><?=getDisplayDate($event['start'])?></h2>
				<div class="col-md-6">
					<dl class="dl-horizontal">
						<dt>Starter:</dt>
						<!--<dd><a href="http://www.google.com/calendar/event?action=TEMPLATE&text=MGamers&dates=20130928T150000Z/20130930T160000Z&details=&location=M%C3%A5l%C3%B8v%20skoles%20gymnastiksal&trp=true&sprop=&sprop=name:" target="_blank">28. september 2013 kl 17:00</a></dd>-->
						<dd><?=getDisplayDateTime($event['start'])?></a></dd>
						<dt>Slutter:</dt>
						<dd><?=getDisplayDateTime($event['end'])?></dd>
						<!--<dt>Tilmeldingsfrist:</dt>
						<dd>25. september 2013 kl 17:00</dd>-->
						<dt>Sted:</dt>
						<dd><?=$event['location']?></dd>
						<dt>Du er:</dt>
						<?php if ($_SESSION['user_type'] == 'none') { ?>
							<dd><span class="label label-danger">Ikke medlem :(</span></dd>
						<?php } elseif ($event['seat'] == null) { ?>
							<dd><span class="label label-warning">Ikke tilmeldt :(</span></dd>
						<?php } elseif ($event['seat'] == '-1.-1') { ?>
							<dd><span class="label label-success">Tilmeldt uden plads</span></dd>
						<?php } else { ?>
							<dd><span class="label label-success">Tilmeldt på plads <?=$event['seat']?></span></dd>
						<?php }?>
					</dl>	
				</div>
				<div class="col-md-6">
						<?php if ($_SESSION['user_type'] == 'none') { ?>
							<a href="./?show=event&amp;id=<?=$event['id']?>" class="btn btn-lg btn-info"><span class="glyphicon glyphicon-user"></span> Deltagerliste</a>
						<?php } elseif ($event['seat'] == null) { ?>
							<a href="./?show=event&amp;id=<?=$event['id']?>" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-calendar"></span> Tilmelding</a>
						<?php } else { ?>
							<a href="./?show=event&amp;id=<?=$event['id']?>" class="btn btn-lg btn-warning"><span class="glyphicon glyphicon-refresh"></span> Skift plads</a>
						<?php }?>
				</div>
			</div>
			<div class="toptilmeldt col-md-2 text-center">
				<span class="event_attendee_count"><?=$event['attendance']?></span>
				<p>tilmeldt</p>
			</div>
		</div>
	</div>
</div>
<img id='asgers_baggrunds_div' src='img/pacmanMi2.png' style='position:absolute;top:80px;left:0px;width:100%;opacity:0.6;'>
<?php }?>


<?php
function currentEvent($event){
?>

<div class="container">
		<div class="well">
	<center><h1>Arrangement i gang!</h1></center>

	<?php drawNextEvent($event); ?>

</div>
</div>
<?php
}
?>


<?php
//Javascript
function javascript(){
?>
<script type="text/javascript">
$('.twitch_btn').each(function(){
	var btn = $(this);
	btn.html("<img src='img/ajax-loader.gif'/>");
	$.getJSON("./twitch_online.php?stream=" + btn.data('streamid'), function(data) {
		btn.addClass("btn");
		btn.addClass("btn-xs");
		if (data == "1")
		{
			btn.html(btn.data('online'));
			btn.addClass("btn-primary");
		}
		else
		{
			btn.html(btn.data('offline'));
			btn.addClass("btn-default");
			btn.addClass("disabled");
		}
	});
});
</script>
<?php
}
?>