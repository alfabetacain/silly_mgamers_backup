<?php
//Form submission etc.
require_once('./php/connectdb.php');
require_once('./php/util.php');
if (isset($_POST['bbcode_field']))
{
	if ($db != null)
	{
		//TODO: Admin check
		//TODO: Sanitize input (e.g. remove/reject <script>'s and shizzle)
		$stm = $db->prepare("UPDATE `special_pages` SET content=:content, date=:date, user=:user WHERE filename='frontpage'");
		$stm->bindParam(':content', $content, PDO::PARAM_STR);
		$stm->bindParam(':date', $date, PDO::PARAM_STR);
		$stm->bindParam(':user', $user, PDO::PARAM_INT);
		$temp_content = $_POST['bbcode_field'];
		$array = validateWysiwygDom($_POST['bbcode_field']);
		if (!empty($array))
		{
			header("Location: ./?show=frontpage&message=invalidHTML");
		}
		else
		{
			$content = $_POST['bbcode_field'];
			date_default_timezone_set("Europe/Copenhagen");
			$date = date('Y-m-d H:i:s');
			$user = $_SESSION['user_id'];
			$stm->execute();
			if ($stm)
			{
				header("Location: ./?show=frontpage&message=frontpageUpdated");
			}
			else
			{
				header("Location: ./?show=frontpage&message=frontpageUpdatedFail");
			}
		}
	}
}

//Content

function content()
{
	if (isset($_GET['edit']) && $_SESSION['user_type']=="admin"){
		contentEdit();
	}
	elseif (isset($_GET['edit']) && $_SESSION['user_type']!="admin"){
		contentDenied();
	}
	else{
		contentDisplay();
	}
}

function contentEdit()
{
?>
	<div class="col-md-10 col-md-offset-1">
	<form action="./" method="post" class="inlineForm">
			<p class="whiteText">Indhold:</p>
			<textarea name="bbcode_field" class="wysiwyg normalwysiwyg">
			<?php 	
				$statement = $GLOBALS['db']->prepare("SELECT * FROM `special_pages` INNER JOIN `users` ON special_pages.user=users.id AND filename='frontpage'");
				$statement->execute();
				$result = $statement->fetch((PDO::FETCH_ASSOC)); 
				echo $result['content'];
			?>
			</textarea>
			<br>
			<input type="submit" class="btn btn-default" value="GEM"></input>
	</form>
	<a href="./">
	<button class="btn btn-default">AFBRYD</button></a>
	</div>
<?php
}

function contentDenied(){
?>
	<div class="container">
		<div class="permissionDenied">
			<h1>You do not have the necessary permission(s) to edit this page</h1><br>
			<h1>ACCESS DENIED</h1>
		</div>
	</div>
<?php
}

function contentDisplay(){
?>

<div id = "myCarousel" class="carousel slide" data-ride="carousel" data-interval="8000"> 
	<ol class = "carousel-indicators">
		<li data-target = "#myCarousel" data-slide-to="0" class="active"> </li>
		<li data-target = "#myCarousel" data-slide-to="1"> </li>
		<li data-target = "#myCarousel" data-slide-to="2"> </li>
		<li data-target = "#myCarousel" data-slide-to="3"> </li>
		<li data-target = "#myCarousel" data-slide-to="4"> </li>
	</ol>
	<div class="carousel-inner">
		<div class = "item active">
			<a href="https://www.mgamers.dk/?show=news&category=10"><img src="img/Battlefield-4.png" class="img-responsive"></a>
		</div>
		<div class="item">
			<a href="https://www.wildstar-online.com/uk/"><img src="img/Wildstar1.png" alt="Dog" class="img-responsive"/></a>
	        		</div>
		<div class="item">
			<a href="https://www.youtube.com/watch?v=5i6Zw9E7U3M"><img src="img/Wildstar2.png" alt="Witcher" class="img-responsive"/></a>
		</div>
		<div class="item">
			<a href="https://www.wildstar-online.com/uk/game/features/warplots/"><img src="img/Wildstar3.png" alt="STAR" class="img-responsive"/></a>
			</div>
		<div class="item">
			<a href="https://www.playgames.dk/wildstar-inkl-dags-headstart-p-1205.html"><img src="img/Wildstar4.png" alt="LCS" class="img-responsive"/></a>
		</div>
	</div>
	<a class="carousel-control left" href="#myCarousel" data-slide="prev"><span class= "icon-prev"></span></a>
	<a class="carousel-control right" href="#myCarousel" data-slide="next"><span class= "icon-next"></span></a>
</div>

<img id='asgers_baggrunds_div' src='img/pacmanMGslide.png' style='position:absolute;top:600px;left:0px;width:100%;opacity:0.6;'>
<img id='asgers_baggrunds_div_fade' src='img/pacmanMGslide.png' style='position:absolute;top:600px;left:0px;width:100%;opacity:0;'>


<br>
<div class="container">

	<div class='row'>
		<div class='col-md-9'>
		 <div class='panel panel-default'>
		 <div class='panel-body'>
			<?php if (!(isset($_GET['edit'])) && $_SESSION['user_type'] == 'admin') { ?>
				<a href="./?frontpage&amp;edit" class="btn btn-primary">Redigér forside</a><br>
			<?php } ?>
			<?php
				$stm = $GLOBALS['db']->prepare("SELECT * FROM `special_pages` INNER JOIN `users` ON special_pages.user=users.id AND filename='frontpage'");
				$stm->execute();
				$rs = $stm->fetch((PDO::FETCH_ASSOC));
				if (!$rs)
				{
					echo "no results returned";
				}
					echo $rs['content'] . "<br>";
				if ($_SESSION['user_type'] == 'admin'){
					echo "<br> <p><small>Sidst redigeret: " . $rs['date'] . " <br>Af: " . $rs['name'] . "</small></p>"; 
				}	
					echo "<hr>";
					echo "<br>";

			?>	
		 </div>
		 </div>
		</div>
	
<div class='col-md-3'>
	
<a href="./?show=tournaments">
	<div class="rubrikker">
	    <div class=' panel-default'>
				<center><h2>Turneringer</h2></center>

	    </div>
    </div>
</a>

<a href="./?show=pictures">
	<div class="rubrikker">
		<div class=' panel-default'>

				<center><h2>Billeder</h2></center>
	      </div>
    </div>
</a>

<a href="./?show=games">
	<div class="rubrikker">
		<div class=' panel-default'>

				<center><h2>Spil</h2></center>
	    </div>
    </div>
</a>

<a href="./?show=contact">
	<div class="rubrikker">
		<div class=' panel-default'>

				<center><h2>Info</h2></center>
	    </div>
	</div>
</a>
        </div>
    </div>

</div>
<?php
}
?>


<?php
//Javascript
function javascript(){
?>
<script type="text/javascript">
	$('#myCarousel').on('slid.bs.carousel', function (e) {
		var background = "";
		switch($(".active", e.target).index())
		{
		case 0: //BF4
			background = "pacmanMGslide.png";
			break;
		case 1: //Witcher
			background = "pacmanTheDivision.png";
			break;
		case 2: //watch dogs
			background = "pacmanHeartstone-slide.png";
			break;
		case 3: //LOL
			background = "pacmanTitanFall.png";
			break;
		case 4: //Star citizen
			background = "pacmanThiefSlide.png";
			break;
		default:
			background = "pacmanThiefSlide.png";
			
		}
		var f = $('#asgers_baggrunds_div_fade');
		var b = $('#asgers_baggrunds_div');
		f.attr('src',b.attr('src'));
		f.fadeTo(0,0.6);
		b.fadeTo(0,0);
		b.attr('src',"img/" + background + "");
		b.fadeTo(1000,0.6);
		f.fadeTo(1000,0);
	})

</script>
<?php
}
?>

