<?php
//Form submission etc.


?>


<?php
//Content
function content()
{
	error_reporting(E_ALL);
	global $db;
	
	$sort = getGET('sort',array('name','nick'),'name');
	
	$search = getGET('search');
	
	if ($search == '')
		$users_stmt = $db->prepare("SELECT * FROM `users` u WHERE `active` = 1 ORDER BY `$sort` ASC;");
	else
	{
		$sql_search = str_replace(array('%','_'),array('\%','\_'),$search);
		$sql_search = "%$sql_search%";
		$users_stmt = $db->prepare("SELECT * FROM `users` WHERE (`name` LIKE :search OR `nick` LIKE :search) AND `active` = 1 ORDER BY `$sort` ASC;");
		$users_stmt->bindParam(':search',$sql_search,PDO::PARAM_STR);
	}
	
	$users_stmt->execute();

?>
<div class="container">
	<div class='row'>
		<div class='col-md-12'>
			<div class='panel panel-default'>
				<div class='panel-body'>
					<div class="row">
						<div class="col-md-8">
							<h1>Medlemmer</h1>
						</div>
						<div class="col-md-4">
							<form action="./" method="get">
								<input type="hidden" name="show" value="members"/>
								<div class="input-group">
								  <input type="search" class="form-control" name="search" value="<?=$search?>" id='search_input'>
								  <span class="input-group-btn">
									<input type="submit" value="Søg!" class="btn btn-default" />
								  </span>
								  <!--<input type="submit" onclick='$("#search_input").val("");' value="X" class="btn btn-default" />-->
								</div>
								<br>
								<strong>Sortering:</strong>
								<div class='btn-group'>
									<button name='sort' value='name' class='btn btn-default btn-sm <?php if ($sort == 'name') echo 'active'; ?> '>Navn</button>
									<button name='sort' value='nick' class='btn btn-default btn-sm <?php if ($sort == 'nick') echo 'active'; ?> '>Nick</button>
								</div>
							</form>
						</div>
					</div>
					<hr/>
					<?php
					while ($rs = $users_stmt->fetch()){
						displayUser($rs);
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<img id='asgers_baggrunds_div' src='img/pacmanMi3.png' style='position:absolute;top:80px;left:0px;width:100%;opacity:0.6;'>
<?php
}
?>

<?php
function displayUser($rs){
?>
<div class="col-sm-4">
	<div class="well user_well" href="./?show=member&amp;id=<?=$rs['id']?>">
		<div class="row">
			<div class="col-md-4">
				<div class="thumbnail">
					<?php if ($rs['avatar'] != null) { ?>
						<img src='uploads/avatars/<?=$rs['avatar']?>'  alt="<?=$rs['name']?>'s avatar" />
					<?php } else { ?>
						<img src="https://gravatar.com/avatar/<?=md5(strtolower($rs['email']))?>?s=200&amp;d=identicon" alt="<?=$rs['name']?>'s avatar">
					<?php } ?>
				</div>
			</div>
			<div class="col-md-8">
				<div class='no_overflow'><strong><?=$rs['nick']?>&nbsp;</strong></div>
				<?php if ($_SESSION['user_type'] != 'none') {?><div class='no_overflow'><?=$rs['name']?>&nbsp;</div><?php }?>
				<div>
					<a href="./?show=member&amp;id=<?=$rs['id']?>" class="btn btn-info btn-sm">Vis</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php } ?>


<?php
//Javascript
function javascript(){
?>
<script type="text/javascript">
jQuery(document).ready(function($) {
	  $(".user_well").click(function() {
			window.document.location = $(this).attr("href");
	  });
});
</script>
<?php
}
?>
