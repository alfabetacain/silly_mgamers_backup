<?php
error_reporting(E_ALL);
if(isset($_POST['id']) && ($_POST['id'] == $_SESSION['user_id'] || $_SESSION['user_type'] == 'admin'))
{
	$stmt = $db->prepare("UPDATE `users` SET `name` = :name, `email` = :email, `privacy` = :privacy, `must_pay` = :must_pay, `phone` = :phone, `address` = :address, `type` = :type, `zip` = :zip, `nick` = :nick, `battlelog` = :battlelog, `skype` = :skype, `steam` = :steam, `teamspeak` = :teamspeak, `origin` = :origin WHERE `id` = :id LIMIT 1;");
	$stmt->bindParam(':id',htmlspecialchars(getPOST('id','/^\d+$/',-1)),PDO::PARAM_INT);
	$stmt->bindParam(':name',htmlspecialchars(getPOST('name'),ENT_NOQUOTES),PDO::PARAM_STR);
	$stmt->bindParam(':privacy',htmlspecialchars(getPOST('privacy',array('on','off'),'off'),ENT_NOQUOTES),PDO::PARAM_STR);
	$stmt->bindParam(':email',htmlspecialchars(getPOST('email'),ENT_NOQUOTES),PDO::PARAM_STR);
	$stmt->bindParam(':phone',htmlspecialchars(getPOST('phone',$php_phone_regex),ENT_NOQUOTES),PDO::PARAM_STR);
	$stmt->bindParam(':address',htmlspecialchars(getPOST('address'),ENT_NOQUOTES),PDO::PARAM_STR);
	$stmt->bindParam(':zip',htmlspecialchars(getPOST('zip','/[1-9][0-9]{3}/')),PDO::PARAM_STR);
	$stmt->bindParam(':nick',htmlspecialchars(getPOST('nick'),ENT_NOQUOTES),PDO::PARAM_STR);
	$stmt->bindParam(':battlelog',htmlspecialchars(getPOST('battlelog'),ENT_NOQUOTES),PDO::PARAM_STR);
	$stmt->bindParam(':skype',htmlspecialchars(getPOST('skype'),ENT_NOQUOTES),PDO::PARAM_STR);
	$stmt->bindParam(':steam',htmlspecialchars(getPOST('steam'),ENT_NOQUOTES),PDO::PARAM_STR);
	$stmt->bindParam(':teamspeak',htmlspecialchars(getPOST('teamspeak'),ENT_NOQUOTES),PDO::PARAM_STR);
	$stmt->bindParam(':origin',htmlspecialchars(getPOST('origin'),ENT_NOQUOTES),PDO::PARAM_STR);
	
	$type = 'normal';
	$must_pay = '1';
	if ($_SESSION['user_type'] == 'admin') {
		$type = getPOST('type',array('normal','admin'),'normal');
		$must_pay = getPOST('must_pay',array('0','1'),'1');
	}
	$stmt->bindParam(':type',$type,PDO::PARAM_STR);
	$stmt->bindParam(':must_pay',$must_pay,PDO::PARAM_INT);
	
	$stmt->execute();
	
	if ($_POST['id'] == $_SESSION['user_id'])
		$_SESSION['user_name'] = htmlspecialchars(getPOST('name'));
	
	
	header("Location: ./?show=settings&id=".getPOST('id','/^\d+$/',-1)."&message=settingsok");
	die("Settings changed");
}

?>


<?php
//Content
function content()
{
	global $db;
	global $html_phone_regex;
	$stmt = $db->prepare("SELECT * FROM `users` WHERE `id` = :id LIMIT 1");
	
	$id = $_SESSION['user_id'];
	if ($_SESSION['user_type'] == 'admin') //If admin, allow use of id param in get
		$id = getGET('id','/^\d+$/',$id);
	
	$stmt->bindParam(':id',$id,PDO::PARAM_INT);
	$stmt->execute();
	$rs = $stmt->fetch();

?>
<div class="container">
	<div class='panel panel-default'>
		<div class='panel-body'>
			<h1>Rediger <?php if ($id != $_SESSION['user_id']) echo "<em>{$rs['name']}</em>'s"; ?> profil</h1>
			<form action="./?show=settings" method="post">
				<input type="hidden" name="id" value="<?=$id?>"/>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="nameinput">Navn</label>
							<input type="text" name="name" class="form-control" id="nameinput" required value='<?=$rs['name']?>'>
						</div>
						
						<div class="form-group">
							<label for="emailinput">E-mail</label>
							<input type="text" name="email" class="form-control" id="emailinput" required value='<?=$rs['email']?>'>
						</div>
						<div class="form-group">
							<label for="phoneinput">Mobiltelefon</label>
							<input type="phone" pattern="<?=$html_phone_regex?>" title="8 tal, ingen mellemrum" name="phone" class="form-control" id="phoneinput" value='<?=$rs['phone']?>'>
						</div>
						<div class="form-group">
							<label for="addressinput">Adresse</label>
							<input type="text" name="address" class="form-control" id="addressinput" value='<?=$rs['address']?>'>
						</div>
						<div class="form-group">
							<label for="zipinput">Postnummer</label>
							<input type="text" name="zip" pattern="[1-9][0-9]{3}" title="4 tal, ingen mellemrum" class="form-control" id="zipinput" value='<?=$rs['zip']?>'>
						</div>
						<div class="form-group">
								<label>Privatliv (tlf. nummer og adresse)</label>
								<label class="radio-inline">
									<input type="radio" name="privacy" value="on" <?=$rs['privacy'] == 'on' ? 'checked' : ''?>> Skjul
								</label>
								<label class="radio-inline">
									<input type="radio" name="privacy" value="off" <?=$rs['privacy'] == 'off' ? 'checked' : ''?>> Vis
								</label>
							</div>
						<?php if ($_SESSION['user_type'] == 'admin') { ?>
							<div class="form-group">
								<label>Brugertype: </label>
								<label class="radio-inline">
									<input type="radio" name="type" value="normal" <?=$rs['type'] == 'normal' ? 'checked' : ''?>> Normal
								</label>
								<label class="radio-inline">
									<input type="radio" name="type" value="admin" <?=$rs['type'] == 'admin' ? 'checked' : ''?>> Admin
								</label>
							</div>
							<div class="form-group">
								<label>Betalende medlem: </label>
								<label class="radio-inline">
									<input type="radio" name="must_pay" value="1" <?=$rs['must_pay'] == '1' ? 'checked' : ''?>> Ja
								</label>
								<label class="radio-inline">
									<input type="radio" name="must_pay" value="0" <?=$rs['must_pay'] == '0' ? 'checked' : ''?>> Nej
								</label>
							</div>
						<?php } ?>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="nickinput">Nick</label>
							<input type="text" name="nick" class="form-control" id="nickinput" value='<?=$rs['nick']?>'>
						</div>
						<div class="form-group">
							<label for="battleloginput">Battlelog</label>
							<input type="text" name="battlelog" class="form-control" id="battleloginput" value='<?=$rs['battlelog']?>'>
						</div>
						<div class="form-group">
							<label for="skypeinput">Skype</label>
							<input type="text" name="skype" class="form-control" id="skypeinput" value='<?=$rs['skype']?>'>
						</div>
						<div class="form-group">
							<label for="skypeinput">Steam</label>
							<input type="text" name="steam" class="form-control" id="skypeinput" value='<?=$rs['steam']?>'>
						</div>
						<div class="form-group">
							<label for="origininput">Origin</label>
							<input type="text" name="origin" class="form-control" id="origininput" value='<?=$rs['origin']?>'>
						</div>
						<div class="form-group">
							<label for="teamspeakinput">Teamspeak</label>
							<input type="text" name="teamspeak" class="form-control" id="teamspeakinput" value='<?=$rs['teamspeak']?>'>
						</div>
						<div class="form-group">
							<label for="avatar_btn">Avatar</label>
							<a href='#' onclick='window.open("./uploads",""	,"width=500,height=600,status=no,scrollbar=no,menubar=no,titlebar=no,toolbar=no")' class='btn btn-primary'>Vælg nyt</a>
						</div>
					</div>
				</div>
				<input type="submit" class="btn btn-primary" value="Gem"/>
			</form>
		</div>
	</div>
</div>

<?php
}
?>


<?php
//Javascript
function javascript(){
?>


<?php
}
?>