<?php
//Form submission etc.
$GLOBALS['limit'] = 0;
if (!isset($_GET['limit']))
{
	$GLOBALS['limit'] = 10;
}
else 
{
	$GLOBALS['limit'] += $_GET['limit'];
}

require_once('./php/connectdb.php');
require_once('./php/util.php');

if (isset($_POST['content']) && isset($_GET['newsID']))
{
//If a single newsstory is ready to be updated
	if ($GLOBALS['db'] != null)
	{
		//TODO: Admin check
		//TODO: Sanitize input (e.g. remove/reject <script>'s and shizzle)
		$stm = $GLOBALS['db']->prepare("UPDATE `news` SET `title`=:title, `created`=:date, `author`=:author, `content`=:content, `category`=:category WHERE `id`=:newsID");
		$stm->bindParam(':date', $date, PDO::PARAM_STR);
		$stm->bindParam(':author', $author, PDO::PARAM_INT);
		$stm->bindParam(':title', $title, PDO::PARAM_STR);
		$stm->bindParam(':content', $content, PDO::PARAM_STR);
		$stm->bindParam(':newsID', $newsID, PDO::PARAM_INT);
		$stm->bindParam(':category', $category, PDO::PARAM_INT);

		date_default_timezone_set("Europe/Copenhagen");
		$date = date('Y-m-d H:i:s');
		$array = validateWysiwygDom($_POST['content']);
		if (!empty($array))
		{
			header("Location: ./?show=news&newsID=$newsID&message=invalidHTML");
		}
		else
		{
			$content = $_POST['content'];
			$title = strip_tags($_POST['title']);
			$author = $_SESSION['user_id'];
			$newsID = -1;
			if (ctype_digit($_GET['newsID']))
			{
				$newsID = $_GET['newsID'];
			}
			$category = strip_tags($_POST['category']);
			$stm->execute();
			if ($stm)
			{
				header("Location: ./?show=news&newsID=$newsID&message=newsEdited");
			}
			else
			{
				header("Location: ./?show=news&newsID=$newsID&message=newsEditedFail");
			}
		}
	}
}
elseif (isset($_POST['content']))
{
//If a new newsstory is ready to be inserted
		//TODO: Admin check
		//TODO: Sanitize input (e.g. remove/reject <script>'s and shizzle)
		$stm = $db->prepare("INSERT INTO `news` (`created`, `author`, `title`, `content`, `category`) VALUES (:date, :author, :title, :content, :category)");
		$stm->bindParam(':date', $date, PDO::PARAM_STR);
		$stm->bindParam(':author', $author, PDO::PARAM_INT);
		$stm->bindParam(':title', $title, PDO::PARAM_STR);
		$stm->bindParam(':content', $content, PDO::PARAM_STR);
		$stm->bindParam(':category', $category, PDO::PARAM_INT);
		
		date_default_timezone_set("Europe/Copenhagen");
		$date = date('Y-m-d H:i:s');
		$array = validateWysiwygDom($_POST['content']);
		if (!empty($array))
		{
			header("Location: ./?show=news&message=invalidHTML");
		}
		else
		{
			$content = $_POST['content'];
			$title = strip_tags($_POST['title']);
			$author = $_SESSION['user_id'];
			$category = strip_tags($_POST['category']);
			$rs = $stm->execute();
			if ($rs)
			{
				header("Location: ./?show=news&message=newsCreated");
			}
			else
			{
				header("Location: ./?show=news&message=newsCreatedFail");
			}
		}

		
}
elseif (isset($_POST['deleteNewsAction']))
{
	$stm = $GLOBALS['db']->prepare("DELETE FROM `news` WHERE id=:newsID");
	$stm->bindParam('newsID', $newsID, PDO::PARAM_INT);
	$newsID = -1;
	if (ctype_digit($_POST['newsID']))
	{
		$newsID = $_POST['newsID'];
	}
	$stm->execute();
	if ($stm)
	{
		header("Location: ./?show=news&message=newsDeleted");
	}
	else
	{
		header("Location: ./?show=news&message=newsDeletedFail");
	}
}
elseif (isset($_POST['createCategoryAction']))
{
	$stm = $GLOBALS['db']->prepare("INSERT INTO `category` (`name`) VALUES (:name)");
	$stm->bindParam(':name', $name, PDO::PARAM_STR);
	$array = validateWysiwygDom($_POST['name']);
	$name = strip_tags($_POST['name']);
	$stm->execute();
	if ($stm)
	{
		header("Location: ./?show=news&categories&message=categoryCreated");
	}
	else
	{
		header("Location: ./?show=news&categories&message=categoryCreatedFail");
	}
}
elseif (isset($_POST['editCategoryAction']))
{
	$stm = $GLOBALS['db']->prepare("UPDATE `category` SET `name`=:name WHERE `id`=:id");
	$stm->bindParam(':name', $name, PDO::PARAM_STR);
	$stm->bindParam(':id', $id, PDO::PARAM_INT);
	$name = strip_tags($_POST['name']);
	$id = -1;
	if (ctype_digit($_POST['id']))
	{
		$id = $_POST['id'];
	}
	$stm->execute();
	if ($stm)
	{
		header("Location: ./?show=news&categories&message=categoryEdited");
	}
	else
	{
		header("Location: ./?show=news&categories&message=categoryEditedFail");
	}
}
elseif (isset($_POST['deleteCategoryAction']))
{
	$stm = $GLOBALS['db']->prepare("DELETE FROM `category` WHERE `id`=:id AND :id>1");
	$stm->bindParam(':id', $id, PDO::PARAM_INT);
	$id = -1;
	if (ctype_digit($_POST['id']))
	{
		$id = $_POST['id'];
	}
	$stm->execute();
	if ($stm)
	{
		header("Location: ./?show=news&categories&message=categoryDeleted");
	}
	else
	{
		header("Location: ./?show=news&categories&message=categoryDeletedFail");
	}
}

?>


<?php
//Content
function content()
{

	if (isset($_GET['create']) && ($_SESSION['user_type'] == 'admin' || $_SESSION['user_type'] == 'normal'))
	{
		createNewNewsstory();
	}
	elseif (isset($_GET['newsID']))
	{
		$stm = $GLOBALS['db']->prepare("SELECT `author` FROM `news` WHERE id=:newsID LIMIT 1");
		//Sanitize input
		$stm->bindParam(':newsID', $newsID, PDO::PARAM_INT);
		$newsID = -1;
		if (ctype_digit($_GET['newsID']))
		{
			$newsID = $_GET['newsID'];
		}
		//Needs to be sanitized!
		$stm->execute();
		$rs = $stm->fetch(PDO::FETCH_ASSOC);
		if (isset($_GET['edit']) && ($_SESSION['user_type'] == 'admin' || $_SESSION['user_id'] == $rs['author']))
		{
			editOneNewsstory();
		}
		else
		{
			displaySingleNewsstory();
		}
	}
	elseif (isset($_GET['category']))
	{
		if (isset($_GET['edit']) && $_SESSION['user_type'] == 'admin')
		{
			displayEditCategory();
		}
		else
		{
			displayCategoryNews();
		}
	}
	elseif (isset($_GET['categories']))
	{
		overviewCategories();
	}
	elseif (isset($_GET['createCategory']) && $_SESSION['user_type'] == "admin")
	{
		displayCreateCategory();
	}
	else
	{
		displayNews();
	}
}
?>

<?php

function createNewNewsstory()
{
?>
	<div class="container">
	<form action="./?show=news" method="post">
		<div>
			<p class="whiteText">Titel:</p>
			<input type="text" name="title" size="70" value="Titel"></input>
		</div>
		<div>
			<br>
			<p class="whiteText">Indhold:</p>
			<textarea name="content" class="wysiwyg smallwysiwyg">Indhold</textarea>
		</div>
			<div>
				<p class="whiteText">Kategori:</p>
				<select required name="category">
					<?php
					$stm = $GLOBALS['db']->prepare("SELECT DISTINCT * FROM `category`");
					$stm->execute();
					while ($rs = $stm->fetch(PDO::FETCH_ASSOC))
					{
					?>
					<option value=<?php echo "\"" .  $rs['id'] . "\"";
					if (isset($_GET['category']) && $_GET['category'] == $rs['id'])
					{
						echo " selected";
					}
					?>><?php echo $rs['name']; ?></option>
					<?php
					}
					?>
				</select>
			</div>
			<div>
			<br>
			<button type="submit" class="btn btn-default">GEM</button>
			</form>
				<a href="<?php echo "./?show=news"; ?>" class="btn btn-default">AFBRYD</a>
			</div>
	</div>
<?php
}

function editOneNewsstory()
{
$stm = $GLOBALS['db']->prepare("SELECT * FROM `news` WHERE id=:newsID");
$stm->bindParam(':newsID', $newsID, PDO::PARAM_INT);
$newsID = -1;
if (ctype_digit($_GET['newsID']))
{
	$newsID = $_GET['newsID'];
}
$stm->execute();
$rs = $stm->fetch(PDO::FETCH_ASSOC);
$category = $rs['category'];
?>
	<div class="container">
		<form action=<?php echo "./?show=news&amp;newsID=" . $newsID;?> method="post">
			<div>
				<p class="whiteText">Titel:</p>
				<input type="text" name="title" value=<?php echo "\"" . $rs["title"] . "\""; ?> size="70"></input>
			</div>
			<div>
				<br>
				<p class="whiteText">Indhold:</p>
				<textarea name="content" class="wysiwyg smallwysiwyg"><?php echo $rs['content']; ?></textarea>
			</div>
			<div>
				<p class="whiteText">Kategori:</p>
				<select required name="category">
				<?php
				$stm = $GLOBALS['db']->prepare("SELECT DISTINCT * FROM `category`");
				$stm->execute();
				while ($rs = $stm->fetch(PDO::FETCH_ASSOC))
				{
				?>
				<option value=<?php echo "\"" . $rs['id'] . "\"";
				if ($category == $rs['id'])
				{
					echo " selected";
				}
				?>><?php echo $rs['name']; ?></option>
				<?php
				}
				?>
				</select>
			</div>
			<div>
				<br>
				<button type="submit" class="btn btn-default">GEM</button>
		</form>
				<a href="<?php echo "./?show=news"; ?>" class="btn btn-default">AFBRYD</a>
				<form class="inlineForm" action=
				<?php
				$outputString = "./?show=news";
				if ($category > 1)
				{
					$outputString = $outputString . "&amp;category=" . $category;
				}
				echo $outputString;
				?> method="post">
				<input type="hidden" name="deleteNewsAction" value="delete"></input>
				<input type="hidden" name="newsID" value=<?php echo $newsID; ?>></input>
				<?php echo $rs['id']; ?>
				<input type="submit" value="SLET" class="btn btn-default"></input>
				</form>
			</div>
	</div>

<?php
}

function displaySingleNewsstory()
{
$stm = $GLOBALS['db']->prepare("SELECT news.title, news.content, news.author, news.created, news.id AS `newsID`, users.name, category.name AS categoryName FROM `news` INNER JOIN `users` ON news.id=:newsID AND news.author=users.id INNER JOIN `category` ON category.id=news.category LIMIT 1");
$stm->bindParam(':newsID', $newsID, PDO::PARAM_INT);
$newsID = 0;
if (ctype_digit($_GET['newsID']))
{
	$newsID = $_GET['newsID'];
}
$stm->execute();

if ($rs = $stm->fetch(PDO::FETCH_ASSOC))
{
?>
<div class="col-md-6 col-md-offset-3">
	<div class='panel panel-default'>
			<div class='panel-body'>
				<div class="media">
				  <div class="media-body">
					<h2 class="media-heading"><?php echo $rs['title'] ?></h2>
					<?php echo "<p>" . $rs['content'] . "</p>"; ?>
				  </div>
				</div>
			</div>
			<div class='panel-footer'>
				<?php echo "<p> " . $rs['name'] . " " . $rs['created'] . "</p>"; ?>
				<p><?php echo "Kategori: " . $rs['categoryName']; ?></p>
				<?php
				if ($_SESSION['user_type'] == "admin" || $_SESSION['user_id'] === $rs['author'])
				{
					?>
					<div>
						<a href=<?php echo "\"./?show=news&amp;newsID=" . $rs['newsID'] . "&amp;edit\"";?> class="btn btn-default">Redigér</a>
						<form class="inlineForm" action=<?php echo "\"./?show=news\"" ?> method="post">
							<input type="hidden" name="deleteNewsAction" value="delete"></input>
							<input type="hidden" name="newsID" value=<?php echo $rs['newsID']; ?>></input>
							<input type="submit" value="SLET" class="btn btn-default"></input>
						</form>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		</div>

<?php 
}
}
?>
	
<?php
function displayEditCategory()
{
	$stm = $GLOBALS['db']->prepare("SELECT DISTINCT * FROM `category` WHERE `id`=:id");
	$stm->bindParam(':id', $id, PDO::PARAM_INT);
	$id = -1;
	if (ctype_digit($_GET['category']))
	{
		$id = $_GET['category'];
	}
	$stm->execute();
	$rs = $stm->fetch(PDO::FETCH_ASSOC);
?>
	<div class="container">
		<form action="<?php echo "./?show=news&amp;category=" . $rs['id'];?>" method="post">
		<div>
			<p class="whiteText">Navn:</p>
			<input type="text" name="name" value=<?php echo "\"" . $rs['name'] . "\"";?> size="70"></input>
			<input type="hidden" value="<?php echo $rs['id']; ?>" name="id"></input>
			<input type="hidden" name="editCategoryAction"></input>
		</div>
		<div>
		<br>
			<button type="submit" class="btn btn-default">GEM</button>
			</form>
			<a href="<?php echo "./?show=news"; ?>" class="btn btn-default">AFBRYD</a>
			<form class="inlineForm" action="<?php echo "./?show=news" ?>" method="post">
			<input type="hidden" name="deleteCategoryAction" value="delete"></input>
			<input type="hidden" name="id" value=<?php echo $rs['id']; ?>></input>
			<input type="submit" value="SLET" class="btn btn-default"></input>
			</form>
		</div>
	</div>
<?php
}
?>

<?php
function displayCategoryNews()
{
	//Fetch news with the correct category
	$stm = $GLOBALS['db']->prepare("SELECT news.title, news.content, news.author, news.created, news.id AS `newsID`, 
									users.name, category.name AS categoryName FROM `news` 
									INNER JOIN `users` ON news.author=users.id 
									INNER JOIN `category` ON category.id=:categoryID AND news.category=:categoryID ORDER BY `created` DESC LIMIT :limit");
	$stm->bindParam(':categoryID', $categoryID, PDO::PARAM_INT);
	$categoryID = 0;
	if (ctype_digit(trim($_GET['category'])))
	{
		$categoryID = $_GET['category'];
	}
	$stm->bindParam(':limit', $limit, PDO::PARAM_INT);
	$limit = $GLOBALS['limit'];
	$stm->execute();
	$overturn = 0;
	if ($_SESSION['user_type'] != 'none')
	{
		echo '<div class="row">';
		echo '<div class="col-md-1 col-md-offset-5">';
		echo '<a class="btn btn-primary" href="./?show=news&amp;create&amp;category=' . $categoryID . '" >Lav ny nyhed</a>';
		echo '</div>';
		echo '</div>';
	}
	while ($rs = $stm->fetch(PDO::FETCH_ASSOC))
	{
	if ($overturn % 2 === 0)
	{
		echo '<div class="row">';
	}
	?>
	<div class="col-md-6">
		<div class='panel panel-default'>
			<div class='panel-body'>
				<div class="media">
				  <div class="media-body">
					<h2 class="media-heading"><?php echo $rs['title'] ?></h2>
					<?php echo "<p>" . $rs['content'] . "</p>"; ?>
				  </div>
				</div>
			</div>
			<div class='panel-footer'>
				<?php echo "<p> " . $rs['name'] . " " . $rs['created'] . "</p>"; ?>
				<p><?php echo "Kategori: " . $rs['categoryName']; ?></p>
				<?php
				if ($_SESSION['user_type'] == "admin" || $_SESSION['user_id'] === $rs['author'])
				{
					?>
						<a href=<?php echo "./?show=news&amp;newsID=" . $rs['newsID'] . "&amp;edit";?> class="btn btn-default">Redigér</a>
					<?php
				}
				?>
			</div>
		</div>
	</div>
	<?php
	if ($overturn % 2 === 1)
	{
		echo '</div>';
	}
	$overturn++;
	}
		?>
		<?php
		$stm = $GLOBALS['db']->prepare('SELECT COUNT(*) AS `total` FROM `news` WHERE news.category=:categoryID');
		$stm->bindParam(':categoryID', $categoryID, PDO::PARAM_INT);
		$stm->execute();
		$rs = $stm->fetch(PDO::FETCH_ASSOC);
		if ($rs['total'] > 10 && $rs['total'] > $GLOBALS['limit'])
		{
		?>
		<a href=<?php echo "./?show=news&amp;category=" . $categoryID . "&amp;limit=" . seeMoreNews() . '"'; ?> class="btn btn-primary">Se flere nyheder</a>
		<?php
		}
		?>
		<?php
}
?>

<?php
function overviewCategories()
{
	if ($_SESSION['user_type'] == 'admin')
	{
	?>
		<div class="row">
		<div class="col-md-1 col-md-offset-5">
		<a href="./?show=news&amp;createCategory" class="btn btn-primary">Opret ny kategori</a>
		</div>
		</div>
	<?php
	}
	$stm = $GLOBALS['db']->prepare("SELECT DISTINCT * FROM `category`");
	$stm->execute();
	while ($rs = $stm->fetch(PDO::FETCH_ASSOC))
	{
	?>
	<div class="col-md-6 col-md-offset-3">
		<div class='panel panel-default'>
			<div class='panel-body'>
				<div class="media">
					<div class="media-body">
						<h2 class="newstextsize media-heading"><a href=<?php echo "./?show=news&amp;category=" . $rs['id']; ?>><?php echo $rs['name'] ?></a></h2>
						<?php
						if ($_SESSION['user_type'] == 'admin')
						{
						?>
						<a href=<?php echo "./?show=news&amp;edit&amp;category=" . $rs['id']; ?>><button class="right btn btn-default">Redigér</button></a>
						<?php
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	}
?>

<?php
}
?>

<?php
function displayCreateCategory()
{
?>
<div class="row">
	<div class="container">
	<form action=<?php echo "./?show=news&amp;categories";?> method="post">
		<div>
			<p class="whiteText">Navn:</p>
			<input type="text" name="name" value="Skriv navnet på din kategori her" size="70"></input>
			<input type="hidden" name="createCategoryAction" value="somethingDarkside"></input>
		</div>
		<div>
			<br>
			<button type="submit" class="btn btn-default">GEM</button>
			</form>
			<a href="<?php echo "./?show=news"; ?>" class="btn btn-default">AFBRYD</a>
		</div>
	</div>

<?php
}
?>

<?php
function displayNews()
{
?>
	<?php
	if ($_SESSION['user_type'] == 'normal')
	{
	?>
	<div class='row'>
		<div class='col-md-12'>
			<a href="./?show=news&amp;create" class="btn btn-primary">Lav ny nyhed</a>
		</div>
	</div>
	<?php
	}
	?>
	<img id='asgers_baggrunds_div' src='img/pacmanMi1.png' style='position:absolute;top:80px;left:0px;width:100%;opacity:0.6;'>
	<div class="row">
		<div class="col-md-7">
			<?php
			if ($_SESSION['user_type'] == 'admin' || $_SESSION['user_type'] == 'normal')
			{
			?>
				<a href="./?show=news&amp;create" class="btn btn-primary">Lav ny nyhed</a>
			<?php
			}
			?>
			
	<?php
		//Fetch all general news and print them
		$stm = $GLOBALS['db']->prepare("SELECT news.title, news.content, news.author, news.created, news.id AS `newsID`, users.name, category.name AS categoryName FROM `news` INNER JOIN `users` ON news.author=users.id INNER JOIN `category` ON news.category=category.id AND category.id=1 ORDER BY `created` DESC LIMIT :limit");
		$stm->bindParam(':limit', $limit, PDO::PARAM_INT);
		$limit = $GLOBALS['limit'];
		$stm->execute();
		while($rs = $stm->fetch(PDO::FETCH_ASSOC))
		{
		?>
					<div class='panel panel-default'>
			<div class="panel-body">
				<div class="media">
				  <div class="media-body">
					<h2 class="media-heading"><a href=<?php echo "./?show=news&amp;newsID=" . $rs['newsID']; ?>><?php echo substr($rs['title'], 0, 100); ?></a></h2>
					<?php 
					//Find a way to limit news size without breaking any tags in the chosen news
						$substring = substr($rs['content'], 0); 
						echo "<p>" . $substring . "</p>";
					?>
				  </div>
				</div>
			</div>
			<div class='panel-footer'>
				<?php echo "<p> " . $rs['name'] . " " . $rs['created'] . "</p>"; ?>
				<p><?php echo "Kategori: " . $rs['categoryName']; ?></p>
				<?php
				if ($_SESSION['user_type'] == "admin" || $_SESSION['user_id'] === $rs['author'])
				{
					?>
						<a href=<?php echo "./?show=news&amp;newsID=" . $rs['newsID'] . "&amp;edit";?> class="btn btn-default">Redigér</a>
					<?php
				}
				?>
			</div>
		</div>
		<?php
		}
		?>
		<?php
		$stm = $GLOBALS['db']->prepare('SELECT COUNT(*) AS `total` FROM `news` WHERE news.category=1');
		$stm->execute();
		$rs = $stm->fetch(PDO::FETCH_ASSOC);
		if ($rs['total'] > 10 && $rs['total'] > $GLOBALS['limit'])
		{
		?>
		<a href=<?php echo "./?show=news&amp;limit=" . seeMoreNews(); ?> class="btn btn-primary">Se flere nyheder</a>
		<?php
		}
		?>
		</div>
	
		<div class="col-md-5">
		<!--Do second column here with news-games-categories-->
		
	<?php
	if ($_SESSION['user_type'] == 'admin' || $_SESSION['user_type'] == 'normal')
	{
	?>
	
			<a href="./?show=news&amp;categories" class="btn btn-primary">Se kategorier</a>
		
	<?php
	}
	?>




		<?php
		//Fetch all other categories and print newest newsstory for each
		//$stm = $GLOBALS['db']->prepare("SELECT news.title, news.content, news.author, news.created, news.id AS `newsID`, users.name, category.name AS categoryName, category.id AS categoryID FROM `news` INNER JOIN `users` ON news.author=users.id INNER JOIN `category` ON category.id > 1");
		$stm = $GLOBALS['db']->prepare("SELECT DISTINCT * FROM `category` WHERE `id`>1");
		$stm->execute();
		while ($rs = $stm->fetch(PDO::FETCH_ASSOC))
		{
		?>
			<div class='panel panel-default'>
				<div class='panel-body'>
					<div class="media">
						<div class="media-body">
							<center>
		<h2 class="media-heading"><a href=<?php echo "./?show=news&amp;category=" . $rs['id']; ?>><?php echo $rs['name']; ?></a></h2></center>
							<?php 
							$statement = $GLOBALS['db']->prepare("SELECT news.title, news.created, news.id AS `newsID` FROM `news` INNER JOIN `category` ON category.id=news.category AND category.id=:id  ORDER BY `created` DESC LIMIT 1");
							$statement->bindParam(':id', $id, PDO::PARAM_INT);
							$id = $rs['id'];
							$statement->execute();
							if ($resultset = $statement->fetch(PDO::FETCH_ASSOC))
							{
							?>
							<center><h4><a href=<?php echo "./?show=news&amp;newsID=" . $resultset['newsID']; ?>><?php echo $resultset['title']; ?></a></h4></center>
							<?php
							}
							?>
						</div>
					</div>
				</div>
			</div>
		<?php 
		}
		?>
		</div>
</div>

<?php
}
?>

<?php
function seeMoreNews()
{
	return $GLOBALS['limit'] + 10;
}
?>

<?php
//Javascript
function javascript(){
?>


<?php
}
?>
