<?php
require_once("./../credentials.php");
$debug = false; //When set to true, no SMS's will be sent!
//Form submission etc.
if (isset($_POST['message'])){
	
	//Resolve recipients
	$rec_type = getPOST('recipient_type',array('all','attending','not_attending'),'all');
	$event = getPOST('event','/^\d+$/',-1);
	if ($rec_type == 'all'){
		$stmt = $db->prepare("SELECT * FROM `users` WHERE `active` = 1;");
	}
	elseif ($rec_type == 'attending'){
		$stmt = $db->prepare("SELECT u.* FROM `users` u, `reservations` r WHERE u.`active` = 1 AND u.`id` = r.`user` AND r.`event` = :event;");
		$stmt->bindParam(':event',$event,PDO::PARAM_INT);
	}
	elseif ($rec_type == 'not_attending'){
		$stmt = $db->prepare("SELECT u.* FROM `users` u LEFT JOIN `reservations` r ON r.`user` = u.`id` AND r.`event` = :event WHERE r.`id` IS NULL AND u.`active` = 1;");
		$stmt->bindParam(':event',$event,PDO::PARAM_INT);
	}
	$stmt->execute();
	$numbers = array();
	while ($rs = $stmt->fetch()){
		if (!in_array($rs['phone'],$numbers) && preg_match($php_phone_regex,$rs['phone']))
			if (0 === strpos($rs['phone'], '45')) {
				$numbers[] = $rs['phone'];
			} else {
				$numbers[] = '45' . $rs['phone'];
			}
	}
	if (count($numbers) == 1) $numbers = $numbers[0];
	
	//Resolve send time
	$send_time = getPOST('send_time',array('now','later'),'now');
	if ($send_time == 'later'){
		$send_time_date = getPOST('send_time_date','|^[0123]?\d/[01]\d/\d{4}$|',date('d/m/Y'));
		$tmp_date = explode('/',$send_time_date);
		$send_time_time = getPOST('send_time_time','|^[012]?\d:[012345]\d$|','20:00');
		$tmp_time = explode(':',$send_time_time);
		$cpsms_time = $tmp_date[2] . $tmp_date[1] . $tmp_date[0] . $tmp_time[0] . $tmp_time[1];
	}
	
	#settings
	$url = 'https://api.cpsms.dk/v2/send';
	$key = 'bWdhbWVyc2RrOjFjODBlNzk2LWE5MGMtNDI5ZC1hMTZiLWQyY2FkNjczYzE5Zg==';
	$data = array(
		'to' => $numbers,
		'from' => 'MGamers',
		'message' => $_POST['message']
	);
	//if ($send_time == 'later')
		//$query['timestamp'] = $cpsms_time;
	$opts = array('http' =>
		array(
			'method' => 'POST',
			'header' => array(
				"Authorization: Basic $key",
				'Content-type: application/json'
			),
			'content' => json_encode($data)
		)
	);
	/*
	$query = array( 'username' => $cpsms_username,
					'password' => $cpsms_password,
					'message' => $_POST['message'],
					'from' => 'MGamers',
					'utf8' => '1',
					'recipient' => $numbers);

	 */
	
	
	//$url  = "http://www.cpsms.dk/sms/?" . http_build_query($query);
	
	if ($debug)
		die("Debug is on! The requested URL is: $url");
	else{
		$context = stream_context_create($opts);
		$result = file_get_contents($url, false, $context);
		$decoded = json_decode($result, true);
		//if (strstr($result,'<succes>'))
		if (count($decoded['error']) === 0)
		{
			header("Location: ./?show=sms&message=smsok");
			die("SMS Sent ok!");
		}
		else {
			$errors = array();
			foreach ($decoded['error'] as $err) {
				$errors[] = $err['to'] . ' -> ' . $err['errMsg'];
			}
			$_SESSION['cpsms_response'] = implode(', ', $errors);
			header("Location: ./?show=sms&message=smsfail");
			die("SMS failed!");
		}
	}	
}

?>


<?php
//Content
function content()
{
	global $db;
	$prev_stmt = $db->prepare("SELECT `id`, `start` FROM `events` WHERE `end` < NOW() ORDER BY `end` DESC LIMIT 10;");
	$prev_stmt->execute();
	$next_stmt = $db->prepare("SELECT `id`, `start` FROM `events` WHERE `end` >= NOW() ORDER BY `start` ASC;");
	$next_stmt->execute();
?>
<div class="container">
	<div class='well'>
		<h1>Afsending af SMSer</h1>
		<form method="post" action="./?show=sms">
			<div class="row">
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">Vælg modtagere</div>
						<div class="panel-body">
							<div class="radio">
								<label>
									<input type="radio" name="recipient_type" value="all" checked class="recipient_type_radio" id="all_type"/> Alle medlemmer
								</label>
							</div>
							<div class="form-group">
								<label class="radio-inline">
									<input type="radio" name="recipient_type" value="attending" class="recipient_type_radio" id="attend_type"/> Tilmeldt arrangement
								</label>
								<label class="radio-inline">
									<input type="radio" name="recipient_type" value="not_attending" class="recipient_type_radio" id="not_attend_type"/> Ikke tilmeldt arrangement
								</label>
							</div>
							<select class="form-control" id="recipient_event" name="event">
								<optgroup label="Kommende arrangementer">
								<?php while ($rs = $next_stmt->fetch()){ ?>
									<option value="<?=$rs['id']?>"><?=getFullDisplayDate($rs['start'])?></option>
								<?php } ?>
								</optgroup>
								<optgroup label="Tidligere arrangementer">
								<?php while ($rs = $prev_stmt->fetch()){ ?>
									<option value="<?=$rs['id']?>"><?=getFullDisplayDate($rs['start'])?></option>
								<?php } ?>
								</optgroup>
							</select>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">Vælg afsendelsestidspunkt</div>
						<div class="panel-body">
							<div class="radio">
								<label>
									<input type="radio" name="send_time" value="now" checked class="send_time_radio" id="now_time"/> Nu
								</label>
							</div>
							<div class="radio">
								<label>
									<input type="radio" name="send_time" value="later" class="send_time_radio"/> Senere
								</label>
							</div>
							<div id="send_time_div">
								<div class="col-sm-6">
									<input type="text" name="send_time_date" id="send_time_date" class="datepicker form-control" value="<?=date("d/m/Y")?>"/>
								</div>
								<div class="col-sm-6">
									<input type="time" name="send_time_time" id="send_time_time" class="form-control" value="15:00" />
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">Skriv SMS</div>
						<div class="panel-body">
							<textarea name="message" class="form-control" id="message" rows=10></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<a data-toggle="modal" href="#sms_send_modal" class="btn btn-primary" id="send_modal_link">Send...</a>
				</div>
			</div>
			<div class="modal fade" id='sms_send_modal'>
			  <div class="modal-dialog">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Bekræft afsendelse af SMSer</h4>
				  </div>
				  <div class="modal-body">
					<p>Du er ved at sende SMSer til medlemmer. Dette koster penge.</p>
					<p>Gennemlæs SMSen herunder og bekræft modtagerne og afsendelsestidpunkt.</p>
					<pre id="confirm_text"></pre>
					<p>Modtagere: <strong id="confirm_recipients"></strong></p>
					<p>Sendes: <strong id="confirm_time"></strong></p>
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Ret</button>
					<button type="submit" class="btn btn-primary loading-button">Send</button>
				  </div>
				</div><!-- /.modal-content -->
			  </div><!-- /.modal-dialog -->
			</div><!-- /.modal -->  
		</form>
	</div>
</div>

<?php
}
?>


<?php
//Javascript
function javascript(){
?>
<script type="text/javascript">
$("#send_modal_link").click(function(){
	$("#confirm_text").html($("#message").val());
	if ($("#all_type").prop("checked"))
		$("#confirm_recipients").html("Alle medlemmer");
	else if ($("#attend_type").prop("checked"))
		$("#confirm_recipients").html("Tilmeldte til arrangementet " + $("#recipient_event option:selected").text());
	else
		$("#confirm_recipients").html("Ikke tilmeldte til arrangementet " + $("#recipient_event").val());
	if ($("#now_time").prop("checked"))
		$("#confirm_time").html("Nu");
	else
		$("#confirm_time").html($("#send_time_date").val() + ' ' + $("#send_time_time").val());
});
$(".recipient_type_radio").change(function(){
	if ($("#all_type").prop("checked"))
		$("#recipient_event").slideUp();
	else
		$("#recipient_event").slideDown();
});
$("#recipient_event").toggle(false);

$(".send_time_radio").change(function(){
	if ($("#now_time").prop("checked"))
		$("#send_time_div").slideUp();
	else
		$("#send_time_div").slideDown();
});
$("#send_time_div").toggle(false);

$(function() {	
	$( ".datepicker" ).datepicker({
		beforeShow: function() {
			setTimeout(function(){
				$('.ui-datepicker').css('z-index', 10);
			}, 0);
		},
		dateFormat : "dd/mm/yy",
		firstDay : 1,
		minDate : 0,
		maxDate : 7,
		monthNames :  ["Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December"],
		monthNamesShort : ["Jan","Feb","Mar","Apr","Maj","Jun","Jul","Aug","Sep","Okt","Nov","Dec"],
		dayNames : ["Søndag","Mandag","Tirsdag","Onsdag","Torsdag","Fredag","Lørdag"],
		dayNamesShort : ["Søn","Man","Tir","Ons","Tors","Fre","Lør"],
		dayNamesMin : ["Sø","Ma","Ti","On","To","Fr","Lø"]
	});
});
</script>
<?php
}
?>
