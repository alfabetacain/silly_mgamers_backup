<?php
//Form submission etc.


?>


<?php
//Content
function content()
{
require_once('./php/connectdb.php');
$stm = $GLOBALS['db']->prepare("SELECT * FROM `users` WHERE id=:user_id");
$stm->bindParam('user_id', $user_id);
$user_id = -1;
if (ctype_digit($_GET['id']))
{
	$user_id = $_GET['id'];
}
$stm->execute();
$rs = $stm->fetch(PDO::FETCH_ASSOC);
?>
<div class="container">
	<div class='panel panel-default'>
		<div class='panel-body'>
			<div class="row">
				<div class="col-md-2">
					<div class="skiftkode thumbnail">
						<?php if ($rs['avatar'] != null) { ?>
							<img src='uploads/avatars/<?=$rs['avatar']?>'  alt="<?=$rs['name']?>'s avatar" />
						<?php } else { ?>
							<img src="https://gravatar.com/avatar/<?=md5(strtolower($rs['email']))?>?s=200&amp;d=identicon" alt="<?=$rs['name']?>'s avatar">
						<?php } ?>
					</div>
				</div>
				<div class="col-md-8">
					<h1><?php echo $rs['name']; ?></h1>
					<dl class="dl-horizontal">
						<dt>Nickname</dt><dd><?php echo $rs['nick']; ?></dd>
						<dt>Email</dt><dd style="overflow:hidden;"><a href="mailto:<?php echo $rs['email']; ?>"><?php echo $rs['email']; ?></a></dd>
						<?php if ($rs['privacy'] == 'off' || $_SESSION['user_type'] == 'admin') {?>
							<dt>Telefon</dt><dd><span class="visible-desktop"><?php echo $rs['phone']; ?></span></dd>
						
						<?php }?>
						<?php if ($rs['privacy'] == 'off' || $_SESSION['user_type'] == 'admin') {?>
							<dt>Adresse</dt><dd  style="overflow:hidden;"><?php 
							
							if (!empty($rs['address']) && !empty($rs['zip']))
							{
								echo $rs['address'] . ", " . $rs['zip'] . ' ' . getCityFromZip($rs['zip']); 
							}	
							?></dd>
						<?php }?>
						<dt>Dato for indmelding</dt><dd><!--SKAL MULIGVIS UDFYLDES--></dd>
						<dt>Admin</dt><dd><?php if ($rs['type'] == 'admin') {echo "Ja";} else {echo "Nej";} ?></dd>
						<dt>Steam</dt><dd><?php echo $rs['steam']; ?></dd>
						<dt>Origin</dt><dd><?php echo $rs['origin']; ?></dd>
						<dt>Skype</dt><dd><a href="skype:<?php echo $rs['skype']; ?>?add" class="visible-desktop"><?php echo $rs['skype']; ?></a></dd>
						<dt>Battlelog</dt><dd><?php echo $rs['battlelog']; ?></dd>
					</dl>
				</div>
				<div class="col-md-2">
					<div class="btn-group-vertical pull-right">
						<?php if ($_SESSION['user_type'] == 'admin' || ($_SESSION['user_type'] == 'normal' && $_SESSION['user_id'] == $user_id)){ ?>
							<a href='./?show=settings&amp;id=<?=$user_id?>' class='skiftkode btn btn-default'>Rediger</a>
							<a href='./?show=password&amp;id=<?=$user_id?>' class='skiftkode btn btn-default'>Skift kode</a>
						<?php } ?>
					</div>
				</div>
				
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class='panel panel-default'>
				<div class='panel-body'>
					<h2>Spil</h2>
					<div class="game-row">
						<ul>
							<?php for ($j = 1; $j <= 15; $j+=1){?>
							<li><img class= "skiftkode" src="img/games/img(<?php echo $j;?>).jpg"/></li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class='panel panel-default'>
				<div class='panel-body'>
					<?php 					
					//System information
					$stmt = $GLOBALS['db']->prepare("SELECT * FROM `systemInfo` WHERE userid=:ID");
					$stmt->bindParam(':ID', $ID, PDO::PARAM_INT);
					$ID = $user_id;
					$stmt->execute();
					global $sysRS;
					$sysRS = $stmt->fetchAll(PDO::FETCH_ASSOC);
					if ($sysRS)
					{
						global $count;
						$count = count($sysRS);
						if ($count > 1)
						{
						?>
							<select id="computers" onchange="switchComputer()">
								<?php for ($i = 0; $i < $count; $i++)
								{ ?>
									<option value="<?php echo $i; ?>"><?php echo $sysRS[$i]['name']; ?></option>
								<?php
								}
								?>
							</select>
						<?php
						}
						else
						{ ?>
							<h2><?php echo $sysRS[0]['name']; ?></h2>
						<?php
						}
						?>
						<dl class="dl-horizontal">
							<dt>Navn</dt><dd id="name"><?php echo $sysRS[0]['name']; ?></dd>
							<dt>CPU</dt><dd id="cpu"><?php echo $sysRS[0]['CPU']; ?></dd>
							<dt>GPU</dt><dd id="gpu"><?php echo $sysRS[0]['GPU']; ?></dd>
							<dt>PSU</dt><dd id="psu"><?php echo $sysRS[0]['PSU']; ?></dd>
							<dt>Motherboard</dt><dd id="motherboard"><?php echo $sysRS[0]['motherboard']; ?></dd>
							<dt>RAM</dt><dd id="ram"><?php echo $sysRS[0]['RAM']; ?></dd>
							<dt>Memory</dt><dd id="memory"><?php echo $sysRS[0]['memory']; ?></dd>
							<dt>Monitor</dt><dd id="monitor"><?php echo $sysRS[0]['monitor']; ?></dd>
							<dt>Mouse</dt><dd id="mouse"><?php echo $sysRS[0]['mouse']; ?></dd>
							<dt>Keyboard</dt><dd id="keyboard"><?php echo $sysRS[0]['keyboard']; ?></dd>
							<dt>Audio</dt><dd id="audio"><?php echo $sysRS[0]['audio']; ?></dd>
						</dl>
						<?php
						if ($user_id == $_SESSION['user_id'] || $_SESSION['user_type'] == 'admin')
						{
						?>
						<?php
						}
						// if ($count > 1)
						// {
						// script for switching between computers
							// javascript();
						// }
					}
					else {
					?>
						<h2 class="whiteText">System</h2>
					<?php } ?>
						<a href="./?show=system_settings&id=<?php echo $user_id; ?>&myComputers" class="btn btn-default">Mine computere</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
}
?>


<?php
//Javascript
function javascript(){
	global $count;
	global $sysRS;
?>
	<script type="text/javascript">
	function switchComputer()
	{
		var selectedElement = document.getElementById("computers");
		var selectedValue = selectedElement.options[selectedElement.selectedIndex].value;
		
		switch (selectedValue)
		{
			<?php
			for ($i = 0; $i < $count; $i++)
			{ ?>
				case "<?php echo $i; ?>":
					document.getElementById("name").innerHTML="<?php echo $sysRS[$i]['name']; ?>";
					document.getElementById("cpu").innerHTML="<?php echo $sysRS[$i]['CPU']; ?>";
					document.getElementById("gpu").innerHTML="<?php echo $sysRS[$i]['GPU']; ?>";
					document.getElementById("psu").innerHTML="<?php echo $sysRS[$i]['PSU']; ?>";
					document.getElementById("motherboard").innerHTML="<?php echo $sysRS[$i]['motherboard']; ?>";
					document.getElementById("ram").innerHTML="<?php echo $sysRS[$i]['RAM']; ?>";
					document.getElementById("memory").innerHTML="<?php echo $sysRS[$i]['memory']; ?>";
					document.getElementById("monitor").innerHTML="<?php echo $sysRS[$i]['monitor']; ?>";
					document.getElementById("mouse").innerHTML="<?php echo $sysRS[$i]['mouse']; ?>";
					document.getElementById("keyboard").innerHTML="<?php echo $sysRS[$i]['keyboard']; ?>";
					document.getElementById("audio").innerHTML="<?php echo $sysRS[$i]['audio']; ?>";
					break;
			<?php
			}
			?>
		}
	}
	</script>

<?php
}
?>
