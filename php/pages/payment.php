<?php
//Form submission etc.
$delete_id = getPOST('delete_id','/^\d+$/',-1);
if ($delete_id != -1 && $_SESSION['user_type'] == 'admin'){
	$delete_stmt = $db->prepare("DELETE FROM `payments` WHERE `id` = :id LIMIT 1;");
	$delete_stmt->bindParam(':id',$delete_id,PDO::PARAM_INT);
	$delete_stmt->execute();
	$user_id = getGET('user','/^\d+$/',-1);
	header("Location: ./?show=payment&user=$user_id");
	die("Deleted payment");
}

if (isset($_POST['user']) && $_SESSION['user_type'] == 'admin'){
	$user = getPOST('user','/^\d+$/',-1);
	$year = getPOST('year','/^\d+$/',-1);
	$season = getPOST('season',array('spring','fall'),'spring');
	$amount = getPOST('amount','/^\d+$/',-1);
	$note = htmlentities(getPOST('note'));
	
	$stmt = $db->prepare("INSERT INTO `payments` (`user`,`year`,`season`,`amount`,`note`,`paid_to_user`) VALUES (:user, :year, :season, :amount, :note, :paid_to_user);");
	$stmt->bindParam(':user',$user,PDO::PARAM_INT);
	$stmt->bindParam(':year',$year,PDO::PARAM_INT);
	$stmt->bindParam(':season',$season,PDO::PARAM_STR);
	$stmt->bindParam(':amount',$amount,PDO::PARAM_INT);
	$stmt->bindParam(':note',$note,PDO::PARAM_STR);
	$stmt->bindParam(':paid_to_user',$_SESSION['user_id'],PDO::PARAM_STR);
	$stmt->execute();
	$user = getGET('user','/^\d+$/',-1);
	if ($user == -1)
		header("Location: ./?show=payment");
	else
		header("Location: ./?show=payment&user=$user");
	die("Payment created");
}

?>


<?php
//Content
function content()
{
	$user = getGET('user','/^\d+$/',-1);
	if ($user != -1)
		userContent($user);
	else
		generalContent();
}

function userContent($user)
{
	global $db;
	$member_stmt = $db->prepare("SELECT * FROM `users`  WHERE `id` = :id ;");
	$member_stmt->bindParam(':id',$user,PDO::PARAM_INT);
	$member_stmt->execute();
	$member = $member_stmt->fetch();
	
	$payment_stmt = $db->prepare("SELECT p.*, u.`name` recipient_name FROM `payments` p LEFT JOIN `users` u ON u.`id` = p.`paid_to_user` WHERE p.`user` = :user ORDER BY p.`year` ASC, p.`season` ASC;");
	$payment_stmt->bindParam(':user',$user,PDO::PARAM_INT);
	$payment_stmt->execute();
	
	$payments = array();
	while ($res = $payment_stmt->fetch()){
		$payments[$res['year'].'_'.$res['season']] = $res;
	}
	$member['missing_payments'] = array();
	$php_date = strtotime($member['registration_date']);
	$year = date('Y',$php_date);
	$season = season($php_date);
	$season_val = ($season == 'spring') ? 0 : 0.5;
	$cur_season_val = (season() == 'spring') ? 0 : 0.5;
	while (($year + $season_val) <= (date('Y') + $cur_season_val))
	{
		if (!isset($payments[$year.'_'.$season])){
			if ($season == 'spring')
				$member['missing_payments'][] = '<span class="label label-warning season_label" data-season="spring" data-year="'.$year.'">' . ('Forår ' . $year) . '</span>';
			else
				$member['missing_payments'][] = '<span class="label label-danger season_label" data-season="fall" data-year="'.$year.'">' . ('Efterår ' . $year) . '</span>';
		}
		if ($season == 'spring'){$season = 'fall';}else{$season = 'spring';$year++;}
		$season_val = ($season == 'spring') ? 0 : 0.5;
	}
	
?>
<div class='container'>
	<div class='panel panel-default'>
		<div class='panel-body'>
			<div class='row'>
				<div class='col-md-10'>
					<h1>Betalingsinformation for <?=$member['name']?></h1>
				</div>
				<div class='col-md-2 text-right'>
					<a href='./?show=payment' class='btn btn-sm btn-primary'>Tilbage til oversigten</a>
				</div>
			</div>
			<form method='post' action='./?show=payment&amp;user=<?=$user?>'>
				<table class='table'>
					<thead>
						<tr><th>Periode</th><th>Beløb</th><th>Betalingsmetode</th><th>Betalings ID</th><th>Note</th><th></th></tr>
					</thead>
					<tbody>
						<?php
						foreach ($payments as $payment){
						?>
							<tr>
								<td>
								<?php
									if ($payment['season'] == 'spring')
										echo '<span class="label label-warning">' . ('Forår ' . $payment['year']) . '</span>';
									else
										echo '<span class="label label-danger">' . ('Efterår ' . $payment['year']) . '</span>';
								?>
								</td>
								<td><?=$payment['amount']?>,- kr</td>
								<td>
								<?php
									if ((isset($payment['paid_to_user']) && $payment['paid_to_user'] != null ))
										echo "Kontant";
									else
										echo "Paypal";
								?>
								</td>
								<td>
								<?php
									if ((isset($payment['paid_to_user']) && $payment['paid_to_user'] != null ))
										echo '#'.$payment['id'] . " (til <a href='./?show=member&amp;id={$payment['paid_to_user']}'>{$payment['recipient_name']}</a>)";
									else
										echo $payment['paypal_txn_id'] . ' / ' . $payment['paypal_payer_id'];
								?>
								</td>
								<td><?=$payment['note']?></td>
								<td><button class='btn btn-xs btn-danger' name='delete_id' value='<?=$payment['id']?>' onclick="return confirm('Er du sikker på at du vil slette denne betaling?\nDette kan ikke fortrydes!')">Slet</button></td>
							</tr>
						<?php }
						if (count($payments) == 0) {
						?>
							<tr>
								<td colspan='6' class='text-center'>Ingen indbetalinger registreret</td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
			</form>
			<hr>
			<p>
				<strong>Manglende indbetalinger: </strong>
				<?=implode(' ', $member['missing_payments'])?>
			</p>
			<div class='pull-right'>
				<a href='./?show=settings&amp;id=<?=$member['id']?>' class='btn btn-default'>Rediger medlemsindstillinger</a> 
				<a data-toggle="modal" href="#register_payment_modal" class='btn btn-primary register_payment_link' data-id='<?=$member['id']?>' data-name='<?=$member['name']?>'>Registrer betaling...</a>
			</div>
		</div>
	</div>
</div>

<?php
	sharedContent(implode(' ', $member['missing_payments']), array($member), './?show=payment&amp;user=' . $member['id']);
}

function generalContent()
{
	global $db;
	$member_stmt = $db->prepare("SELECT * FROM `users` ORDER BY `name` ASC;");
	$member_stmt->execute();
	$payment_stmt = $db->prepare("SELECT * FROM `payments`;");
	$payment_stmt->execute();
	
	$payments = array();
	while ($res = $payment_stmt->fetch()){
		if (!isset($payments[$res['user']]))
			$payments[$res['user']] = array();
		$payments[$res['user']][$res['year'].'_'.$res['season']] = $res;
	}
	
	$members = array();
	while($res = $member_stmt->fetch()){
		$res['missing_payments'] = array();
		$php_date = strtotime($res['registration_date']);
		$year = date('Y',$php_date);
		$season = season($php_date);
		$season_val = ($season == 'spring') ? 0 : 0.5;
		$cur_season_val = (season() == 'spring') ? 0 : 0.5;
		while (($year + $season_val) <= (date('Y') + $cur_season_val))
		{
			if (!isset($payments[$res['id']][$year.'_'.$season])){
				if ($season == 'spring')
					$res['missing_payments'][] = '<span class="label label-warning season_label" data-season="spring" data-year="'.$year.'">' . ('Forår ' . $year) . '</span>';
				else
					$res['missing_payments'][] = '<span class="label label-danger season_label" data-season="fall" data-year="'.$year.'">' . ('Efterår ' . $year) . '</span>';
			}
			if ($season == 'spring'){$season = 'fall';}else{$season = 'spring';$year++;}
			$season_val = ($season == 'spring') ? 0 : 0.5;
		}
		$members[] = $res;
	}
	
?>
<div class='container'>
	<div class='panel panel-default'>
		<div class='panel-body'>
			<h1>Oversigt over medlemmer og indbetalinger (IKKE TAGET I BRUG ENDNU!)</h1>
			<p>Kolonnen <code>Manglende betalinger</code> viser hvilke perioder et medlem mangler at betale for.</p>
			<table class='table'>
				<thead>
					<tr><th>Navn</th><th>Nick</th><th>Medlem siden</th><th>Manglende betalinger</th><th></th></tr>
				</thead>
				<tbody>
					<?php
					foreach ($members as $res){
					?>
						<tr class='member_row' data-id='<?=$res['id']?>'>
							<td><strong><a href='./?show=member&amp;id=<?=$res['id']?>'><?=$res['name']?></a></strong></td>
							<td><?=$res['nick']?></td>
							<td><?=getFullDisplayDate($res['registration_date'])?></td>
							<td class='missing_payments_col'>
								<?php
									if ($res['must_pay'] == 0)
										echo '<span class="label label-success">Ikke betalende medlem</span>';
									elseif (count($res['missing_payments']) == 0)
										echo '<span class="label label-success">Alt betalt</span>';
									else
										echo implode(' ', $res['missing_payments']);
								?>
							</td>
							<td><a href='./?show=payment&amp;user=<?=$res['id']?>' class='btn btn-xs btn-info'>Vis info</a></td>
							<td><a data-toggle="modal" href="#register_payment_modal" class='btn btn-xs btn-primary register_payment_link' data-id='<?=$res['id']?>' data-name='<?=$res['name']?>'>Registrer...</a></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<?php
	sharedContent('', $members, './?show=payment');
}

function sharedContent($default_missing_payments, $members, $return)
{

?>

<div class="modal fade" id='register_payment_modal'>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Registrer betaling for <span id='register_name'></span></h4>
      </div>
	  <form method='post' action='<?=$return?>' class='form-horizontal'>
		  <div class="modal-body">
			<div class="form-group">
				<label for="register_id" class="col-sm-2 control-label">Medlem</label>
				<div class="col-sm-10">
					<select name='user' id='register_id' class='form-control'>
						<?php
							foreach ($members as $res) {
						?>
							<option value='<?=$res['id']?>' ><?=$res['name']?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="inputYear" class="col-sm-2 control-label">Periode</label>
				<div class="col-sm-5">
					<input type='number' value='<?=date('Y')?>' name='year' id='inputYear' class='form-control'>
				</div>
				<div class="col-sm-5">
					<select name='season' class='form-control' id='register_season'>
						<option value='spring' <?php if (season() == 'spring') echo 'selected';?>>Forår</option>
						<option value='fall' <?php if (season() == 'fall') echo 'selected';?>>Efterår</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="inputYear" class="col-sm-2 control-label">Mangler</label>
				<div class="col-sm-10" id="modal_missing_payments">
					<?=$default_missing_payments?>
				</div>
			</div>
			<div class="form-group">
				<label for="inputAmount" class="col-sm-2 control-label">Beløb</label>
				<div class="col-sm-10">
					<div class="input-group">
					  <input type="number" class="form-control text-right" id='inputAmount' name='amount' value='100'>
					  <span class="input-group-addon">.00 kr</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="inputNote" class="col-sm-2 control-label">Note</label>
				<div class="col-sm-10">
					<textarea id='inputNote' name='note' class='form-control' rows=3 placeholder="Indtast en note om betalingen ved særlige omstændigheder (f.eks. lavere beløb, tilladt manglende betaling osv.)"></textarea>
				</div>
			</div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Annuller</button>
			<button type="submit" class="btn btn-primary">Registrer</button>
		  </div>
	  </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 


<?php
}
?>


<?php
//Javascript
function javascript(){
?>
<script type="text/javascript">
$(".register_payment_link").click(function(){
	$("#register_name").text($(this).data('name'));
	$("#register_id").val($(this).data('id'));
	$("#modal_missing_payments").html($(this).parent().parent().find('.missing_payments_col').html());
	updateSeasonLinks();
});

$("#register_id").change(function() {
	$("#register_name").text($("option:selected", this).text());
	$("#modal_missing_payments").html($(".member_row[data-id='" + $("#register_id").val() + "']").find('.missing_payments_col').html());
	updateSeasonLinks();
});

function updateSeasonLinks()
{
	$("#modal_missing_payments .season_label").css('cursor','pointer');
	$("#modal_missing_payments .season_label").click(function() {
		$("#register_season").val($(this).data('season'));
		$("#inputYear").val($(this).data('year'));
		$("#register_season").effect('highlight',{},400);
		$("#inputYear").effect('highlight',{},400);
	});
}
</script>

<?php
}
?>