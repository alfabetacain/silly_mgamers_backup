<?php
//Form submission etc.


?>


<?php
//Content
function content()
{
	global $db;
	$event_stmt = $db->prepare("SELECT e.*, COUNT(r.`id`) as attendance
										FROM `events` e 
										LEFT JOIN `reservations` r ON e.`id` = r.`event` 
										WHERE e.`end` < NOW()
										GROUP BY e.`id`
										ORDER BY e.`start` DESC");
	$event_stmt->execute();


?>
<div class="container">
	<div class="col-md-12 text-center">
		<a href="./?show=events" class="btn btn-lg btn-default">Se kommende arrangementer</a>
	</div>
	<br>
	<div class="well">
		<h1>Tidligere arrangementer</h1>
		<table class="table">
			<thead>
				<tr>
					<th>Start</th><th>Slut</th><th>Sted</th><th>Antal deltagere</th><th></th>
				</tr>
			</thead>
			<tbody>
				<?php			
					$year = -1;
					while ($event = $event_stmt->fetch()){
						if ($year != date('Y', strtotime($event['start']))){
							$year = date('Y', strtotime($event['start']));
							drawYearLine($year);
						}
						drawPrevEvent($event);
					}
				?>
			</tbody>
		</table>
	</div>
</div>
<?php
}
?>

<?php
function drawYearLine($year){
?>
	<tr>
		<td colspan="5" class="text-center"><strong><?=$year?></strong></td>
	</tr>
<?php
}
?>


<?php
function drawPrevEvent($event){
if ($event['legacy_attendance'] != 0 && $event['attendance'] == 0) 
	$event['attendance'] = $event['legacy_attendance'];
?>
	<tr>
		<td><?=getDisplayDateTime($event['start'])?></td>
		<td><?=getDisplayDateTime($event['end'])?></td>
		<td><?=$event['location']?></td>
		<td><strong><?=$event['attendance']?></strong> / <?=$event['rows']*4?></td>
		<td>
			<div class="btn-group">
				<a href="./?show=event&amp;id=<?=$event['id']?>&amp;none" class="btn btn-xs btn-info"><span class="glyphicon glyphicon-user"></span> Deltagerliste</a>
			</div>
		</td>
	</tr>
<?php }?>


<?php
//Javascript
function javascript(){
?>


<?php
}
?>