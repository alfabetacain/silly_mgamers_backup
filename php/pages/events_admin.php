<?php
//Form submission etc.

if (isset($_POST['id']) && ($_POST['id'] == '-1' || ctype_digit($_POST['id'])))
{
	if (isset($_POST['delete'])){
		$stmt1 = $db->prepare("DELETE FROM `reservations` WHERE `event` = :id");
		$stmt2 = $db->prepare("DELETE FROM `events` WHERE `id` = :id");
		$stmt1->bindParam(':id',$_POST['id'],PDO::PARAM_INT);
		$stmt2->bindParam(':id',$_POST['id'],PDO::PARAM_INT);
		$stmt1->execute();
		$stmt2->execute();
		header("Location: ./?show=events_admin&message=event_deleted");
		die();
	}
	//Load from form
	//$datetime = strtotime($row->createdate);
	//$mysqldate = date("m/d/y g:i A", $datetime);
	$id = getPOST('id','/\d+/','-1');
	$startdate = getPOST('startdate','|^[0123]?\d/[01]\d/\d{4}$|',date('d/m/Y'));
	$tmp = explode('/',$startdate);
	$startdate = $tmp[2] . '-' . $tmp[1] . '-' . $tmp[0];
	$start = $startdate . ' ' . getPOST('starttime','|^[012]?\d:[012345]\d$|',date('H:i')) . ':00';
	$enddate = getPOST('enddate','|^[0123]?\d/[01]\d/\d{4}$|',date('d/m/Y',time()+60*60*24*2));
	$tmp = explode('/',$enddate);
	$enddate = $tmp[2] . '-' . $tmp[1] . '-' . $tmp[0];
	$end = $enddate . ' ' . getPOST('endtime','|^[012]?\d:[012345]\d$|',date('H:i')) . ':00';
	$location = getPOST('location');
	$description = getPOST('description');
	$rows = getPOST('rows','/\d*/',8);
	
	$check_stmt = $db->prepare("SELECT `start` FROM `events` WHERE `id` = :id LIMIT 1;");
	$check_stmt->bindParam(':id',$id,PDO::PARAM_INT);
	$check_stmt->execute();
	$check = $check_stmt->fetch();
	
	if ($check === false){
		$stmt = $db->prepare("INSERT INTO `events` (`start`,`end`,`location`,`description`,`rows`) VALUES (:start, :end, :location, :description, :rows);");
	}
	else{
		$stmt = $db->prepare("UPDATE `events` SET `start` = :start ,`end` = :end ,`location` = :location ,`description` = :description, `rows` = :rows WHERE `id` = :id LIMIT 1;");
		$stmt->bindParam(':id',$id,PDO::PARAM_INT);
	}
	$stmt->bindParam(':start',$start,PDO::PARAM_STR);
	$stmt->bindParam(':end',$end,PDO::PARAM_STR);
	$stmt->bindParam(':location',$location,PDO::PARAM_STR);
	$stmt->bindParam(':description',$description,PDO::PARAM_STR);
	$stmt->bindParam(':rows',$rows,PDO::PARAM_INT);
	$stmt->execute();
	
	if ($id == '-1')
		header("Location: ./?show=events_admin&message=event_created");
	else
		header("Location: ./?show=events_admin&message=event_updated");
	die();
}



?>


<?php
//Content
function content()
{
	if (isset($_GET['edit']))
		contentEdit();
	else
		contentOverview();
}
?>

<?php

function contentOverview(){
	global $db;

?>
<div class="container">
	<div class="well">
	
	<?php
		?>
		<div class="row">
			<div class="col-md-4 col-md-push-8"><a href="./?show=events_admin&amp;edit=-1" class="btn btn-success pull-right">Tilføj arrangement</a></div>
			<div class="col-md-8 col-md-pull-4"><h1>Kommende arrangementer</h1></div>
		</div>
		<?php
		$next_stmt = $db->prepare("SELECT e.*, COUNT(r.`id`) participants FROM `events` e LEFT JOIN `reservations` r ON r.`event` = e.`id` WHERE e.`end` >  NOW() GROUP BY e.`id` ORDER BY e.`end` ASC");
		$next_stmt->execute();
		showEventsList($next_stmt);
		?><h1>Tidligere arrangementer</h1><?php
		$prev_stmt = $db->prepare("SELECT e.*, COUNT(r.`id`) participants FROM `events` e LEFT JOIN `reservations` r ON r.`event` = e.`id` WHERE e.`end` <= NOW() GROUP BY e.`id` ORDER BY e.`end` DESC");
		$prev_stmt->execute();
		showEventsList($prev_stmt);
	?>
	</div>
</div>
<?php
}
?>


<?php
function showEventsList($stmt)
{
?>
	<table class="table">
		<thead><tr><th>Start</th><th>Slut</th><th>Sted</th><th>Antal deltagere</th><th></th><th></th></tr></thead>
		<tbody>
			<?php 
				$lastYear = '';
				while ($res = $stmt->fetch()){
					if ($res['legacy_attendance'] != 0 && $res['participants'] == 0) 
						$res['participants'] = $res['legacy_attendance'];
					
					
					if ($lastYear != date("Y", strtotime($res['start'])))
					{
						$lastYear = date("Y", strtotime($res['start']));
						?>
						<tr>
							<td colspan="6" class="text-center"><strong><?=$lastYear?></strong></td>
						</tr>
						<?php 
					}
					?>
					<tr>
						<td><span class="hidden-xs"><?=getDisplayDateTime($res['start'])?></span><span class="visible-xs"><?=date("d/m H:i",strtotime($res['start']))?></span></td>
						<td><span class="hidden-xs"><?=getDisplayDateTime($res['end'])?></span><span class="visible-xs"><?=date("d/m H:i",strtotime($res['end']))?></span></td>
						<td><?=$res['location']?></td>
						<td><strong><?=$res['participants']?></strong> / <?=$res['rows']*4?></td>
						<td><a href="./?show=events_admin&amp;edit=<?=$res['id']?>" class="btn btn-xs btn-info"><span class="glyphicon glyphicon-cog"></span> Rediger</a></td>
						<td><a href="./?show=event&amp;id=<?=$res['id']?>&amp;admin" class="btn btn-xs btn-warning"><span class="glyphicon glyphicon-user"></span> Reservationer</a></td>
					</tr>
			<?php 
				}
			?>
		</tbody>
	</table>
<?php
}

?>

<?php
//Content
function contentEdit()
{
	global $db;
	$startdate = date("d/m/Y");
	$starttime = "17:00";
	$enddate = date("d/m/Y",time()+60*60*24*2);
	$endtime = "16:00";
	$location = "Måløv Skoles gymnastiksal";
	$description = "";
	$rows = 8;
	
	if (isset($_GET['edit']) && $_GET['edit'] != -1 && ctype_digit($_GET['edit']))
	{
		$event_stmt = $db->prepare("SELECT * FROM `events` WHERE `id` = :id LIMIT 1;");
		$event_stmt->bindParam(':id',$_GET['edit'],PDO::PARAM_INT);
		$event_stmt->execute();
		$event = $event_stmt->fetch();
		if ($event !== false) {
			$startdate = date("d/m/Y", strtotime($event['start']));
			$starttime = date("H:i", strtotime($event['start']));
			$enddate = date("d/m/Y", strtotime($event['end']));
			$endtime = date("H:i", strtotime($event['end']));
			$location = htmlspecialchars(str_replace("\\r\\n","\r\n",$event['location']));
			$description = htmlspecialchars(str_replace("\\r\\n","\r\n",$event['description']));
			$rows = $event['rows'];
		}
	}

?>
<div class="container">
  <div class="well">
	<h1><?=($_GET['edit']==-1)?'Tilføj':'Rediger'?> arrangement</h1>
	<form action="./?show=events_admin" method="post" class="form-horizontal">
		<input type="hidden" name="id" value="<?=$_GET['edit']?>" />
		<div class="form-group">
			<label for="fromdate" class="control-label col-sm-2">Start</label>
			<div class="col-sm-6">
				<input type="text" id="fromdate" name="startdate" class="datepicker form-control" value="<?=$startdate?>"/>
			</div>
			<div class="col-sm-4">
				<input type="time" id="fromtime" name="starttime" class="form-control" value="<?=$starttime?>" />
			</div>
		</div>
		<div class="form-group">
			<label for="todate" class="control-label col-sm-2">Slut</label>
			<div class="col-sm-6">
				<input type="text" id="todate" name="enddate" class="datepicker form-control" value="<?=$enddate?>"/>
			</div>
			<div class="col-sm-4">
				<input type="time" id="totime" name="endtime" class="form-control" value="<?=$endtime?>" />
			</div>
		</div>
		<div class="form-group">
			<label for="place" class="control-label col-sm-2">Sted</label>
			<div class="col-sm-10">
				<input type="text" id="place" name="location" class="form-control" value="<?=$location?>"/>
			</div>
		</div>
		<div class="form-group">
			<label for="description" class="control-label col-sm-2">Beskrivelse</label>
			<div class="col-sm-10">
				<textarea type="text" id="description" name="description" class="form-control wysiwyg" rows=8><?="$description"?></textarea>
			</div>
		</div>
		<div class="form-group">
			<label for="rows" class="control-label col-sm-2">Bordrækker</label>
			<div class="col-sm-2">
				<input type="number" id="rows" name="rows" class="form-control" onchange='$("#total").val($("#rows").val()*4)' onkeyup='$("#total").val($("#rows").val()*4)' value="<?=$rows?>"/>
			</div>
			<label for="rows" class="control-label col-sm-2">Pladser i alt</label>
			<div class="col-sm-2">
				<input type="text" id="total" class="form-control" readonly value="<?=$rows*4?>"/>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-6 col-sm-offset-2">
				<input type="submit" value="Gem" class="btn btn-primary"/>
				<a href="./?show=events_admin" class="btn btn-default"/>Annuller</a>
				<?php if ($_GET['edit'] != '-1') { ?>
				<button type="submit" name="delete" value="delete" class="btn btn-danger pull-right" onclick="return confirm('Er du sikker på at du vil slette dette arrangement?\nDette kan ikke fortrydes!')">Slet</button>
				<?php }?>
			</div>
		</div>
	</form>
  </div>
</div>
<?php
}
?>


<?php
//Javascript
function javascript(){
?>
<script type="text/javascript">
$(function() {	
	$( ".datepicker" ).datepicker({
		beforeShow: function() {
			setTimeout(function(){
				$('.ui-datepicker').css('z-index', 10);
			}, 0);
		},
		dateFormat : "dd/mm/yy",
		firstDay : 1,
		monthNames :  ["Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December"],
		monthNamesShort : ["Jan","Feb","Mar","Apr","Maj","Jun","Jul","Aug","Sep","Okt","Nov","Dec"],
		dayNames : ["Søndag","Mandag","Tirsdag","Onsdag","Torsdag","Fredag","Lørdag"],
		dayNamesShort : ["Søn","Man","Tir","Ons","Tors","Fre","Lør"],
		dayNamesMin : ["Sø","Ma","Ti","On","To","Fr","Lø"]
	});
});
</script>
<?php
}
?>