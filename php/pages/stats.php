<?php
//Form submission etc.


?>


<?php
//Content
function content()
{
?>
<div class='container'>
	<div class='panel panel-default'>
		<div class='panel-heading'>
			Fordeling af medlemmer
		</div>
		<div class='panel-body'>
			<p>
				Den fastsatte grænse for medlemmer fra Ballerup kommune (post nr. 2740-2760) er 75%.
			</p>
			<?php
				global $db;
				$stmt = $db->prepare("SELECT `zip` FROM `users`");
				$stmt->execute();
				$in_count = 0;
				$out_count = 0;
				$no_count = 0;
				while ($r = $stmt->fetch()){
					if ($r['zip'] == '' || $r['zip'] == 0) $no_count++;
					elseif ($r['zip'] < 2740 || $r['zip'] > 2760) $out_count++; 
					else $in_count++;
				}
				$total_count = $in_count + $no_count + $out_count;
				$pct_in = floor(($in_count / ($total_count)) * 100);
				$pct_out = floor(($out_count / ($total_count)) * 100);
				$pct_no = 100 - ($pct_in + $pct_out);
				
				$pct_on_in = floor(($in_count / ($in_count + $out_count)) * 100);
				$pct_on_out = 100 - $pct_on_in;
			?>
			<strong>Total fordeling:</strong>
			<div class="progress">
			  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?=$pct_in ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=$pct_in ?>%;">
				<span class=""><?=$pct_in ?>% fra Ballerup kommune</span>
			  </div>
			  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?=$pct_out?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=$pct_out?>%;">
				<span class=""><?=$pct_out ?>% udefra</span>
			  </div>
			  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="<?=$pct_no?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=$pct_no?>%;">
				<span class=""><?=$pct_no ?>% ukendt</span>
			  </div>
			</div>
			<strong>Fordeling af medlemmer der har indtastet postnr. :</strong>
			<div class="progress">
			  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?=$pct_on_in ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=$pct_on_in ?>%;">
				<span class=""><?=$pct_on_in ?>% fra Ballerup kommune</span>
			  </div>
			  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?=$pct_on_out?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=$pct_on_out?>%;">
				<span class=""><?=$pct_on_out ?>% udefra</span>
			  </div>
			</div>
		</div>
	</div>
	<div class='panel panel-default'>
		<div class='panel-heading'>
			Tilmeldinger
		</div>
		<div class='panel-body'>
			<?php
				global $db, $DANISH_MONTHS_CAP;
				$stmt = $db->prepare("SELECT e.*, COUNT(r.`id`) participants FROM `events` e LEFT JOIN `reservations` r ON r.`event` = e.`id` WHERE e.`end` <=  NOW() GROUP BY e.`id` ORDER BY e.`end` ASC");
				$stmt->execute();
				$total_events = 0;
				$total_participants = 0;
				$overall_q = array('bar_width=24');
				$month_buckets = array(0,0,0,0,0,0,0,0,0,0,0,0);
				$month_buckets_event = array(0,0,0,0,0,0,0,0,0,0,0,0);
				while ($res = $stmt->fetch()){
					if ($res['legacy_attendance'] != 0 && $res['participants'] == 0) 
						$res['participants'] = $res['legacy_attendance'];
					$date = strtotime($res['start']);
					$month_buckets[date("n",$date)-1] += $res['participants'];
					$month_buckets_event[date("n",$date)-1] ++;
					$date_str = date('d/m/y',$date);
					$overall_q[] = "input[$date_str]={$res['participants']}";
					$total_events++;
					$total_participants += $res['participants'];
				}
				$months_q = array('bar_width=24');
				foreach($month_buckets as $m => $v){
					$n = $DANISH_MONTHS_CAP[$m+1];
					$months_q[] = "input[$n]=$v";
				}
				$months_event_q = array('bar_width=24');
				foreach($month_buckets_event as $m => $v){
					$n = $DANISH_MONTHS_CAP[$m+1];
					$months_event_q[] = "input[$n]=$v";
				}
				$months_avg_q = array('bar_width=24');
				foreach($month_buckets as $m => $v){
					$n = $DANISH_MONTHS_CAP[$m+1];
					if ($month_buckets_event[$m] == 0)
						$months_avg_q[] = "input[$n]=0";
					else
						$months_avg_q[] = "input[$n]=" . (round(($v / $month_buckets_event[$m])));
				}
			?>
			<strong>
				Antal tilmeldte til arrangementer over tid
			</strong>
			<img src='graph.php?<?=implode('&',$overall_q)?>' class='img-responsive'/>
			<div class='row'>
				<div class='col-md-4'>
					<strong>
						Arrangementer pr. måned
					</strong>
					<img src='graph.php?<?=implode('&',$months_event_q)?>' class='img-responsive'/>
				</div>
				<div class='col-md-4'>
					<strong>
						Tilmeldinger pr. måned
					</strong>
					<img src='graph.php?<?=implode('&',$months_q)?>' class='img-responsive'/>
				</div>
				<div class='col-md-4'>
					<strong>
						Tilmedlinger pr. arrangement pr. måned
					</strong>
					<img src='graph.php?<?=implode('&',$months_avg_q)?>' class='img-responsive'/>
				</div>
				
			</div>
			<strong>
				Totaler
			</strong>
			<dl class="dl-horizontal">
			  <dt>Arrangementer</dt>
				<dd><?=$total_events?> arrangementer</dd>
			  <dt>Tilmeldinger</dt>
				<dd><?=$total_participants?> tilmeldinger</dd>
			</dl>
			
		</div>
	</div>
</div>
<?php
}
?>


<?php
//Javascript
function javascript(){
?>


<?php
}
?>