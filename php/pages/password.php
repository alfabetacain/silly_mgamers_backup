<?php
error_reporting(E_ALL);
if(isset($_POST['id']) && ($_POST['id'] == $_SESSION['user_id'] || $_SESSION['user_type'] == 'admin'))
{
	$id = getPOST('id','/^\d+$/',-1);
	//Verify old password. Only for non-admins or when admins try to change their own password.
	if ($_SESSION['user_type'] != 'admin' || $id == $_SESSION['user_id']){
		//Fetch user
		$user_stmt = $db->prepare("SELECT `name`, `password`, `reset_password`, `reset_time` FROM `users` WHERE `id` = :id LIMIT 1;");
		//reset_time > :time
		$user_stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$user_stmt->execute();
		$user = $user_stmt->fetch();
		//Check login
		$success = $user != null && (
			password_verify($_POST['old_password'], $user['password']) || 
			($user['reset_password'] === $_POST['old_password'] && strtotime($user['reset_time']) > time()));
		if (!$success)
		{
			header("Location: ./?show=password&id=$id&message=passwordchange_unknown");
			die("User not found");
		}
	}
	
	if ($_POST['new_password'] != $_POST['new_password2'])
	{
		header("Location: ./?show=password&id=$id&message=passwordchange_mismatch");
		die("Passwords do not match");
	}
	
	//Reset 'reset password'
	$reset_stmt = $db->prepare("UPDATE `users` SET `reset_password` = '', `reset_time` = '0000-00-00 00:00:00' WHERE `id` = :id LIMIT 1;");
	$reset_stmt->bindParam(':id',$id,PDO::PARAM_INT);
	$reset_stmt->execute();
	
	//Change password
	$update_stmt = $db->prepare("UPDATE `users` SET `password` = :password WHERE `id` = :id LIMIT 1;");
	$newPass = password_hash($_POST['new_password'], PASSWORD_DEFAULT);
	$update_stmt->bindParam(':password',$newPass, PDO::PARAM_STR);
	$update_stmt->bindParam(':id', $id, PDO::PARAM_INT);
	$update_stmt->execute();
	
	//TODO: Policy
	//TODO: Log?
	
	header("Location: ./?show=password&id=$id&message=passwordchange_ok");
	die("Password changed");

}

?>


<?php
//Content
function content()
{
	global $db;
	$stmt = $db->prepare("SELECT `name` FROM `users` WHERE `id` = :id LIMIT 1");
	
	$id = $_SESSION['user_id'];
	if ($_SESSION['user_type'] == 'admin') //If admin, allow use of id param in get
		$id = getGET('id','/^\d+$/',$id);
		
	$stmt->bindParam(':id',$id,PDO::PARAM_INT);
	$stmt->execute();
	$rs = $stmt->fetch();
?>
<div class="container">
	<div class='panel panel-default'>
		<div class='panel-body'>
			<h1>Skift <?php if ($id != $_SESSION['user_id']) echo "<em>{$rs['name']}</em>'s"; ?> adgangskode</h1>
			<form action="./?show=password" method="post">
				<div class="row">
					<div class="col-md-5">
						<input type="hidden" name="id" value="<?=$id?>"/>
						<?php if ($_SESSION['user_type'] != 'admin' || $id == $_SESSION['user_id']){ ?>
							<div class="form-group">
								<label for="old_password">Nuværende adgangskode</label>
								<input type="<?=(isset($_GET['prefill']))?'text':'password'?>" name="old_password" class="form-control" id="old_password" required autocomplete="off" value="<?=(isset($_GET['prefill']))?$_GET['prefill']:''?>">
							</div>
						<?php } ?>
						<div class="form-group">
							<label for="new_password">Ny adgangskode</label>
							<input type="password" name="new_password" class="form-control" id="new_password" required autocomplete="off">
						</div>
						<div class="form-group">
							<label for="new_password2">Gentag ny adgangskode</label>
							<input type="password" name="new_password2" class="form-control" id="new_password2" required autocomplete="off">
						</div>
						<input type="submit" class="btn btn-primary" value="Gem"/>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<?php
}
?>


<?php
//Javascript
function javascript(){
?>


<?php
}
?>
