<?php
//Form submission etc.
if (isset($_POST['confirm']) && $_SESSION['user_type'] != 'none') {
	$stmt = $db->prepare("UPDATE `users` SET `active` = 1, `confirmation_user` = NULL WHERE `confirmation_user` = :cur_id AND `id` = :other_id LIMIT 1;");
	$stmt->bindParam(':cur_id',$_SESSION['user_id'],PDO::PARAM_INT);
	$stmt->bindParam(':other_id',$_POST['confirm'],PDO::PARAM_INT);
	$stmt->execute();
	header("Location: ./?show=confirm_user&message=confirm_ok");
	die("Confirmed user");
}
if (isset($_POST['deny']) && $_SESSION['user_type'] != 'none') {
	$stmt = $db->prepare("UPDATE `users` SET `confirmation_user` = NULL WHERE `confirmation_user` = :cur_id AND `id` = :other_id LIMIT 1;");
	$stmt->bindParam(':cur_id',$_SESSION['user_id'],PDO::PARAM_INT);
	$stmt->bindParam(':other_id',$_POST['deny'],PDO::PARAM_INT);
	$stmt->execute();
	header("Location: ./?show=confirm_user&message=deny_ok");
	die("Denied user");
}


?>


<?php
//Content
function content()
{
	global $db;
	$stmt = $db->prepare("SELECT * FROM `users` WHERE `confirmation_user` = :user AND `active` = '0';");
	$stmt->bindParam(':user',$_SESSION['user_id'],PDO::PARAM_INT);
	$stmt->execute();
?>
<div class='container'>
	<div class='panel panel-default'>
		<div class='panel-body'>
			<h1>Bekræft nye medlemmer</h1>
			<p>
				Nye medlemmer skal bekræftes af eksisterende medlemmer.
			</p>
			<p>
				Følgende liste viser nye medlemmer der har bedt dig om at bekræfte at de bør være medlemmer.
			</p>
			<form action='./?show=confirm_user' method='post'>
				<table class='table'>
					<thead>
						<tr>
							<th>Navn</th><th>Email</th><th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($res = $stmt->fetch()) {
						?>
							<tr>
								<td><?=htmlentities($res['name'])?></td>
								<td><?=htmlentities($res['email'])?></td>
								<td>
									<button class='btn btn-success btn-sm' name='confirm' value='<?=$res['id']?>'>Bekræft</button>
									<button class='btn btn-danger btn-sm' name='deny' value='<?=$res['id']?>'>Afvis</button>
								</td>
							</tr>
						<?php }
						if ($stmt->rowCount() == 0)
						{ ?>
							<tr>
								<td colspan='3' class='text-center'><strong>Ingen ansøgninger</strong></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</form>
		</div>
	</div>
</div>

<?php
}
?>


<?php
//Javascript
function javascript(){
?>
<script type="text/javascript">

</script>
<?php
}
?>