<?php
require_once("./php/seating_table.php");
require_once("./php/connectdb.php");
require_once("./php/util.php");

//$mode = (($_SESSION['user_type'] == 'admin')?SeatingTable::MODE_ADMIN:(($_SESSION['user_type'] == 'normal')?SeatingTable::MODE_NORMAL : SeatingTable::MODE_NONE));
$mode = ($_SESSION['user_type'] != 'none')?SeatingTable::MODE_NORMAL : SeatingTable::MODE_NONE;
$id = getGET('id','/^\d+$/',-1);
$return_url = "./?show=event&id=$id";

if (isset($_GET['none'])){
	$return_url = "./?show=event&id=$id&none";
	$mode = SeatingTable::MODE_NONE;
}
else if ($_SESSION['user_type'] == 'admin' && isset($_GET['admin'])){
	$return_url = "./?show=event&id=$id&admin";
	$mode = SeatingTable::MODE_ADMIN;
}

$st = new SeatingTable($id,$return_url, $mode);
$st->form_submission();
?>


<?php
//Content
function content()
{
	global $st, $db;
	$id = getGET('id','/^\d+$/',-1);
	if ($id == -1){
		header("Location: ./?show=events");
		die("Unknown id");
	}
	else
	{
		$stmt = $db->prepare("SELECT * FROM `events` WHERE `id` = :id LIMIT 1;");
		$stmt->bindParam(':id',$id,PDO::PARAM_INT);
		$stmt->execute();
		$event = $stmt->fetch();
		?>
		<div class="container">
			<div class="panel panel-default">
				<div class="panel-body">
					<center><h2 class="media-heading">Arrangement <?=getDisplayDate($event['start'])?></h2></center>
					<dl class="dl-horizontal">
						<dt>Starter:</dt>
						<dd><?=getDisplayDateTime($event['start'])?></a></dd>
						<dt>Slutter:</dt>
						<dd><?=getDisplayDateTime($event['end'])?></dd>
						<dt>Sted:</dt>
						<dd><?=$event['location']?></dd>
						<dt>Beskrivelse:</dt>
						<dd><?=$event['description']?></dd>
						<?php if ($_SESSION['user_type'] == 'admin') {?>
							<dt>Administration:</dt>
							<dd><?php if (isset($_GET['admin'])) {?>
								<a href='./?show=event&amp;id=<?=$id?>' class='btn btn-info'>Tilbage til alm. tilmelding</a>
							<?php } else { ?>
								<a href='./?show=event&amp;id=<?=$id?>&amp;admin' class='btn btn-info'>Flyt tilmedinger</a>
							<?php } ?>
								<a href='./?show=events_admin&amp;edit=<?=$id?>' class='btn btn-warning'>Rediger arrangement</a>
							</dd>
						<?php } ?>
					</dl>	
				</div>
			</div>
			<?php if ($_SESSION['user_type'] != 'none') $st->content(); ?>
		</div>
		<?php
	}
}
?>

<?php
//Javascript
function javascript(){
	global $st;
	$st->javascript();
}
?>