<?php
//Form submission etc.

if (isset($_POST['user']) && ($_POST['user'] == $_SESSION['user_id'] || $_SESSION['user_type'] == 'admin'))
{	
	if (isset($_POST['editComputer']))
	{
		$upStm = $GLOBALS['db']->prepare("UPDATE `systemInfo` SET `name` = :name, `CPU` = :cpu, `GPU` = :gpu, `PSU` = :psu,
		`motherboard` = :motherboard, `RAM` = :ram, `memory` = :memory,
		`monitor` = :monitor, `mouse` = :mouse, `keyboard` = :keyboard,
		`audio` = :audio WHERE `id` = :computerID");
		$upStm->bindParam(':name', $name, PDO::PARAM_STR);
		$upStm->bindParam(':cpu', $CPU, PDO::PARAM_STR);
		$upStm->bindParam(':gpu', $GPU, PDO::PARAM_STR);
		$upStm->bindParam(':psu', $PSU, PDO::PARAM_STR);
		$upStm->bindParam(':motherboard', $motherboard, PDO::PARAM_STR);
		$upStm->bindParam(':ram', $RAM, PDO::PARAM_STR);
		$upStm->bindParam(':memory', $memory, PDO::PARAM_STR);
		$upStm->bindParam(':monitor', $monitor, PDO::PARAM_STR);
		$upStm->bindParam(':mouse', $mouse, PDO::PARAM_STR);
		$upStm->bindParam(':keyboard', $keyboard, PDO::PARAM_STR);
		$upStm->bindParam(':audio', $audio, PDO::PARAM_STR);
		$upStm->bindParam(':computerID', $computerID, PDO::PARAM_INT);
		
		$name = strip_tags($_POST['name']);
		$CPU = strip_tags($_POST['CPU']);
		$GPU = strip_tags($_POST['GPU']);
		$PSU = strip_tags($_POST['PSU']);
		$motherboard = strip_tags($_POST['motherboard']);
		$RAM = strip_tags($_POST['RAM']);
		$memory = strip_tags($_POST['memory']);
		$monitor = strip_tags($_POST['monitor']);
		$mouse = strip_tags($_POST['mouse']);
		$keyboard = strip_tags($_POST['keyboard']);
		$audio = strip_tags($_POST['audio']);
		$computerID = -1;
		if (ctype_digit($_POST['computerID']))
		{
			$computerID = $_POST['computerID'];
		}
		$upStm->execute();
		
		$user = -1;
		if (ctype_digit($_POST['user']))
		{
			$user = $_POST['user'];
		}
		
		if ($upStm)
		{
			header("Location: ./?show=member&id=$user&message=userUpdated");
		}
		else
		{
			header("Location: ./?show=member&id=$user&message=userUpdatedFail");
		}
	}
	elseif (isset($_POST['deleteComputer']))
	{
		$delStm = $GLOBALS['db']->prepare("DELETE FROM `systemInfo` WHERE `id`=:computerID");
		$delStm->bindParam(':computerID', $computerID, PDO::PARAM_INT);
		
		$computerID = -1;
		if (ctype_digit($_POST['computerID']))
		{
			$computerID = $_POST['computerID'];
		}
		
		$delStm->execute();
		
		if ($delStm)
		{
			header("Location: ./?show=system_settings&id=$_POST[user]&myComputers&message=successful");
		}
		else
		{
			header("Location: ./?show=system_settings&id=$_POST[user]&myComputers&message=unsuccessful");
		}
	}
	elseif (isset($_POST['createComputer']))
	{
		$insStm = $GLOBALS['db']->prepare("INSERT INTO `systemInfo` (`name`, `CPU`,`GPU`, `PSU`, `motherboard`, `RAM`, `memory`, `monitor`, `mouse`, `keyboard`, `audio`, `userid`)
		VALUES (:name, :cpu, :gpu, :psu, :motherboard, :ram, :memory, :monitor, :mouse, :keyboard, :audio, :user)");
		$insStm->bindParam(':name', $name, PDO::PARAM_STR);
		$insStm->bindParam(':cpu', $CPU, PDO::PARAM_STR);
		$insStm->bindParam(':gpu', $GPU, PDO::PARAM_STR);
		$insStm->bindParam(':psu', $PSU, PDO::PARAM_STR);
		$insStm->bindParam(':motherboard', $motherboard, PDO::PARAM_STR);
		$insStm->bindParam(':ram', $RAM, PDO::PARAM_STR);
		$insStm->bindParam(':memory', $memory, PDO::PARAM_STR);
		$insStm->bindParam(':monitor', $monitor, PDO::PARAM_STR);
		$insStm->bindParam(':mouse', $mouse, PDO::PARAM_STR);
		$insStm->bindParam(':keyboard', $keyboard, PDO::PARAM_STR);
		$insStm->bindParam(':audio', $audio, PDO::PARAM_STR);
		$insStm->bindParam(':user', $userID, PDO::PARAM_INT);
		
		$name = strip_tags($_POST['name']);
		$CPU = strip_tags($_POST['CPU']);
		$GPU = strip_tags($_POST['GPU']);
		$PSU = strip_tags($_POST['PSU']);
		$motherboard = strip_tags($_POST['motherboard']);
		$RAM = strip_tags($_POST['RAM']);
		$memory = strip_tags($_POST['memory']);
		$monitor = strip_tags($_POST['monitor']);
		$mouse = strip_tags($_POST['mouse']);
		$keyboard = strip_tags($_POST['keyboard']);
		$audio = strip_tags($_POST['audio']);
		$userID = -1;
		if (ctype_digit($_POST['user']))
		{
			$userID = $_POST['user'];
		}
		$insStm->execute();
		
		if ($insStm)
		{
			header("Location: ./?show=member&id=$userID&message=successful");
		}
		else
		{
			header("Location: ./?show=member&id=$userID&message=unsuccessful");
		}
	}
}


//Content
function content()
{
	if ((isset($_GET['id']) && $_GET['id'] == $_SESSION['user_id']) || (isset($_POST['userid']) && $_POST['userid'] == $_SESSION['user_id']) || $_SESSION['user_type'] == 'admin')
	{
		if (isset($_GET['myComputers']))
		{
			displayComputers();
		}
		elseif (isset($_POST['editComputerPage']))
		{
			displayEditComputer();
		}
		elseif (isset($_POST['createComputerPage']))
		{
			displayEditComputer(true);
		}
	}
	else
	{
		header("Location: ./?show=frontpage&message=access_error");
	}
}
	
function displayComputers()
{
//System information
if ((isset($_GET['id']) && $_GET['id'] == $_SESSION['user_id']) || $_SESSION['user_type'] == 'admin')
{
	global $db;
	$stm = $db->prepare("SELECT systemInfo.id AS computerID, systemInfo.name AS cName, `CPU`, `GPU`, users.`name`,
	`userid`, users.id FROM `systemInfo` INNER JOIN `users` ON systemInfo.userid=users.id WHERE systemInfo.userid=:id");
	$stm->bindParam(':id', $id, PDO::PARAM_INT);
	$id = -1;
	if (ctype_digit($_GET['id']))
	{
		$id = $_GET['id'];
	}
	$stm->execute();
}
?>
	<div class="container">
		<?php 
		$nameNotWritten = true;
		while ($rs = $stm->fetch(PDO::FETCH_ASSOC)) { 
		if ($nameNotWritten) 
		{?>
		 <h2 style="color:white;"><?php echo $rs['name']; ?></h2>
		 		<form action="./?show=system_settings" method="post">
			<input type="hidden" name="user" value="<?php echo $rs['userid']; ?>"></input>
			<input type="hidden" name="createComputerPage" value="somethingLightside"></input>
			<input type="submit" class="btn btn-default" value="Tilføj ny computer"></input>
		</form>
		<?php $nameNotWritten = false; } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $rs['cName']; ?></h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-7 col-md-offset-2">
						<dl class="dl-horizontal">
							<dt>Navn</dt><dd><?php echo $rs['name']; ?></dd>
							<dt>CPU</dt><dd><?php echo $rs['CPU']; ?></dd>
							<dt>GPU</dt><dd><?php echo $rs['GPU']; ?></dd>	
							<dt>PSU</dt><dd>...</dd>
						</dl>
					</div>
					<div class="col-md-3">
						<form action="./?show=system_settings" method="post" style="display:inline;">
							<input type="hidden" name="editComputerPage" value="somethingDarkside"></input>
							<input type="hidden" name="computerID" value="<?php echo $rs['computerID']; ?>"></input>
							<input type="hidden" name="user" value="<?php echo $rs['userid']; ?>"></input>
							<input type="submit" class="btn btn-default" value="Redigér"></input>
						</form>
						<form action="./?show=system_settings" method="post" style="display:inline;">
							<input type="hidden" name="deleteComputer" value="somethingDarkside"></input>
							<input type="hidden" name="computerID" value="<?php echo $rs['computerID']; ?>"></input>
							<input type="hidden" name="user" value="<?php echo $rs['userid']; ?>"></input>
							<input type="submit" class="btn btn-default" value="Slet"></input>
						</form>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
<?php
}

function displayEditComputer($new = false)
{
if ($_POST['user'] == $_SESSION['user_id'] || $_SESSION['user_type'] == 'admin')
{
	if (!$new)
	{
	global $db;
	$stm = $db->prepare("SELECT systemInfo.id AS computerID, systemInfo.name AS cName, `CPU`, `GPU`, `PSU`, `motherboard`, `RAM`, `memory`, `monitor`, `mouse`, `keyboard`, `audio`, users.id AS id, users.`name` FROM `systemInfo` INNER JOIN `users` ON systemInfo.userid=users.id WHERE systemInfo.id=:id LIMIT 1");
	$stm->bindParam(':id', $id, PDO::PARAM_INT);
	$id = -1;
	if (ctype_digit($_POST['computerID']))
	{
		$id = $_POST['computerID'];
	}
	$stm->execute();
	$rs = $stm->fetch(PDO::FETCH_ASSOC);
	}
?>

<div class="container">
	<div class='panel panel-default'>
		<div class='panel-body'>
				<h1>Rediger <?php if (!$new) echo $rs['cName']; ?></h1>
				<form action="./?show=system_settings" method="post" style="display:inline;">
					<?php if ($new) { ?>
					<input type="hidden" name="createComputer" value="somethingLightside"></input>
					<?php } 
					else { ?>
					<input type="hidden" name="editComputer" value="somethingDarkside"></input>
					<input type="hidden" name="computerID" value="<?php echo $rs['computerID']; ?>"></input>
					<?php } ?>
					<input type="hidden" name="user" value="<?php echo $_POST['user']; ?>"></input>
					<div class="row">
						<div class="col-md-10">
							<div class="form-group">
								<label for="nameInput">Navn</label>
								<input type="text" name="name" class="form-control" id="nameInput" value="<?php if (!$new) echo $rs['cName']; ?>" required>
							</div>
							
							<div class="form-group">
								<label for="CPUinput">CPU</label>
								<input type="text" name="CPU" class="form-control" id="CPUinput" value="<?php if (!$new) echo $rs['CPU']; ?>">
							</div>
							
							<div class="form-group">
								<label for="GPUinput">GPU</label>
								<input type="text" name="GPU" class="form-control" id="GPUinput" value="<?php if (!$new) echo $rs['GPU']; ?>">
							</div>
							
							<div class="form-group">
								<label for="PSUinput">PSU</label>
								<input type="text" name="PSU" class="form-control" id="PSUinput" value="<?php if (!$new) echo $rs['PSU']; ?>">
							</div>
							
							<div class="form-group">
								<label for="motherboardinput">Motherboard</label>
								<input type="text" name="motherboard" class="form-control" id="motherboardinput" value="<?php if (!$new) echo $rs['motherboard']; ?>">
							</div>
							
							<div class="form-group">
								<label for="RAMinput">RAM</label>
								<input type="text" name="RAM" class="form-control" id="RAMinput" value="<?php if (!$new) echo $rs['RAM']; ?>">
							</div>
							
							<div class="form-group">
								<label for="memoryinput">Memory</label>
								<input type="text" name="memory" class="form-control" id="memoryinput" value="<?php if (!$new) echo $rs['memory']; ?>">
							</div>
							
							<div class="form-group">
								<label for="monitorinput">Monitor</label>
								<input type="text" name="monitor" class="form-control" id="monitorinput" value="<?php if (!$new) echo $rs['monitor']; ?>">
							</div>
							
							<div class="form-group">
								<label for="mouseinput">Mouse</label>
								<input type="text" name="mouse" class="form-control" id="mouseinput" value="<?php if (!$new) echo $rs['mouse']; ?>">
							</div>
							
							<div class="form-group">
								<label for="keyboardinput">Keyboard</label>
								<input type="text" name="keyboard" class="form-control" id="keyboardinput" value="<?php if (!$new) echo $rs['keyboard']; ?>">
							</div>
							
							<div class="form-group">
								<label for="audiinputo">Audio</label>
								<input type="text" name="audio" class="form-control" id="audioinput" value="<?php if (!$new) echo $rs['audio']; ?>">
							</div>
						</div>
					</div>
					
				<input type="submit" class="btn btn-primary" value="Gem">
				</form>
					<?php if (!$new) 
					{ ?>
					<form action="./?show=system_settings" method="post" style="display:inline;">
					<input type="hidden" name="user" value="<?php echo $rs['id']; ?>"></input>
					<input type="hidden" name="computerID" value="<?php echo $rs['computerID']; ?>"></input>
					<input type="hidden" name="deleteComputer" value="blablabla"></input>
					<input type="submit" value="Slet" class="btn btn-default"></input>
					<?php }
					else
					{ ?>
					<form action="./?show=system_settings&id=<?php echo $_POST['user']; ?>&myComputers" method="post" style="display:inline;">
					<input type="submit" value="Afbryd" class="btn btn-default"></input>
					<?php } ?>
				</form>
			</div>
		</div>
	</div>
<?php
}
}

//Javascript
function javascript(){
?>


<?php
}
?>