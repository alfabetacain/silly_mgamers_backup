<?php
$conf_stmt = $db->prepare("SELECT COUNT(*) c FROM `users` WHERE `confirmation_user` = :user AND `active` = '0';");
$conf_stmt->bindParam(':user',$_SESSION['user_id'],PDO::PARAM_INT);
$conf_stmt->execute();
$conf_count = $conf_stmt->fetch();
$conf_count = $conf_count['c'];
$conf_dom = ($conf_count == 0) ? '' : "<span class='badge'>$conf_count</span>";
?>
<br>
  <div class="navbar navbar-inverse navbar-static-top">
	<div class="navbar-header">
	  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	  </button>
	
	</div>
	<div class="navbar-collapse collapse">
		<a class="navbar-brand" href="./"><img src="img/mgaa3.png" alt="city" class="img-responsive" id="mglogo"></a>
		<div id = "buttons">
	  <ul class="nav navbar-nav">
				
				<li id = "forside" class="<?=(!isset($_GET['show']) || $_GET['show'] == 'frontpage')?'active':''?>"><a href="./"> Forside</a></li>
				<li id = "nyheder" class="<?=(isset($_GET['show']) && $_GET['show'] == 'news')?'active':''?>"><a href="./?show=news"> Nyheder</a></li>
				<li id = "arrangementer" class="<?=(isset($_GET['show']) && ($_GET['show'] == 'events' || $_GET['show'] == 'oldevents'))?'active':''?>"><a href="./?show=events"> Arrangementer</a></li>
				<li id = "medlemmer" class="<?=(isset($_GET['show']) && ($_GET['show'] == 'members' || $_GET['show'] == 'member'))?'active':''?>"><a href="./?show=members"> Medlemmer</a>
				</li>
	  </ul>
	  </div>
	  <div id = "knapper">
	  <ul class="nav navbar-nav navbar-right">
	    <?php if ($_SESSION['user_type'] == 'admin') { ?>
		<li class="dropdown <?=
			(isset($_GET['show']) && 
				($_GET['show'] == 'files' 
				|| $_GET['show'] == 'pictures' 
				|| $_GET['show'] == 'stats' 
				|| $_GET['show'] == 'events_admin' 
				|| $_GET['show'] == 'sms'))?'active':''?>">
		  <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Admin <b class="caret"></b></a>
		  
		  <ul class="dropdown-menu">
			<li class="disabled <?=(isset($_GET['show']) && $_GET['show'] == 'files')?'active':''?>"><a href="./?show=files"><span class='glyphicon glyphicon-folder-open'></span> Filer</a></li>
			<li class="disabled <?=(isset($_GET['show']) && $_GET['show'] == 'pictures')?'active':''?>"><a href="./?show=pictures"><span class='glyphicon glyphicon-picture'></span> Billeder</a></li>
			<li class="<?=(isset($_GET['show']) && $_GET['show'] == 'stats')?'active':''?>"><a href="./?show=stats"><span class='glyphicon glyphicon-stats'></span> Statistik</a></li>
			<li class="<?=(isset($_GET['show']) && $_GET['show'] == 'sms')?'active':''?>"><a href="./?show=sms"><span class='glyphicon glyphicon-send'></span> SMS'er</a></li>
			<!--<li class="<?=(isset($_GET['show']) && $_GET['show'] == 'payment')?'active':''?>"><a href="./?show=payment"><span class='glyphicon glyphicon-usd'></span> Indbetalinger</a></li>-->
			<li class="divider"></li>
			<!--<li class="dropdown-header">Indhold</li>-->
			<li class="<?=(isset($_GET['show']) && $_GET['show'] == 'events_admin')?'active':''?>"><a href="./?show=events_admin"><span class='glyphicon glyphicon-calendar'></span> Arrangementer</a></li>
			<li class="<?=(isset($_GET['show']) && $_GET['show'] == 'members_admin')?'active':''?>"><a href="./?show=members_admin"><span class='glyphicon glyphicon-user'></span> Medlemmer</a></li>
		  </ul>
		</li>
		<?php } ?>
		<?php if ($_SESSION['user_type'] != 'none') { ?>
		<li class="dropdown <?=(isset($_GET['show']) && ($_GET['show'] == 'settings' ))?'active':''?>">
		  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=$conf_dom?> Profil <b class="caret"></b></a>
		  <ul class="dropdown-menu">
			<?php if ($conf_count > 0){ ?><li class="<?=(isset($_GET['show']) && $_GET['show'] == 'confirm_user')?'active':''?>"><a href="./?show=confirm_user"><?=$conf_dom?> Bekræft nye medlemmer</a></li><?php } ?>
			<li class="<?=(isset($_GET['show']) && $_GET['show'] == 'member' && isset($_GET['id']) &&  $_GET['id'] == $_SESSION['user_id'])?'active':''?>"><a href="./?show=member&amp;id=<?=$_SESSION['user_id']?>"><span class='glyphicon glyphicon-user'></span> Min side</a></li>
			<li class="<?=(isset($_GET['show']) && $_GET['show'] == 'settings')?'active':''?>"><a href="./?show=settings"><span class='glyphicon glyphicon-cog'></span> Indstillinger</a></li>
			<li class="<?=(isset($_GET['show']) && $_GET['show'] == 'password')?'active':''?>"><a href="./?show=password"><span class='glyphicon glyphicon-lock'></span> Skift adgangskode</a></li>
			<!--<li class="<?=(isset($_GET['show']) && $_GET['show'] == 'my_payments')?'active':''?>"><a href="./?show=my_payments"><span class='glyphicon glyphicon-usd'></span> Mine indbetalinger</a></li>-->
			<li class="divider"></li>
			<li><a href="./login.php?action=logout"> Log ud</a></li>
		  </ul>
		</li>
		<?php } else { ?>
		<li><a data-toggle="modal" href="#login_modal"> Log ind</a></li>
		
		<?php } ?>
	  </ul>
	</div>
	<!--/.nav-collapse -->
  </div>	
 </div>	
  
<?php
 
	//Notification area

if (isset($_GET['message']))
{
	$title = '';
	$content = '';
	$type = ''; //success / danger / warning / info
	switch ($_GET['message'])
	{
		case 'reset':
			$title = 'Adgangskode nulstillet!';
			$content = 'Der er afsendt en email med din nye midlertidige adgangskode. Husk at tjekke dit spam filter!';
			$type = 'success';
			break;
		case 'reset_failed':
			$title = 'Ukendt email!';
			$content = 'Den angivne email blev ikke fundet i systemet! Prøv venligst igen.';
			$type = 'danger';
			break;
		case 'logout':
			$title = 'Logget ud!';
			$content = 'Du er nu logget ud.';
			$type = 'success';
			break;
		case 'create':
			$title = 'Oprettet!';
			$content = 'Din bruger er nu oprettet. Log ind ved at trykke på knappen i øverste højre hjørne når du er blevet bekræftet.';
			$type = 'success';
			break;
		case 'loginerror':
			$title = 'Login fejl!';
			$content = 'Ugyldig email eller adgangskode. Prøv igen.';
			$type = 'danger';
			break;
		case 'fill_info':
			$title = 'Udfyld profilinfo!';
			$content = 'Du skal udfylde som minimum: postnummer og kaldenavn';
			$type = 'warning';
			break;		
		case 'confirm_ok':
			$title = 'Medlem bekræftet!';
			$content = 'Det nye medlem er blevet godkendt!';
			$type = 'success';
			break;		
		case 'deny_ok':
			$title = 'Medlem afvist!';
			$content = 'Det nye medlem er blevet afvist!';
			$type = 'success';
			break;		
		case 'event_created':
			$title = 'Arrangement oprettet!';
			$content = 'Arrangementet er blevet oprettet.';
			$type = 'success';
			break;
		case 'event_updated':
			$title = 'Arrangement gemt!';
			$content = 'Dine ændringer til arrangementet er blevet gemt.';
			$type = 'success';
			break;
		case 'event_deleted':
			$title = 'Arrangement slettet!';
			$content = 'Det valgte arrangmenet blev slettet!';
			$type = 'success';
			break;
		case 'access_error':
			$title = 'Ingen adgang!';
			$content = 'Du har ikke adgang til den forespurgte side! Enten fordi du ikke er logget ind eller fordi siden kræver administratorrettigheder.';
			$type = 'warning';
			break;
		case 'userDeleted':
			$title = 'Medlemsdata slettet!';
			$content = 'Det valgte medlem er blevet slettet!';
			$type = 'success';
			break;
		case 'settingsok':
			$title = 'Indstillinger gemt!';
			$content = 'Dine ændringer er blevet gemt!';
			$type = 'success';
			break;
		case 'passwordchange_ok':
			$title = 'Adgangskode ændret!';
			$content = 'Din adgangskode er blevet opdateret!';
			$type = 'success';
			break;
		case 'passwordchange_mismatch':
			$title = 'Forskellige adgangskoder!';
			$content = 'De to indtastede adgangskoder var ikke ens!';
			$type = 'danger';
			break;
		case 'passwordchange_unknown':
			$title = 'Forkert adgangskode!';
			$content = 'Den indtastede gamle adgangskode var ikke korrekt! Prøv igen.';
			$type = 'danger';
			break;
		case 'pleasechange':
			$title = 'Skift din adgangskode!';
			$content = 'Du skal skifte din adgangskode til én du kan huske.';
			$type = 'warning';
			break;
		case 'logininactive':
			$title = 'Brugerkonto deaktiveret!';
			$content = 'Din brugerkonto er ikke blevet aktiveret. Kontakt mgamers hvis du har nogle spørgsmål.';
			$type = 'danger';
			break;
		case 'reservation_deleted':
			$title = 'Reservation slettet!';
			$content = 'Din reservation er blevet slettet!';
			$type = 'success';
			break;
		case 'reservation_saved':
			$title = 'Reservation gemt!';
			$content = 'Din reservation er gemt. Husk at angive hvilke nætter du overnatter samt hvilken plads du vil sidde på!';
			$type = 'success';
			break;
		case 'smsok':
			$title = 'SMSer afsendt ok!';
			$content = 'SMSerne er afsendt eller sat til afsendelse på det valgte tidspunkt.';
			$type = 'success';
			break;
		case 'smsfail':
			$title = 'SMS afsendelse fejlede!';
			$content = 'Besked fra CPSMS: <em>' . htmlentities($_SESSION['cpsms_response']).'</em>';
			$type = 'danger';
			break;
		case 'newsCreated':
			$title = 'Nyhed blev oprettet!';
			$content = 'Operationen lykkedes!';
			$type = 'success';
			break;
		case 'newsCreatedFail':
			$title = 'Nyhed blev ikke oprettet!';
			$content = 'Operationen fejlede!';
			$type = 'danger';
			break;
		case 'newsEdited':
			$title = 'Nyhed blev redigeret!';
			$content = 'Operationen lykkedes!';
			$type = 'success';
			break;
		case 'newsEditedFail':
			$title = 'Nyhed blev ikke redigeret!';
			$content = 'Operationen fejlede!';
			$type = 'danger';
			break;
		case 'newsDeleted':
			$title = 'Nyhed blev slettet!';
			$content = 'Operationen lykkedes!';
			$type = 'success';
			break;
		case 'newsDeletedFail':
			$title = 'Nyhed blev ikke slettet!';
			$content = 'Operationen fejlede!';
			$type = 'danger';
			break;
		case 'categoryCreated':
			$title = 'Kategori blev oprettet!';
			$content = 'Operationen lykkedes!';
			$type = 'success';
			break;
		case 'categoryCreatedFail':
			$title = 'Kategori blev ikke oprettet!';
			$content = 'Operationen fejlede!';
			$type = 'danger';
			break;
		case 'categoryEdited':
			$title = 'Kategori blev redigeret!';
			$content = 'Operationen lykkedes!';
			$type = 'success';
			break;
		case 'categoryEditedFail':
			$title = 'Kategori blev ikke redigeret!';
			$content = 'Operationen fejlede!';
			$type = 'danger';
			break;
		case 'categoryDeleted':
			$title = 'Kategori blev slettet';
			$content = 'Operationen lykkedes!';
			$type = 'success';
			break;
		case 'categoryDeletedFail':
			$title = 'Kategori blev ikke slettet!';
			$content = 'Operationen fejlede!';
			$type = 'danger';
			break;
		case 'frontpageUpdated':
			$title = 'Forsiden blev opdateret!';
			$content = 'Operationen lykkedes';
			$type = 'success';
			break;
		case 'frontpageUpdatedFail':
			$title = 'Forsiden blev ikke opdateret!';
			$content = 'Operationen fejlede';
			$type = 'danger';
			break;
		case 'invalidHTML':
			$title = 'Ulovlig kode fundet!';
			$content = 'Brug kun tilladte tags!';
			$type = 'warning';
			break;
		case 'userUpdated':
			$title = 'Bruger blev opdateret!';
			$content = 'Operationen lykkedes';
			$type = 'success';
			break;
		case 'userUpdatedFail':
			$title = 'Bruger blev ikke opdateret!';
			$content = 'Operationen fejlede';
			$type = 'danger';
			break;
		case 'successful':
			$title = 'SUCCESS!';
			$content = 'Operationen lykkkedes';
			$type = 'success';
			break;
		case 'unsuccessful':
			$title = 'FEJL!';
			$content = 'Operationen fejlede';
			$type = 'danger';
			break;
			
			
			
			
			
	}	
	
?>
<div class="alert alert-<?=$type?> alert-dismissable message-alert">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong><?=$title?></strong> <?=$content?>
</div>
	
<?php	
	
}
	
?> 
  
 
