<?php
$html_phone_regex = '(\+45)?[2-9][0-9]{7}';
$php_phone_regex = '/'.$html_phone_regex.'/';

$DANISH_MONTHS = array(1=>"januar", 2=>"februar", 3=>"marts", 4=>"april", 5=>"maj", 6=>"juni", 7=>"juli", 8=>"august", 9=>"september", 10=>"oktober", 11=>"november", 12=>"december");
$DANISH_MONTHS_CAP = array(1=>"Januar", 2=>"Februar", 3=>"Marts", 4=>"April", 5=>"Maj", 6=>"Juni", 7=>"Juli", 8=>"August", 9=>"September", 10=>"Oktober", 11=>"November", 12=>"December");

$IMAGE_SIDE_LENGTH = 200;

// Handle avatar uploads
// NOTE: Assumes cwd is /uploads
//Return
//	str - filename of uploaded file
// 	1-2	- file too large
//	3-8	- other error
//	9 - illegal file type (filename wrong or content wrong)
function handle_avatar_upload($form_name){
	//Initial validation
	if (!isset($_FILES[$form_name])) return 3;
	if ($_FILES[$form_name]['error'] != 0) return $_FILES[$form_name]['error'];
	
	//Is image validation
	@$size = getimagesize($_FILES[$form_name]['tmp_name']);
	if ($size[0] == 0) return 9;
	
	//Is proper image type validation (and loading of image into gd)
	if (preg_match('/\.jpe?g$/i',$_FILES[$form_name]['name']) && preg_match('|^image/p?jpe?g$|i',$_FILES[$form_name]['type'])){
		$im = imagecreatefromjpeg($_FILES[$form_name]['tmp_name']);
	}
	elseif (preg_match('/\.png$/i',$_FILES[$form_name]['name']) && preg_match('|^image/(x-)?png$|i',$_FILES[$form_name]['type'])){
		$im = imagecreatefrompng($_FILES[$form_name]['tmp_name']);
	}
	else{
		return 9;
	}
	
	//Resample image
	$result = imagecreatetruecolor($GLOBALS['IMAGE_SIDE_LENGTH'], $GLOBALS['IMAGE_SIDE_LENGTH']);
	imagecopyresampled ($result, $im, 0, 0, 0, 0, $GLOBALS['IMAGE_SIDE_LENGTH'], $GLOBALS['IMAGE_SIDE_LENGTH'], $size[0], $size[1]);
	
	//Find a filename
	$filename = hash('sha256',$_SERVER['REMOTE_ADDR'] . time()) . '.png';
	
	if (!is_dir('avatars'))
		mkdir('avatars');
	
	imagepng($result, "avatars/$filename");

	imagedestroy($im);
	imagedestroy($result);
	return $filename;
}

$VALID_TAGS = array('#text','html','body','strong','em','span','div','ul','ol','li','br','a','img','p');
$VALID_ATTRIBUTES = array('style','href','src','width','height');
$VALIDATOR_ERRORS = array();
$VALID_IFRAME_TAGS = array('width','height','frameborder','src','allowfullscreen');
$VALID_IFRAME_URL_REGEX = '#^((//)|(https?://))?www\.youtube\.com/embed/[a-zA-Z0-9]+$#';

//Returns array of errors. If empty, no errors
function validateWysiwygDom($markup){
	global $VALIDATOR_ERRORS;
	$VALIDATOR_ERRORS = array();
	
	if (strlen(trim($markup)) == 0) return $VALIDATOR_ERRORS; //return empty array
	
	set_error_handler('WysiwygValidatorErrorHandler');
	
	$dom = new DOMDocument();
	$dom->loadHTML($markup);
	
	restore_error_handler();
	
	return array_merge($VALIDATOR_ERRORS, validateWysiwygDomRec($dom));
}

function validateIframe($node)
{
	global $VALID_IFRAME_TAGS, $VALID_IFRAME_URL_REGEX;
	if ($node->attributes != null){
		foreach ($node->attributes as $name => $attrnode){
			if ($name == 'src' && !preg_match($VALID_IFRAME_URL_REGEX, $attrnode->value)){
				return false;
			}
			elseif (!in_array($name,$VALID_IFRAME_TAGS))
				return false;
			}
		}
	return true;
}
/*
<iframe width="420" height="315" src="//www.youtube.com/embed/iYcYaUBaEeI" frameborder="0" allowfullscreen></iframe>
									  //www.youtube.com/embed/4Y1iErgBrDQ
*/

//Internal recursing method
function validateWysiwygDomRec($dom){
	global $VALID_TAGS, $VALID_ATTRIBUTES;
	$errors = array();
	foreach ($dom->childNodes as $node){
		
		if ($node->nodeName == 'iframe' && validateIframe($node))
			continue;
		
		
		if (!in_array($node->nodeName,$VALID_TAGS)){
			$errors[] = "Illegal tag: " . $node->nodeName;
		}
			
		if ($node->attributes != null){
			foreach ($node->attributes as $name => $attrnode){
				if (!in_array($name,$VALID_ATTRIBUTES))
					$errors[] = "Illegal attribute: " . $name;
			}
		}
		
		if ($node->hasChildNodes())
			$errors = array_merge($errors, validateWysiwygDomRec($node));
	}
	return $errors;
}
//Internal DOM parsing error catching method
function WysiwygValidatorErrorHandler($errno, $errstr, $errfile, $errline, array $errcontext){
	global $VALIDATOR_ERRORS;
	if (strstr($errstr,'DOMDocument::loadHTML()'))
		$VALIDATOR_ERRORS[] = "Invalid markup: " . str_replace('DOMDocument::loadHTML():','',$errstr);
}


$city_error = false;
function getCityFromZip($zip){
	global $city_error;
	if (!preg_match('/^\d+$/',$zip)) return '';
	$url = "https://geo.oiorest.dk/postnumre/" . $zip . ".json";
	set_error_handler("getCityErrorHandler");
	$city_error = false;
	$json = file_get_contents($url);
	$obj = json_decode($json);
	restore_error_handler();
	if ($city_error) return '';
	return $obj->navn;
}

function getCityErrorHandler( $errno , $errstr , $errfile , $errline , $errcontext ){
	global $city_error;
	$city_error = true;
}


function getPOSTorGET($POSTorGET, $name, $legal_values = '', $default_value = '')
{
	if (is_array($legal_values))
	{
		return (isset($POSTorGET[$name]) && in_array($POSTorGET[$name],$legal_values)) ? $POSTorGET[$name] : $default_value;
	}
	else if($legal_values != '')
	{
		return (isset($POSTorGET[$name]) && preg_match($legal_values,$POSTorGET[$name])) ? $POSTorGET[$name] : $default_value;
	}
	else
	{
		return (isset($POSTorGET[$name])) ? $POSTorGET[$name] : $default_value;
	}
}

function getGET($name, $legal_values = '', $default_value = '')
{
	return getPOSTorGET($_GET, $name, $legal_values, $default_value);
}

function getPOST($name, $legal_values = '', $default_value = '')
{
	return getPOSTorGET($_POST, $name, $legal_values, $default_value);
}

function insertOrUpdate($table, $id, $key_values)
{
	if ($id == -1)
	{
		$keys = array(); 
		$values = array();
		foreach ($key_values as $key => $value)
		{
			$keys[] = "`$key`";
			$values[] = "'".mysql_real_escape_string($value)."'";
		}
		mysql_query("INSERT INTO `$table` (" . implode(',',$keys) . ") VALUES (" . implode(',',$values) . ");");
		return mysql_insert_id();
	}
	else
	{
		$kv_strings = array();
		foreach ($key_values as $key => $value)
		{
			$kv_strings[] = "`$key` = '" . mysql_real_escape_string($value) . "'";
		}
		mysql_query("UPDATE `$table` SET " . implode(',',$kv_strings) . " WHERE `id` = '$id' LIMIT 1;");
		return $id;
	}
}

function getDisplayDateTime($sql_date){
	return getDisplayDate($sql_date) . ' ' . getDisplayTime($sql_date);
}

function getFullDisplayDate($sql_date){
	$php_date = strtotime($sql_date);
	return getDisplayDate($sql_date) . ' ' . date('Y',$php_date);
}

function getDisplayDate($sql_date){
	global $DANISH_MONTHS;
	$php_date = strtotime($sql_date);
	return date('j',$php_date) . '. ' . $DANISH_MONTHS[date('n',$php_date)];
}

function getDisplayTime($sql_date){
	$php_date = strtotime($sql_date);
	return date("H:i", $php_date);
}

function season($php_date = null){
	if (!isset($php_date) || $php_date == null) $php_date = time();
	return (date('n',$php_date) <= 6) ? 'spring' : 'fall';
}

function generateSalt($length = 16) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $salt = '';
    for ($i = 0; $i < $length; $i++) {
        $salt .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $salt;
}

function hashPassword($password, $salt)
{
	return hash('sha256',$password . $salt);
}

function shortenName($name)
{
	$parts = explode(' ',$name);
	for ($i = 1; $i < count($parts)-1;$i++)
		$parts[$i] = strtoupper(substr($parts[$i],0,1)).'.';
	return implode(' ',$parts);
}

?>
