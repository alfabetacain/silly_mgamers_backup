<?php
/*
	Seating table generating code
	
	Requires session started!
*/

class SeatingTable {
	const MODE_NONE = 0, MODE_NORMAL = 1, MODE_ADMIN = 2;
	const LEFT = 1, RIGHT = 2;
	private $mode;
	private $event;
	private $return_url;
	
	private $reservations;
	private $nights;
	private $event_assoc;
	private $num_nights;
	private $current_reservation;
	
	public function __construct($event, $return_url, $mode = self::MODE_NONE){
		require_once("connectdb.php");
		global $db;
		//Setup
		$this->mode = $mode;
		$this->event = $event;
		$this->return_url = ($return_url == NULL || $return_url == '') ? './?' : $return_url;
		
		//Fetch event data
		$stmt = $db->prepare("SELECT * FROM `events` WHERE `id` = :id LIMIT 1;");
		$stmt->bindParam(':id',$this->event,PDO::PARAM_INT);
		$stmt->execute();
		$this->event_assoc = $stmt->fetch();
		
		//Fetch reservation data for current user
		if ($this->mode == self::MODE_NORMAL){
			$check_stmt = $db->prepare("SELECT * FROM `reservations` WHERE `event` = :event AND `user` = :user LIMIT 1;");
			$check_stmt->bindParam(':event',$this->event,PDO::PARAM_INT);
			$check_stmt->bindParam(':user',$_SESSION['user_id'],PDO::PARAM_INT);
			$check_stmt->execute();
			$this->current_reservation = $check_stmt->fetch();
		}
		
		//Calc # nights
		$this->num_nights = 0;
		$_start = strtotime($this->event_assoc['start']);
		$_end = strtotime($this->event_assoc['end']);
		while (date("z",$_start) < date("z",$_end)){
			$this->num_nights++;
			$_start += 60*60*24;
		}
	}
	
	public function form_submission(){
		if ($_SESSION['user_type'] == 'none') return;
		
		$reservation_position = getPOST('reservation_position','/^\d\.\d+$/',-1);

		if ($this->mode == self::MODE_NORMAL && $reservation_position != -1){		//Assign seat to self
			$this->form_submission_place_self($reservation_position);
		}		
		elseif ($this->mode == self::MODE_NORMAL && isset($_POST['delete'])){		//Remove self
			$this->delete_reservation($_SESSION['user_id']);
		}
		elseif ($this->mode == self::MODE_NORMAL && isset($_POST['nights_form'])){	//Assign nights to self
			$this->form_submission_nights_self();
		}
		elseif (isset($_POST['expand'])){											//Expand to double table
			$this->form_submission_expand_shrink('expand','double');
		}
		elseif (isset($_POST['shrink'])){											//Divide to single tables
			$this->form_submission_expand_shrink('shrink','single');
		}
		elseif ($this->mode == self::MODE_ADMIN && isset($_POST['move'])){			//Admin move(or add) reservation
			$this->form_submission_move_add();
		}
		elseif ($this->mode == self::MODE_ADMIN && isset($_POST['delete_admin'])){	//Admin delete reservation 
			$this->delete_reservation(getPOST('user','/^\d+$/',-1));
		}
		elseif ($this->mode == self::MODE_ADMIN && isset($_POST['nights_form'])){	//Admin change nights
			$this->form_submission_admin_nights();
		}
	}
	
	private function form_submission_admin_nights(){
		global $db;
		
		//Fetch and rearrange data
		if (!is_array($_POST['nights'])) return;
		$user_nights_int = array();
		foreach ($_POST['nights'] as $user => $nights){
			if (!is_array($nights)) return;
			$user_nights_int[$user] = 0;
			foreach ($nights as $night){
				$user_nights_int[$user] += 1 << $night;
			}
		}
		
		//Fill gaps (users not staying any nights)
		$stmt = $db->prepare("SELECT `user` FROM `reservations` WHERE `event` = :event;");
		$stmt->bindParam(':event',$this->event,PDO::PARAM_INT);
		$stmt->execute();
		while ($res = $stmt->fetch()){
			if (!isset($user_nights_int[$res['user']])) $user_nights_int[$res['user']] = 0;
		}
		
		//Store
		$stmt = $db->prepare("UPDATE `reservations` SET `nights` = :nights WHERE `event` = :event AND `user` = :user LIMIT 1;");
		$stmt->bindParam(':event',$this->event,PDO::PARAM_INT);
		foreach ($user_nights_int as $user => $nights){
			$stmt->bindParam(':user',$user,PDO::PARAM_INT);
			$stmt->bindParam(':nights',$nights,PDO::PARAM_INT);
			$stmt->execute();
			//echo "Doing user $user = $nights : ".$stmt->errorcode()." affecting ".$stmt->rowCount()." on event {$this->event}<br>\r\n";
		}
		header('Location: ' . $this->return_url);
		die("Updated nights");
	}
	
	private function form_submission_move_add(){
		$user = getPOST('user','/^\d+$/',-1);
		$to_location = getPOST('to_location','/^\d\.\d+$/',-1);
		
		global $db;
		$reservation_position = explode('.',$to_location);
		$table = $reservation_position[0];
		$seat = $reservation_position[1];
		
		$check_stmt = $db->prepare("SELECT * FROM `reservations` WHERE `event` = :event AND `user` = :user LIMIT 1;");
		$check_stmt->bindParam(':event',$this->event,PDO::PARAM_INT);
		$check_stmt->bindParam(':user',$user,PDO::PARAM_INT);
		$check_stmt->execute();
		$check = $check_stmt->fetch();
		
		if ($check === false){ //Create
			$stmt = $db->prepare("INSERT INTO `reservations` (`user`,`event`,`table`,`seat`) VALUES (:user, :event, :table, :seat);");
		}
		else{ //Update
			$stmt = $db->prepare("UPDATE `reservations` SET `table` = :table, `seat` = :seat, `size` = 'single' WHERE `event` = :event AND `user` = :user;");
		}
		$stmt->bindParam(':user',$user,PDO::PARAM_INT);
		$stmt->bindParam(':event',$this->event,PDO::PARAM_INT);
		$stmt->bindParam(':table',$table,PDO::PARAM_INT);
		$stmt->bindParam(':seat',$seat,PDO::PARAM_INT);
		$stmt->execute();
		header('Location: ' . $this->return_url);
		die("Moved user");
	}
	
	private function form_submission_expand_shrink($expand_shrink, $double_single){
		$pos = getPOST($expand_shrink,'/^\d\.\d+$/',-1);
		global $db;
		if ($this->mode == self::MODE_ADMIN /*|| ($this->mode == self::MODE_NORMAL && $pos == $this->current_reservation['table'] . '.' . $this->current_reservation['seat'])*/){
			$pos = explode('.',$pos);
			$table = $pos[0];
			$seat = $pos[1];
			
			//Move reservation up if expanding a bottom table
			$new_seat = ($seat % 4 == 0 || ($seat+1) % 4 == 0) ? $seat - 2 : $seat;
			
			$stmt = $db->prepare("UPDATE `reservations` SET `seat` = :new_seat, size = :size WHERE `event` = :event AND `table` = :table AND `seat` = :old_seat LIMIT 1;");
			$stmt->bindParam(':event',$this->event,PDO::PARAM_INT);
			$stmt->bindParam(':table',$table,PDO::PARAM_INT);
			$stmt->bindParam(':old_seat',$seat,PDO::PARAM_INT);
			$stmt->bindParam(':new_seat',$new_seat,PDO::PARAM_INT);
			$stmt->bindParam(':size',$double_single,PDO::PARAM_INT);
			$stmt->execute();
			
			header('Location: ' . $this->return_url);
			die("$expand_shrink done. e:{$this->event} t:$table o:$seat n:$new_seat s:$double_single");
		}
	}
	
	private function delete_reservation($user_to_delete){
		global $db;
		$stmt = $db->prepare("DELETE FROM `reservations` WHERE `user` = :user AND `event` = :event LIMIT 1;");
		$stmt->bindParam(':user',$user_to_delete,PDO::PARAM_INT);
		$stmt->bindParam(':event',$this->event,PDO::PARAM_INT);
		$stmt->execute();
		header('Location: ' . $this->return_url . '&message=reservation_deleted');
		die("Reservation deleted");
	}
	
	private function form_submission_place_self($reservation_position){
		global $db;
		$reservation_position = explode('.',$reservation_position);
		$table = $reservation_position[0];
		$seat = $reservation_position[1];
		
		if ($this->current_reservation === false){ //Create
			$stmt = $db->prepare("INSERT INTO `reservations` (`user`,`event`,`table`,`seat`) VALUES (:user, :event, :table, :seat);");
		}
		else{ //Update
			$stmt = $db->prepare("UPDATE `reservations` SET `table` = :table, `seat` = :seat WHERE `event` = :event AND `user` = :user;");
		}
		$stmt->bindParam(':user',$_SESSION['user_id'],PDO::PARAM_INT);
		$stmt->bindParam(':event',$this->event,PDO::PARAM_INT);
		$stmt->bindParam(':table',$table,PDO::PARAM_INT);
		$stmt->bindParam(':seat',$seat,PDO::PARAM_INT);
		$stmt->execute();
		
		header('Location: ' . $this->return_url . '&message=reservation_saved');
		die("Reservation complete");
	}
	
	private function form_submission_nights_self(){
		$nights = (isset($_POST['nights']) && is_array($_POST['nights'])) ? $_POST['nights'] : array();
		$nights_int = 0;
		foreach ($nights as $night){
			$nights_int += (1 << $night);
		}
		
		global $db;
		if ($this->current_reservation === false){ //Create
			$stmt = $db->prepare("INSERT INTO `reservations` (`user`,`event`,`table`,`seat`,`nights`) VALUES (:user, :event, -1,-1, :nights);");
		}
		else{ //Update
			$stmt = $db->prepare("UPDATE `reservations` SET `nights` = :nights WHERE `event` = :event AND `user` = :user;");
		}
		$stmt->bindParam(':user',$_SESSION['user_id'],PDO::PARAM_INT);
		$stmt->bindParam(':event',$this->event,PDO::PARAM_INT);
		$stmt->bindParam(':nights',$nights_int,PDO::PARAM_INT);
		$stmt->execute();
		
		header('Location: ' . $this->return_url . '&message=reservation_saved');
		die("Nights entered successfully");
		
	}
	
	public function javascript(){
		?>
	<script type="text/javascript">
		function applyDraggable(){
			$(".draggable_name").draggable({
				revert:'invalid', 
				opacity: 0.7, 
				helper: "clone", 
				cursor:"move"
				}
			);
		}
		applyDraggable();
		$("#deleteReservationArea").droppable({
			accept:'.draggable_name',
			tolerance:'touch',
			activeClass:'btn-warning',
			hoverClass:'btn-danger',
			drop:function(event, ui){
				$('#deleteFormUser').val(ui.draggable.data('value'));
				$('#deleteForm').submit();
			}
		});
		$(".droppable_seat").droppable({
			accept:'.draggable_name',
			tolerance:'touch',
			activeClass:'btn-warning',
			hoverClass:'btn-success',
			drop:function(event, ui){
				$('#moveFormUser').val(ui.draggable.data('value'));
				$('#moveFormToLocation').val($(this).data('value'));
				$('#moveForm').submit();
			},
			over:function(event, ui){$(this).html("Flyt hertil");},
			out:function(event, ui){$(this).html("Ledig plads");}
		});
	<?php if ($this->mode == self::MODE_ADMIN){ ?>
		var admin_search_timer;
		$("#admin_add_search").on('keypress', function(){
			if (admin_search_timer) clearTimeout(admin_search_timer);
			admin_search_timer = setTimeout(function(){
				$("#search_results").html("<img src='img/ajax-loader.gif'>");
				$.getJSON('ajax/user_search.php',{search:$("#admin_add_search").val(),exclude_event_id:<?=$this->event?>},function(data){
					var items = [];
					$.each(data, function(k, val){
						items.push("<div class='label label-default draggable_name' data-value='" + val.id + "'>" + val.name + "</div>");
					});
					if (items.length == 0)
						$("#search_results").html("<span>Ingen resultater</span>");
					else
						$("#search_results").html(items.join(" "));
					applyDraggable();
				});
			},500);
		});
		
	<?php } ?>
	</script>
	<?php 
	}
	
	public function content(){
		$this->prepareContent();
		
		if ($this->mode == self::MODE_ADMIN){ 
			$this->showAdminAddLine();
		}
		
		$this->showTables();
		
		$unseated = $this->getUnseated();
		if ($this->mode == self::MODE_ADMIN && count($unseated) > 0){
			$this->showUnseated($unseated);
		}
		
		$this->showNights();
	}
	///////////////////////////////////////////////////
	// PRIVATE METHODS BEGIN
	///////////////////////////////////////////////////
	
	private function prepareContent(){
		global $db;
		
		//Build reservations table
		$reservations_stmt = $db->prepare("SELECT r.*, u.name name, u.id userid FROM `reservations` r, `users` u WHERE u.`id`=r.`user` AND r.`event` = :event");
		$reservations_stmt->bindParam(':event',$this->event,PDO::PARAM_INT);
	
		$this->reservations = array();
		$reservations_stmt->execute();
		while ($res = $reservations_stmt->fetch())
		{
			$this->reservations[$res['table'].'.'.$res['seat']] = $res;
		}
		
		//Build nights table
		$this->nights = array();
		$j = 1;
		for ($i = 0; $i < $this->num_nights; $i++){
			$this->nights[$i] = array();
			foreach ($this->reservations as $res){
				if (($res['nights'] & $j) != 0)
					$this->nights[$i][] = $res;
			}
			$j = $j << 1;
		}
	}
	
	private function showAdminAddLine(){ ?>
			<form id='moveForm' action='<?=htmlentities($this->return_url)?>' method='post'>
				<input type='hidden' id='moveFormUser' name='user' value=''/>
				<input type='hidden' id='moveFormToLocation' name='to_location' value=''/>
				<input type='hidden' name='move' value='move'/>
			</form>
			<form id='deleteForm' action='<?=htmlentities($this->return_url)?>' method='post'>
				<input type='hidden' id='deleteFormUser' name='user' value=''/>
				<input type='hidden' name='delete_admin' value='delete_admin'/>
			</form>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<form class="form-inline" action="#" onsubmit="return false;" >
								<div class="form-group">
									<strong>Find og tilf&oslash;j:</strong>
								</div>
								<div class="form-group">
									<input type="search" class="form-control" id="admin_add_search" placeholder="Søg på navn">
								</div>
								<div class="form-group" id="search_results">
								</div>
								<div class="form-group pull-right">
									<button id='deleteReservationArea' class='btn btn-default'>
										<span class="glyphicon glyphicon-trash"></span>
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		<?php
	}
	
	private function showTables(){ ?>
		<form method='post' action='<?=htmlentities($this->return_url)?>'>
			<input type="hidden" name="event" value="<?=$this->event?>" />
			<div class="row">
				<?php for($table = 1; $table <= 2; $table++){?>
				<div class="borde">
					<div class='col-lg-6'>
						<div class="panel panel-default">
						<div class="panel-heading"><h3 class="panel-title">Bord <?=$table?></h3></div>
						<table class='table table-table' style="table-layout:fixed;">
							<col>
							<col class="name_column">
							<col>
							<col>
							<col class="name_column">
							<col>
						<?php 
							for($row = 0; $row < $this->event_assoc['rows']; $row+=2)
								$this->createRowPair($table, $row);
						?>
						</table>
					</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</form>
		<?php
	}
	
	private function showUnseated($unseated){ ?>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<form class="form-inline">
							<div class="form-group">
								<strong>Tilmeldt uden plads: </strong>
							</div>
							<div class="form-group">
								<?php
									foreach ($unseated as $res){
										?><div class='label label-default draggable_name' data-value='<?=$res['user']?>'><?=$res['name']?></div> <?php 
									} ?>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<?php
	} 
	
	private function getUnseated(){
		$unseated = array();
		foreach ($this->reservations as $res){
			if ($res['table'] < 1 || $res['table'] > 2 || $res['seat'] < 1 || $res['seat'] > $this->event_assoc['rows']*2)
				$unseated[] = $res;
		}
		return $unseated;
	}
	
	///////////////////////////////////////////////////
	//Nights
	///////////////////////////////////////////////////
	
	private function showNights(){
		switch ($this->num_nights){
			case 1: $col_count = 12; break;
			case 2: $col_count = 6; break;
			case 3: $col_count = 4; break;
			default: $col_count = 3; break;
		}
		
		?>
		<form method="post" action="<?=htmlentities($this->return_url)?>">
		<input type="hidden" name="nights_form" value="nights_form" />
        <div class="overnatninger">

		<div class="container well">
			<h2>Overnatninger</h2>
			<?php
			if ($this->mode == self::MODE_ADMIN) {
				?><p>Rediger overnatninger ved at tilføje/fjerne flueben. Husk at gemme.</p><?php 
			}
			elseif ($this->mode == self::MODE_NORMAL){
				?><p>Af hensyn til brandmyndighederne skal vi vide hvilke nætter du overnatter.</p>
				<p>Sæt flueben ved dit navn under de nætter hvor du overnatter og tryk så på "Gem overnatninger".</p><?php
			}
			elseif ($this->mode == self::MODE_NONE) {
				?><p>Af hensyn til brandmyndighederne skal vi vide hvilke nætter hvem overnatter.</p><?php 
			}
			
				if ($this->num_nights == 0){
					?><p>Der er ingen overnatninger ved dette arrangement<?php
				}
				else
				{
					$cur_date = strtotime($this->event_assoc['start']);
					?>
					<div class="row">
					<?php
						for($day = 0; $day < $this->num_nights; $day++) {
							$next_date = $cur_date + 24*60*60;
							?>
								<div class="col-md-<?=$col_count?>">
									<div class="panel panel-default">
										<div class="panel-heading">Nat <?=$day+1?> (<?=date("j/m",$cur_date)?>-<?=date("j/m",$next_date)?>) </div>
										<div class="panel-body">
										<?php $this->showNight($day); ?>
									</div>
								</div>
							</div>
							<?php
							$cur_date = $next_date;
						}
					?>
					</div>
				<?php } ?>
					
			<div class="row">
				<div class="col-md-12">
					<?php if ($this->mode != self::MODE_NONE){ ?><input type="submit" value="Gem overnatninger" class="gemovernatninger btn btn-primary"/><?php } ?>
					<?php if ($this->mode == self::MODE_NORMAL){ ?><button class="btn btn-danger pull-right" name="gemovernatninger delete" value="delete">Slet reservation</button><?php } ?>
				</div>
			</div>
		</div>
	</div>
		</form>
	<?php
	}
	
	private function showNight($day){
		if ($this->mode == self::MODE_ADMIN)
			$this->showNightAdmin($day);
		elseif ($this->mode == self::MODE_NORMAL)
			$this->showNightNormal($day);
		elseif ($this->mode == self::MODE_NONE)
			$this->showNightNone($day);
	}
	
	private function showNightNone($day){
		$cur_res_int = ($this->current_reservation === false) ? 0 : $this->current_reservation['nights'];
		$checked = (($cur_res_int & (1 << $day)) != 0) ? 'checked' : '';
		
		?><ul class="list"><?php
		foreach ($this->nights[$day] as $res){ ?>
			<li><?=$res['name']?></li>
		<?php }
		?></ul><?php
	}
	
	private function showNightNormal($day){
		$cur_res_int = ($this->current_reservation === false) ? 0 : $this->current_reservation['nights'];
		$checked = (($cur_res_int & (1 << $day)) != 0) ? 'checked' : '';
		?>
		<div class="checkbox">
			<label>
				<input type="checkbox" <?=$checked?> name="nights[]" value="<?=$day?>"> <strong><?=$_SESSION['user_name']?></strong>
			</label>
		</div>
		<?php 
			foreach ($this->nights[$day] as $res){
				if ($res['userid'] != $_SESSION['user_id']){
					?>
					<div class="checkbox">
						<label>
							<input type="checkbox" checked disabled> <?=$res['name']?>
						</label>
					</div>
				<?php }
			}
	}
	
	private function showNightAdmin($day){
		foreach ($this->reservations as $res){ ?>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="nights[<?=$res['user']?>][]" value="<?=$day?>" <?=(($res['nights'] & (1 << $day)) != 0)?'checked':''?>> <?=$res['name']?>
				</label>
			</div>
		<?php }
	}
	
	///////////////////////////////////////////////////
	//Tables
	///////////////////////////////////////////////////
	
	private function isDoublePos($position){
		return (isset($this->reservations[$position]) && $this->reservations[$position]['size'] == 'double');
	}
	
	private function canExpandPos($position){
		$ex = explode('.',$position);
		$table = $ex[0];
		$seat = $ex[1];
		$seat = ($seat % 4 == 0 || ($seat+1) % 4 == 0) ? $seat - 2 : $seat;
		return (!isset($this->reservations[$table.'.'.($seat)])) ^ (!isset($this->reservations[$table.'.'.($seat+2)]));
	}
	
	private function createRowPair($table, $row)
	{	
		echo "<tr>";
			$this->createLeft($table.'.'.($row*2+1));
			$this->createRight($table.'.'.($row*2+2));
		echo "</tr>";
		
		echo "<tr>";
			if (!$this->isDoublePos($table.'.'.($row*2+1)))
				$this->createLeft($table.'.'.(($row+1)*2+1));
			if (!$this->isDoublePos($table.'.'.($row*2+2)))
				$this->createRight($table.'.'.(($row+1)*2+2));
		echo "</tr>";
	}

	private function createLeft($position)
	{
		$rowspan = ($this->isDoublePos($position)) ? 'rowspan=\'2\'' : '';
	?>
		<td <?=$rowspan?> class="left">
			<?php $this->createDropdown($position);?>
		</td>
		<td <?=$rowspan?> class="left">
			<?php $this->createPosition($position);?>
		</td>
		<td <?=$rowspan?> class="left">
			<span class='badge'><?=$position?></span>
		</td>
	<?php
	}

	private function createRight($position)
	{
		$rowspan = ($this->isDoublePos($position)) ? 'rowspan=\'2\'' : '';
	?>
		<td <?=$rowspan?>>
			<span class='badge'><?=$position?></span>
		</td>
		<td <?=$rowspan?>>
			<?php $this->createPosition($position);?>
		</td>
		<td <?=$rowspan?>>
			<?php $this->createDropdown($position);?>
		</td>
	<?php
	}

	private function createDropdown($position)
	{
		if ($this->mode == self::MODE_ADMIN /*|| ($this->mode == self::MODE_NORMAL && $position == $this->current_reservation['table'] . '.' . $this->current_reservation['seat'])*/)
		{
			if (!isset($this->reservations[$position])) return; //No button for empty seats
			
			if($this->isDoublePos($position)){?>
				<button class="btn btn-xs btn-default hidden-print" title="Opdel til enkeltborde" name="shrink" value="<?=$position?>"><span class="glyphicon glyphicon-resize-small"></span></button>
			<?php } elseif ($this->canExpandPos($position)){?>
				<button class="btn btn-xs btn-default hidden-print" title="Udvid til dobbeltbord" name="expand" value="<?=$position?>"><span class="glyphicon glyphicon-resize-full"></span></button>
			<?php }
		}
	}


	private function createPosition($position)
	{
		if (isset($this->reservations[$position]))
		{
			$reservation_class = ($this->reservations[$position]['userid'] == $_SESSION['user_id']) ? 'label-primary' : 'label-default';
			$name = shortenName($this->reservations[$position]['name']);
			$id = $this->reservations[$position]['userid'];
			
			if ($this->mode == self::MODE_NONE)
				echo "<span class='name_label'>$name</span>";
			else if ($this->mode == self::MODE_NORMAL)
				echo "<div class='label $reservation_class' data-value='$id'>$name</div>";
			else if ($this->mode == self::MODE_ADMIN)
				echo "<div class='label $reservation_class draggable_name' data-value='$id'>$name</div>";
		} 
		else 
		{
			if ($this->mode == self::MODE_NONE){
				//Nothing to show
			}
			else if ($this->mode == self::MODE_NORMAL)
				echo "<button class='btn btn-xs btn-default hidden-print' name='reservation_position' value='$position'>V&aelig;lg plads</button>";
			else if ($this->mode == self::MODE_ADMIN)
				echo "<div class='btn btn-xs btn-default droppable_seat hidden-print' data-value='$position'>Ledig plads</div>";
		}
	}
	
}

?>