<?php
$access_control = array();
//////////////////
// Access control
//
// To add access control to a page, add an entry with the page handle as key, 
// and minimum required access level (e.g. none, normal or admin) as value.
// By standard all pages are allowed for everyone
//////////////////
$access_control['events_admin'] = 'admin';
$access_control['members_admin'] = 'admin';
$access_control['sms'] = 'admin';
$access_control['settings'] = 'normal';
$access_control['password'] = 'normal';
$access_control['member'] = 'normal';
$access_control['confirm_user'] = 'normal';
$access_control['my_payments'] = 'normal';
$access_control['system_settings'] = 'normal';




//////////////////
// Access control end
//////////////////
$sl_access_order = array('none' => 0, 'normal' => 1, 'admin' => 2);
$show = (isset($_GET["show"]) && preg_match('/^[a-z_]+$/',$_GET["show"])) ? $_GET["show"] : 'frontpage';
if (isset($access_control[$show]) && $sl_access_order[$access_control[$show]] > $sl_access_order[$_SESSION['user_type']])
{
	$show = 'frontpage';
	$_GET['message'] = "access_error";
}
include("pages/$show.php");
?>