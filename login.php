<?php
error_reporting(E_ALL);
session_start();
require_once('php/connectdb.php');
require_once('php/util.php');
date_default_timezone_set("Europe/Copenhagen");

$_SESSION['login_return_query'] = (isset($_SESSION['login_return_query'])) ? $_SESSION['login_return_query'] : 'no_query_set';

$_POST['action'] = isset($_POST['action']) ? $_POST['action'] : '';
$_GET['action'] = isset($_GET['action']) ? $_GET['action'] : '';

function tryImportFromOldDB($email, $raw_password){
	/*
	global $db;
	$old_db = new PDO("mysql:host=mysql1061.servage.net;dbname=rasmusgreve2","rasmusgreve2","JCaBsheep");
	//$old_db = new PDO("mysql:host=localhost;dbname=rasmusgreve2","root","");
	$stmt = $old_db->prepare("SELECT * FROM `users` WHERE `email` = :email AND `password` = :password LIMIT 1;");
	$pw = hash('sha256',$raw_password);
	$stmt->bindParam(':email',$email,PDO::PARAM_STR);
	$stmt->bindParam(':password',$pw,PDO::PARAM_STR);
	$stmt->execute();
	$rs = $stmt->fetch();
	if ($rs !== false){
		$salt = generateSalt();
		$new_pw = hashPassword($raw_password,$salt);
		if ($rs['usertype'] != 'admin') $rs['usertype'] = 'normal';
		
		$in_stmt = $db->prepare("INSERT INTO `users` (`salt`,`password`,`email`,`name`,`nick`,`phone`,`address`,`zip`,`type`,`steam`,`skype`,`registration_date`) VALUES (:salt,:password,:email,:name,:nick,:phone,:address,:zip,:type,:steam,:skype,:reg_date)");
		$in_stmt->bindParam(':salt',$salt,PDO::PARAM_STR);
		$in_stmt->bindParam(':password',$new_pw,PDO::PARAM_STR);
		$in_stmt->bindParam(':email',$rs['email'],PDO::PARAM_STR);
		$in_stmt->bindParam(':name',$rs['name'],PDO::PARAM_STR);
		$in_stmt->bindParam(':nick',$rs['nick'],PDO::PARAM_STR);
		$in_stmt->bindParam(':phone',$rs['number'],PDO::PARAM_STR);
		$in_stmt->bindParam(':address',$rs['address'],PDO::PARAM_STR);
		$in_stmt->bindParam(':zip',$rs['zipcode'],PDO::PARAM_STR);
		$in_stmt->bindParam(':type',$rs['usertype'],PDO::PARAM_STR);
		$in_stmt->bindParam(':steam',$rs['steamID'],PDO::PARAM_STR);
		$in_stmt->bindParam(':skype',$rs['skypeID'],PDO::PARAM_STR);
		$reg_date = date("Y-m-d H:i:s");
		$in_stmt->bindParam(':reg_date',$reg_date,PDO::PARAM_STR);
		$in_stmt->execute();
		// $systemStm = $db->prepare("INSERT INTO `systemInfo` (`userid`) VALUES (:uID)");
		// $systemStm->bindParam(':uID', $uID, PDO::PARAM_INT);
		// $uID = $db->lastInsertID('id');
		// $systemStm->execute();
		return $salt;
	}
	 */
	return false;
}

if ($_POST['action'] == 'login')
{
	sleep(2); //Delay execution to prevent automated scripts trying to log in
	
	//Fetch user
	$user_stmt = $db->prepare("SELECT * FROM `users` WHERE `email` = :email LIMIT 1;");
	//reset_time > :time
	$user_stmt->bindParam(':email', $_POST['email'], PDO::PARAM_STR);
	$user_stmt->execute();
	$user = $user_stmt->fetch();
	$success = $user != null && (
		password_verify($_POST['password'], $user['password']) || 
		($_POST['password'] === $user['reset_password'] && strtotime($user['reset_time']) > time())
	);
	
	//Check login
	if (!$success)
	{
		header("Location: ./?{$_SESSION['login_return_query']}&message=loginerror&notfound");
		die("User not found");
	}
	else //Login correct
	{
		if ($user['active'] == 0){
			header("Location: ./?{$_SESSION['login_return_query']}&message=logininactive");
			die("Inactive");
		}
	
		session_regenerate_id(true);
		$_SESSION['user_type'] = $user['type'];
		$_SESSION['user_id'] = $user['id'];
		$_SESSION['user_name'] = $user['name'];
		
		
		//Remember
		if (isset($_POST['remember']))
		{
			$cookie_key = generateSalt(64);
			setcookie("remember_id", $user['id'], time()+60*60*24*365);
			setcookie("remember_key", $cookie_key, time()+60*60*24*365);
			
			$cookie_stmt = $db->prepare("UPDATE `users` SET `cookie_key` = :cookie_key WHERE `id` = :id LIMIT 1;");
			$cookie_stmt->bindParam(':id',$user['id'],PDO::PARAM_INT);
			$cookie_stmt->bindParam(':cookie_key',$cookie_key,PDO::PARAM_STR);
			$cookie_stmt->execute();
		}
		
		//Reset 'reset password' on normal login
		if ($user['reset_password'] != $_POST['password']){
			$reset_stmt = $db->prepare("UPDATE `users` SET `reset_password` = '', `reset_time` = '0000-00-00 00:00:00' WHERE `id` = :id LIMIT 1;");
			$reset_stmt->bindParam(':id',$user['id'],PDO::PARAM_INT);
			$reset_stmt->execute();
			
			if ($user['zip'] == '' || $user['nick'] == '' || $user['nick'] == 'Unknown') 
			{
				header("Location: ./?show=settings&message=fill_info");
			}
			else
			{
				header("Location: ./?{$_SESSION['login_return_query']}");
			}
		}
		else //Redirect to password change form when logging in using the reset password
		{
			header("Location: ./?show=password&prefill=".urlencode($_POST['password'])."&message=pleasechange");
		}
		die("Log in successful");
	}
}
elseif ($_GET['action'] == 'cookie_login'){
	$stmt = $db->prepare("SELECT * FROM `users` WHERE `id` = :id AND `cookie_key` = :cookie_key LIMIT 1;");
	$stmt->bindParam(':id',$_COOKIE['remember_id'],PDO::PARAM_INT);
	$stmt->bindParam(':cookie_key',$_COOKIE['remember_key'],PDO::PARAM_STR);
	$stmt->execute();
	$user = $stmt->fetch();
	if ($user == null)
	{
		setcookie("remember_id","",time()+2);
		setcookie("remember_key","",time()+2);
		
		header("Location: ./?{$_SESSION['login_return_query']}");
		die("Cookie login failed. Shouldn't happen again");
	}
	else
	{
		session_regenerate_id(true);
		$_SESSION['user_type'] = $user['type'];
		$_SESSION['user_id'] = $user['id'];
		$_SESSION['user_name'] = $user['name'];
		
		if ($user['zip'] == '' || $user['nick'] == '' || $user['nick'] == 'Unknown') 
		{
			header("Location: ./?show=settings&message=fill_info");
		}
		else
		{
			header("Location: ./?{$_SESSION['login_return_query']}");
		}
	}
}
elseif ($_POST['action'] == 'logout' || $_GET['action'] == 'logout')
{
	$_SESSION['user_type'] = 'none';
	$_SESSION['user_id'] = -1;
	$_SESSION['user_name'] = '';
	setcookie("remember_id","",time()+2);
	setcookie("remember_key","",time()+2);
	session_destroy();
	header("Location: ./?{$_SESSION['login_return_query']}&message=logout");
	die("Logged out");
}
elseif ($_POST['action'] == 'reset')
{
	sleep(5); //Delay execution to prevent automated scripts scraping for valid emails
	
	$reset_password = generateSalt(16);
	
	$reset_stmt = $db->prepare("UPDATE `users` SET `reset_password` = :reset_password, `reset_time` = :time WHERE `email` = :email LIMIT 1;");
	$reset_stmt->bindParam(':reset_password',$reset_password,PDO::PARAM_STR);
	$reset_stmt->bindParam(':time',date( 'Y-m-d H:i:s', time() + 60*60*4),PDO::PARAM_STR);
	$reset_stmt->bindParam(':email',$_POST['email'],PDO::PARAM_STR);
	
	$reset_stmt->execute();
	
	if ($reset_stmt->rowCount() == 0){
		header("Location: ./?{$_SESSION['login_return_query']}&message=reset_failed");
		die("Password reset mail failed!");
	}
	
	$name_stmt = $db->prepare("SELECT `name` FROM `users` WHERE `email` = :email LIMIT 1;");
	$name_stmt->bindParam(':email',$_POST['email'],PDO::PARAM_STR);
	$name_stmt->execute();
	$name = $name_stmt->fetchColumn();
	
	$email = str_replace(array("\r","\n","<",">",":",";"),'',$_POST['email']);
	
	$boundary = uniqid('np');
	
$mail_content_plain = <<<EOT
Hej $name
Der er blevet bedt om en nulstilling af adgangskoden for $email.
Hvis du ikke har bedt om dette, kan du blot se bort fra denne mail.

Din nye midlertidige adgangskode er $reset_password

Gå til mgamers.dk inden 1 time og log ind med din midlertidige adgangskode.
Husk at ændre din kode når du har logget ind.

Venlig hilsen
MGamers

Bemærk: Denne mail er afsendt automatisk og kan ikke besvares.
EOT;

	
	
$mail_content_html = <<<EOT
<html>
<body>
<div style="font-family:Helvetica, Arial, sans-serif;">
	<h1>MGamers</h1>
	<strong style="color:#333">Nulstilling af adgangskode</strong>
	<hr/>
	<p>Hej $name</p>
	<p>Der er blevet bedt om en nulstilling af adgangskoden for $email.</p>
	<p>Hvis du ikke har bedt om dette, kan du blot se bort fra denne mail.</p>
	<br/>
	<p>Din nye midlertidige adgangskode er <strong>$reset_password</strong></p>
	<br/>
	<p>Gå til <a href="https://mgamers.dk">mgamers.dk</a> inden 1 time og log ind med din midlertidige adgangskode.</p>
	<p>Husk at ændre din kode når du har logget ind.</p>
	<br/>
	<p>Venlig hilsen</p>
	<p>MGamers</p>
	<br/>
	<p style="font-style:italic;">Bemærk: Denne mail er afsendt automatisk og kan ikke besvares.</p>
</div>
</body>
</html>
EOT;

	//$mail_content = "This is a MIME encoded message.";
	//$mail_content .= $mail_content_plain;
	//$mail_content .= "\r\n\r\n--" . $boundary . "\r\n";
	//$mail_content .= "Content-type: text/html;charset=utf-8\r\n\r\n";
	$mail_content = $mail_content_html;
	//$mail_content .= "\r\n\r\n--" . $boundary . "--";

	$headers  = array();
	$headers[] = 'MIME-Version: 1.0';
	$headers[] = 'Content-type: text/html; charset=utf-8';
	$headers[] = "From: MGamers <noreply@mgamers.dk>";
	$headers[] = "To: $name <$email>";
	
	$result = mail ($email, "MGamers - Nulstilling af adgangskode", $mail_content ,implode("\r\n",$headers));
	
	header("Location: ./?{$_SESSION['login_return_query']}&message=reset");
	die("Password reset mail sent!");
}
elseif ($_POST['action'] == 'create')
{
	sleep(5); //Delay execution to prevent automated scripts creating accounts or scraping for valid emails
	
	//Password check
	if ($_POST['password1'] != $_POST['password2'])
	{
		header("Location: ./?{$_SESSION['login_return_query']}&name={$_POST['name']}&email=".urlencode($_POST['email'])."&error=2#create_user_modal");
		die("Password mismatch");
	}
	if (strlen($_POST['password1']) < 7)
	{
		header("Location: ./?{$_SESSION['login_return_query']}&name={$_POST['name']}&email=".urlencode($_POST['email'])."&error=3#create_user_modal");
		die("Password too short");
	}
	if ($_POST['confirmation_user'] == -1)
	{
		header("Location: ./?{$_SESSION['login_return_query']}&name={$_POST['name']}&email=".urlencode($_POST['email'])."&error=4#create_user_modal");
		die("No confirmation user selected");
	}
	
	//User check

	//Captcha check
	require_once('php/recaptchalib.php');
	$publickey = "6LdIBOkSAAAAABw8cbzCEGwfsRecgy-QUSzhTJDK";
	$privatekey = "6LdIBOkSAAAAAJuutCztMKR0xcLUVXJ4tEUVr6ou";
	if ($_POST["recaptcha_response_field"]) {
		$resp = recaptcha_check_answer ($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
		if ($resp->is_valid) {
			
			//Check email already registered (done after captcha check to prevent automated retrieval of valid emails)
			$mail_check_stmt = $db->prepare("SELECT `id` FROM `users` WHERE `email` = :email LIMIT 1;");
			$mail_check_stmt->bindParam(':email',$_POST['email'],PDO::PARAM_STR);
			$mail_check_stmt->execute();
			if ($mail_check_stmt->fetch() != null)
			{
				header("Location: ./?{$_SESSION['login_return_query']}&name={$_POST['name']}&email=".urlencode($_POST['email'])."&error=1#create_user_modal");
				die("Email already registered");
			}
			
			//Actually create user
			$hash = password_hash($_POST['password1'], PASSWORD_DEFAULT);
			$confirmation_user = getPOST('confirmation_user','/^\d+$/',1);
			
			$create_stmt = $db->prepare("INSERT INTO `users` (`email`,`password`,`name`,`active`,`confirmation_user`,`registration_date`) VALUES (:email,:hash,:name,0,:conf,:reg_date);");
			$create_stmt->bindParam(':email',$_POST['email'],PDO::PARAM_STR);
			$create_stmt->bindParam(':name',$_POST['name'],PDO::PARAM_STR);
			$create_stmt->bindParam(':hash',$hash,PDO::PARAM_STR);
			$create_stmt->bindParam(':conf',$confirmation_user,PDO::PARAM_INT);
			$reg_date = date("Y-m-d H:i:s");
			$create_stmt->bindParam(':reg_date',$reg_date,PDO::PARAM_STR);
			$create_stmt->execute();
			// $systemStm = $db->prepare("INSERT INTO `systemInfo` (`userid`) VALUES (:uID)");
			// $systemStm->bindParam(':uID', $uID, PDO::PARAM_INT);
			// $uID = $db->lastInsertID('id');
			// $systemStm->execute();
			
			header("Location: ./?{$_SESSION['login_return_query']}&message=create");
			die("User created");
			
		} else {
			//Fail
			header("Location: ./?{$_SESSION['login_return_query']}&name={$_POST['name']}&email=".urlencode($_POST['email'])."&error={$resp->error}#create_user_modal");
			die("CAPTCHA error");
		}
	}
}

?>
