-- phpMyAdmin SQL Dump
-- version 3.1.5
-- http://www.phpmyadmin.net
--
-- Vært: mysql1061.servage.net
-- Genereringstid: 24. 09 2013 kl. 21:36:53
-- Serverversion: 5.0.85
-- PHP-version: 5.2.42-servage26

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rasmusgreve2`
--
CREATE DATABASE `rasmusgreve2` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `rasmusgreve2`;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(10) NOT NULL auto_increment,
  `start` datetime NOT NULL,
	`end` datetime NOT NULL,
	`location` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `rows` tinyint(4) NOT NULL,
	`legacy_attendance` int(10),
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `file_folders`
--

CREATE TABLE IF NOT EXISTS `file_folders` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(80) NOT NULL,
  `privacy` enum('all','admin') NOT NULL,
  `folder` int(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `id` int(10) NOT NULL auto_increment,
  `path` text NOT NULL,
  `owner` int(10) NOT NULL,
  `folder` int(10) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=349 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `galleries`
--

CREATE TABLE IF NOT EXISTS `galleries` (
  `id` int(10) NOT NULL auto_increment,
  `year` smallint(4) NOT NULL,
  `month` enum('1','2','3','4','5','6','7','8','9','10','11','12') NOT NULL,
  `date` enum('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31') NOT NULL,
  `folder` varchar(64) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `id` int(10) NOT NULL auto_increment,
  `time` int(10) NOT NULL,
  `user` int(10) NOT NULL,
  `action` text NOT NULL,
  `img` varchar(50) NOT NULL default 'empty.png',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1169 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(10) NOT NULL auto_increment,
  `userid` int(10) NOT NULL,
  `date` varchar(8) NOT NULL,
  `amount` int(10) NOT NULL,
  `paid` enum('yes','no') NOT NULL,
  `yeartime` enum('first','last') NOT NULL,
  `year` int(4) NOT NULL,
  `type` enum('membership','trial') NOT NULL,
  `event` int(10) NOT NULL,
  `force_created` enum('yes','no') NOT NULL default 'no',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=183 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `seatreservations`
--

CREATE TABLE IF NOT EXISTS `reservations` (
  `id` int(10) NOT NULL auto_increment,
  `event` int(10) NOT NULL,
  `user` int(10) NOT NULL,
  `table` int(1) NOT NULL,
  `seat` int(10) NOT NULL,
  `side` enum('left','right') NOT NULL,
	`size` varchar(10),
  `nights` varchar(10),
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1221 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `shouts`
--

CREATE TABLE IF NOT EXISTS `shouts` (
  `id` int(10) NOT NULL auto_increment,
  `author` int(10) NOT NULL,
  `text` text NOT NULL,
  `time` int(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=155 ;

CREATE TABLE IF NOT EXISTS `special_pages` (
	`id` int(10) NOT NULL auto_increment,
	`content` text NOT NULL,
	`date` date NOT NULL,
	`user` int(10) NOT NULL,
	`filename` varchar(80) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=155 ;

CREATE TABLE IF NOT EXISTS `news` (
	`id` int(10) NOT NULL auto_increment,
	`created` date NOT NULL,
	`author` int(10) NOT NULL,
	`title` varchar(80) NOT NULL,
	`content` text NOT NULL,
	`category` int(10) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=MYISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=155 ;

CREATE TABLE IF NOT EXISTS `category` (
	`id` int(10) NOT NULL auto_increment,
	`name` varchar(80) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=155 ;

CREATE TABLE IF NOT EXISTS `systemInfo` (
	`id` int(10) NOT NULL auto_increment,
	`name` varchar(80) NOT NULL,
	`CPU` varchar(80),
	`GPU` varchar(80),
	`PSU` varchar(80),
	`motherboard` varchar(80),
	`RAM` varchar(80),
	`memory` varchar(80),
	`monitor` varchar(80),
	`mouse` varchar(80),
	`keyboard` varchar(80),
	`audio` varchar(80),
	`userid` int(10) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=MYISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=155 ;


-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL auto_increment,
  `email` varchar(80) NOT NULL,
  `password` varchar(255) NOT NULL,
  `reset_password` varchar(64),
	`reset_time` datetime,
  `name` varchar(80) NOT NULL,
	`active` int(1) NOT NULL,
	`confirmation_user` int(10),
	`registration_date` date NOT NULL,
  `nick` varchar(50),
  `phone` varchar(8),
  `address` text,
  `zip` varchar(4),
  `receive_notifications` enum('yes','no'),
  `type` enum('normal','admin','guest') DEFAULT 'normal',
  `key` varchar(32),
  `must_pay` int(1),
  `avatar` varchar(256),
  `steam` varchar(80),
  `skype` varchar(80),
	`battlelog` varchar(80),
	`teamspeak` varchar(80),
	`origin` varchar(80),
  `privacy` varchar(10),
  `specs` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=271 ;

-- data
CREATE TABLE IF NOT EXISTS `special_pages` (
	`id` int(10) NOT NULL auto_increment,
	`content` text NOT NULL,
	`date` date NOT NULL,
	`user` int(10) NOT NULL,
	`filename` varchar(80) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=155 ;
INSERT INTO `special_pages` (`content`, `date`, `user`, `filename`) 
VALUES ('', NOW(), 0, 'frontpage');

