<?php
header ("Content-type: image/png");

//Cosmetic
$padding = 25;
$bar_width = (isset($_GET['bar_width']) && is_numeric($_GET['bar_width'])) ? $_GET['bar_width'] : 32;
$bar_space = 10;
$bar_legend_angle = -50;

$bar_height = 200;
$padding_top = 10;
$legend_height = 75;

$f = "fonts";
$VERDANA = "$f/verdana.ttf";
$VERDANAB = "$f/verdanab.ttf";


/****************************************
Load data
****************************************/
$arr = $_GET['input'];
//$arr = array('11/07/14' => 9, '12/07/14' => 30, '12/07/15' => 100);

$high = -1;
foreach($arr as $v){
	$high = max($high, $v);
}

$width = (count($arr)) * ($bar_space+$bar_width) + 2 * $padding;
/****************************************
Colors
****************************************/
$im = imagecreatetruecolor($width,$bar_height + $legend_height);

$transparency = imagecolorallocatealpha($im, 0, 0, 0, 127);
imagefill($im, 0, 0, $transparency);
imagesavealpha($im, true);

$text_color = ImageColorAllocate ($im, 0, 0, 0);
$graph_color = ImageColorAllocate ($im,150,150,250);


/****************************************
Bars
****************************************/
$i = 0;
foreach($arr as $key => $value)
{
	$top = $bar_height-($value/$high)*($bar_height-$padding_top);
	$x = $i*($bar_width+$bar_space)+$padding;
	
	$c = $graph_color;
	imagefilledrectangle($im,$x,$bar_height,$x+$bar_width,$top,$c);
	
	$tx = $x + ($bar_width/2)-10;
	imagettftext($im,9,$bar_legend_angle,$tx,$bar_height+10,$text_color,$VERDANA,$key);

	$y = ($top+15 > 155) ? ($top-5) : ($top+15); //Above graph if not enough room
	$tx += 7;
	if ($value > 9) $tx -= 4; //Centering
	if ($value > 99) $tx -= 4; //Centering
	if ($value > 999) $tx -= 5; //Centering
	imagettftext($im,10,0,$tx,$y,$text_color,$VERDANA,$value);
	
	$i++;
}

/****************************************
Lines & output
****************************************/
imageline($im,0,$bar_height,$width,$bar_height,$text_color);
Imagepng ($im);
imagedestroy($im);
?>