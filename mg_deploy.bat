@echo off
echo You are deploying www/mgamers to mgamers.dk! Continue?
pause
rmdir /S /Q C:\wamp\www\temp\mgamers
mkdir C:\wamp\www\temp\mgamers
xcopy C:\wamp\www\mgamers C:\wamp\www\temp\mgamers /EXCLUDE:C:\wamp\www\mgamers\excl.txt /S
echo Copy done
echo Starting ftp tranfer...
ncftpput -f "../deploy_credentials.txt" -z -R / C:\wamp\www\temp\mgamers
rmdir /S /Q C:\wamp\www\temp\mgamers
