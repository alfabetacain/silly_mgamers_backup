<?php
session_start();
if (!isset($_SESSION['user'])) $_SESSION['user'] = '';
if (!isset($_SESSION['admin'])) $_SESSION['admin'] = false;
$_SESSION['event'] = '1';
if (isset($_POST['user']))
{
	$_SESSION['user'] = $_POST['user'];
	header("Location:./");
}
if (isset($_POST['admin']))
{
	$_SESSION['admin'] = $_POST['admin'] == 'true' ? true : false;
	header("Location:./");
}

mysql_connect("localhost","root","");
mysql_select_db("mgamers");


if (isset($_POST['rows']))
{
	$q = mysql_query("UPDATE `event` SET `rows` = '{$_POST['rows']}' WHERE `id` = '{$_SESSION['event']}';");
	header("Location:./");
}
if (isset($_POST['reservation_position']))
{
	$q_d = mysql_query("DELETE FROM `reservation` WHERE `event` = '{$_SESSION['event']}' AND `user` = '{$_SESSION['user']}' LIMIT 1;");
	$parts = explode('.',$_POST['reservation_position']);
	$table = $parts[0];
	$position = $parts[1];
	$q = mysql_query("INSERT INTO `reservation` (`user`,`event`,`table`,`position`) VALUES ('{$_SESSION['user']}','{$_SESSION['event']}','$table','$position');");
	header("Location:./");
}

$users_query = mysql_query("SELECT * FROM `user`");
$event_query = mysql_query("SELECT * FROM `event` WHERE `id` = '{$_SESSION['event']}';");
$event_assoc = mysql_fetch_assoc($event_query);

$reservations_query = mysql_query("SELECT r.*, u.name name, u.id userid FROM `reservation` r, `user` u WHERE u.`id`=r.`user` AND r.`event` = '{$_SESSION['event']}'");

$reservations = array();
while ($res = mysql_fetch_assoc($reservations_query))
{
	$reservations[$res['table'].'.'.$res['position']] = $res;
}

/*
Functions for reservation table table
*/
function createRowPair($table, $row)
{
	global $reservations;
	
	//Check for double reservations
	$double_left  = (isset($reservations[$table.'.'.($row*2+1)]) && $reservations[$table.'.'.($row*2+1)]['double'] == '1');
	$double_right = (isset($reservations[$table.'.'.($row*2+2)]) && $reservations[$table.'.'.($row*2+2)]['double'] == '1');
	//Check for room for double reservations
	$can_expand_left  = !isset($reservations[$table.'.'.($row*2+1)]) xor !isset($reservations[$table.'.'.(($row+1)*2+1)]);
	$can_expand_right = !isset($reservations[$table.'.'.($row*2+2)]) xor !isset($reservations[$table.'.'.(($row+1)*2+2)]);
	
	createTopRow($table, $row, $double_left, $double_right, $can_expand_left, $can_expand_right);
	createBottomRow($table, $row+1, $double_left, $double_right, $can_expand_left, $can_expand_right);
}

function createTopRow($table, $row, $double_left, $double_right, $can_expand_left, $can_expand_right)
{
	echo "<tr>";
		createTopLeft($table, $row, $double_left, $can_expand_left);
		createTopRight($table, $row, $double_right, $can_expand_right);
	echo "</tr>";
}

function createTopLeft($table, $row, $double, $can_expand)
{
	$class = ($double) ? 'double' : '';
	$rowspan = ($double) ? 'rowspan="2"' : '';
	$position = $table.'.'.($row*2+1);
?>
	<td class='<?=$class?>' <?=$rowspan?>>
		<?php createDropdown($position, $double, $can_expand);?>
	</td>
	<td class='seating-right <?=$class?>' <?=$rowspan?>>
		<?php createPosition($position);?>
	</td>
	<td class='table-center-left seating-right <?=$class?>' <?=$rowspan?>>
		<span class='badge'><?=($double)?'<br>'.$position.'<br>&nbsp;':$position;?></span>
	</td>
<?php
}

function createTopRight($table, $row, $double, $can_expand)
{
	$class = ($double) ? 'double' : '';
	$rowspan = ($double) ? 'rowspan="2"' : '';
	$position = $table.'.'.($row*2+2);
?>
	<td class='seating-left <?=$class?>' <?=$rowspan?>>
		<span class='badge'><?=($double)?'<br>'.$position.'<br>&nbsp;':$position;?></span>
	</td>
	<td class='seating-left <?=$class?>' <?=$rowspan?>>
		<?php createPosition($position);?>
	</td>
	<td class='<?=$class?>' <?=$rowspan?>>
		<?php createDropdown($position, $double, $can_expand);?>
	</td>
<?php
}

function createBottomRow($table, $row, $double_left, $double_right, $can_expand_left, $can_expand_right)
{
	echo "<tr>";
		if (!$double_left)
			createBottomLeft($table, $row, $can_expand_left);
		if (!$double_right)
			createBottomRight($table, $row, $can_expand_right);
	echo "</tr>";
}

function createBottomLeft($table, $row, $can_expand)
{
	$position = $table.'.'.($row*2+1);
?>
	<td>
		<?php createDropdown($position, false, $can_expand);?>
	</td>
	<td class='seating-right'>
		<?php createPosition($position);?>
	</td>
	<td class='table-center-left seating-right'>
		<span class='badge'><?=$position?></span>
	</td>
<?php
}

function createBottomRight($table, $row, $can_expand)
{
	$position = $table.'.'.($row*2+2);
?>
	<td class='seating-left'>
		<span class='badge'><?=$position?></span>
	</td>
	<td class='seating-left'>
		<?php createPosition($position);?>
	</td>
	<td>
		<?php createDropdown($position, false, $can_expand);?>
	</td>
<?php
}

function createDropdown($position, $double, $can_expand)
{
	return;
	global $reservations;
	if (!isset($reservations[$position])) return; //TODO: Reserver for andre
	
?>
	<div class="dropdown">
		<button type="button" class="btn btn-default dropdown-toggle btn-xs" data-toggle="dropdown">
			<span class="caret"></span>
		</button>
	  <ul class="dropdown-menu" role="menu">
		<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Slet</a></li>
		<?php if($double){?>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Opdel til enkeltborde</a></li>
		<?php } elseif ($can_expand){?>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Udvid til dobbeltbord</a></li>
		<?php }?>
	  </ul>
	</div>
<?php
}


function createPosition($position)
{
	global $user, $reservations;
	if (isset($reservations[$position])){
		if ($reservations[$position]['userid'] == $_SESSION['user'])
		{
			//echo "<div class='label label-primary draggable_name' data-value='{$reservations[$position]['userid']}'>{$reservations[$position]['name']}</div>";
			echo "<div class='label label-primary'>{$reservations[$position]['name']}</div>";
		}
		else
		{
			//echo "<div class='label label-default draggable_name' data-value='{$reservations[$position]['userid']}'>{$reservations[$position]['name']}</div>";
			echo "<div class='label label-default'>{$reservations[$position]['name']}</div>";
		}
	} else {
		//echo "<button class='btn btn-xs btn-default droppable_seat' name='reservation_position' value='$position'>V&aelig;lg plads</button>";
		echo "<button class='btn btn-xs btn-default' name='reservation_position' value='$position'>V&aelig;lg plads</button>";
	}
}


?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

    <title>MGamers pladsreservation</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
	
  </head>

  <body>

    <div class="container">

      <!-- Static navbar -->
      <div class="navbar navbar-default">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">MGamers</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Link</a></li>
            <li><a href="#">Link</a></li>
            <li><a href="#">Link</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="./">Default</a></li>
            <li><a href="../navbar-static-top/">Static top</a></li>
            <li><a href="../navbar-fixed-top/">Fixed top</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
	
	
	<div class='row'>
		<div class='col-md-12'>
			<div class='well'>
				<h2>Arrangmenent 28. september 2013</h2>
				<dl class="dl-horizontal">
					<dt>Starter:</dt>
					<dd><a href="http://www.google.com/calendar/event?action=TEMPLATE&text=MGamers&dates=20130928T150000Z/20130930T160000Z&details=&location=M%C3%A5l%C3%B8v%20skoles%20gymnastiksal&trp=true&sprop=&sprop=name:" target="_blank">28. september 2013 kl 17:00</a></dd>
					<dt>Slutter:</dt>
					<dd>30. september 2013 kl 17:00</dd>
					<dt>Tilmeldingsfrist:</dt>
					<dd>25. september 2013 kl 17:00</dd>
					<dt>Sted:</dt>
					<dd>M&aring;l&oslash;v skoles gymnastiksal</dd>
				</dl>
			</div>
		</div>
	</div>
		
	<div class="row">
	<?php for($table = 1; $table <= $event_assoc['tables']; $table++){?>
		<div class='col-lg-6'>
			<div class="panel panel-default">
			<div class="panel-heading"><h3 class="panel-title">Bord <?=$table?></h3></div>
			<form method='post' action=''>
			<table class='table table-table'>
			<?php 
				for($row = 0; $row < $event_assoc['rows']; $row+=2)
					createRowPair($table, $row);
			?>
			</table>
			</form>
			</div>
		</div>
	<?php } ?>
	</div>
	
	
	<?php
	$names = array();
	$names[] = "Malthe Norup";
	$names[] = "Kennet Bodilsen";
	$names[] = "Ingolf Christiansen";
	$names[] = "Emanuel Riber";
	$names[] = "Jakob Dalgaard";
	$names[] = "Fritjof Falk";
	$names[] = "Jesper Jakobsen";
	$names[] = "Loke Kaspersen";
	$names[] = "Verner Karstensen";
	$names[] = "Frode Rasmussen";
	$names[] = "Alf Solberg";
	$names[] = "Jacob Nielsen";
	$names[] = "Ingvar Steffensen";
	$names[] = "Yngve Hjort";
	$names[] = "Osvald Akselsen";
	$names[] = "Kjeld Reenberg";
	function rn() {global $names; return $names[rand(0,count($names)-1)];}
	
	?>
	
	<div class="container well">
		<h2>Overnatninger</h2>
		<div class="row">
			<?php for($day = 0; $day < 2; $day++) {?>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">Nat <?=$day+1?></div>
					<div class="panel-body">
						<?php $k = rand(2,7); for($i = 0; $i < $k; $i++) {?>
						<div class="checkbox">
							<label>
								<input type="checkbox" checked> <?=rn()?>
							</label>
						</div>
						<?php }
						for ($i = 0; $i < 10-$k; $i++) {?>
						<div class="checkbox">
							<label>
								<input type="checkbox"> <?=rn()?>
							</label>
						</div>
						<?php }?>
					</div>
				</div>
			</div>
			<?php }?>
		</div>
		<div class="row">
			<div class="col-md-12">
				<input type="submit" value="Gem overnatninger" class="btn btn-primary"/>
			</div>
		</div>
	</div>
	  

    </div> <!-- /container -->
	
	<form id='moveForm' action='http://localhost/get' method='get'>
		<input type='hidden' id='moveFormUser' name='user' value=''/>
		<input type='hidden' id='moveFormToLocation' name='tolocation' value=''/>
		<input type='hidden' name='move' value='move'/>
	</form>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <script src="js/bootstrap.min.js"></script>
	<script>
		$(".draggable_name").draggable({
			revert:'invalid', 
			opacity: 0.7, 
			helper: "clone", 
			cursor:"move"
			}
		);
		
		$("#changeNightsArea").droppable({
			accept:'.draggable_name',
			tolerance:'touch',
			activeClass:'btn-warning',
			hoverClass:'btn-danger',
			drop:function(event, ui){
				alert("Change nights " + ui.draggable.data('value'));
			}
		});
		$("#deleteReservationArea").droppable({
			accept:'.draggable_name',
			tolerance:'touch',
			activeClass:'btn-warning',
			hoverClass:'btn-danger',
			drop:function(event, ui){
				alert("Delete " + ui.draggable.data('value'));
			}
		});
		$(".droppable_seat").droppable({
			accept:'.draggable_name',
			tolerance:'touch',
			activeClass:'btn-warning',
			hoverClass:'btn-success',
			drop:function(event, ui){
				$('#moveFormUser').val(ui.draggable.data('value'));
				$('#moveFormToLocation	').val($(this).val());
				$('#moveForm').submit();
			},
			over:function(event, ui){$(this).html("Flyt hertil");},
			out:function(event, ui){$(this).html("V&aelig;lg plads");}
		});
	</script>
  </body>
</html>
