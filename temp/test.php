<html>
<head>
    <link href="css/bootstrap.css" rel="stylesheet">
</head>
<body>	
	

	<div class="container">
		<h1>Test!</h1>
		<a href='http://www.twitch.tv/mgamersdk' data-streamid='mgamersdk' data-online='Stream online' data-offline='Stream offline' class='twitch_btn'></a>
		
	</div>
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script>
		$('.twitch_btn').each(function(){
			var btn = $(this);
			btn.html("<img src='img/ajax-loader.gif'/>");
			$.getJSON("./twitch_online.php?stream=" + btn.data('streamid'), function(data) {
				btn.addClass("btn");
				if (data == "1")
				{
					btn.html(btn.data('online'));
					btn.addClass("btn-primary");
				}
				else
				{
					btn.html(btn.data('offline'));
					btn.removeClass("btn-primary");
					btn.addClass("btn-default");
					btn.addClass("disabled");
				}
			});
		});
	</script>

</body>
</html>