#!/bin/sh
HOST=$1
DBNAME=$2
DBUSER=$3
DBPASS=$4
echo "<?php" > credentials.php
echo "\$db_host = \"$HOST\";" >> credentials.php
echo "\$db_name = \"$DBNAME\";" >> credentials.php
echo "\$db_user = \"$DBUSER\";" >> credentials.php
echo "\$db_pass = \"$DBPASS\";" >> credentials.php
echo "?>" >> credentials.php

