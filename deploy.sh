#!/bin/sh
rm -rf /var/www/html/*
rsync -r -F --exclude-from excl.txt . /var/www/html/
chown -R www-data:www-data /var/www/html/
chmod -R 0700 /var/www/html/
