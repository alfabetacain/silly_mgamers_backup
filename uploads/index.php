<?php 
session_start();
$_SESSION['user_type'] = (isset($_SESSION['user_type']) && ($_SESSION['user_type'] == 'normal' || $_SESSION['user_type'] == 'admin')) ? $_SESSION['user_type'] : 'none';
$_SESSION['user_id'] = (isset($_SESSION['user_id']) && ctype_digit($_SESSION['user_id'])) ? $_SESSION['user_id'] : -1;
$_SESSION['user_name'] = (isset($_SESSION['user_name'])) ? $_SESSION['user_name'] : '';

if ($_SESSION['user_type'] == 'none') die("This feature is only for logged in users");

chdir('..');
include_once("php/util.php");
require_once("php/connectdb.php");
chdir('uploads');
$resulting_message = '';
if (isset($_POST['use_gravatar'])){
	$resulting_message = "Gravatar valgt!<br>";
	$s = $db->prepare("UPDATE `users` SET `avatar` = NULL WHERE `id` = :id LIMIT 1;");
	$s->bindParam(':id',$_SESSION['user_id'],PDO::PARAM_INT);
	$s->execute();
}
elseif (isset($_FILES['upload_file'])){
	$res = handle_avatar_upload('upload_file');
	if (is_numeric($res)){
		$resulting_message = "Fejlkode: $res<br>";
	}
	else
	{
		$resulting_message = "Uploadet OK!<br>";
		$s = $db->prepare("UPDATE `users` SET `avatar` = :avatar WHERE `id` = :id LIMIT 1;");
		$s->bindParam(':avatar',$res,PDO::PARAM_STR);
		$s->bindParam(':id',$_SESSION['user_id'],PDO::PARAM_INT);
		$s->execute();
	}
}


$stmt = $db->prepare("SELECT * FROM `users` WHERE `id` = :id LIMIT 1;");
$stmt->bindParam(':id',$_SESSION['user_id'],PDO::PARAM_INT);
$stmt->execute();
$user = $stmt->fetch();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>MGamers</title>
    <!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">	
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
	<style>
	body {margin-top:15px;}
	</style>

	
  </head>

  <body>
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
	<div class='container'>
	<div class='panel panel-default'>
		<div class='panel-heading'><h3 class='panel-title'>Upload nyt avatar</h3></div>
		<div class='panel-body'>
			<p><strong>Nuværende avatar</strong></p>
			<div class='thumbnail'>
				<?php if ($user['avatar'] != null) { ?>
					<img src='avatars/<?=$user['avatar']?>' />
				<?php } else { ?>
					<img src="https://gravatar.com/avatar/<?=md5(strtolower($user['email']))?>?s=200&amp;d=identicon" alt="<?=$user['name']?>'s avatar">
				<?php } ?>
			</div>
			<p>
				Det uploadede billede vil blive skaleret til <strong>200x200px</strong>
			</p>
			<p>
				Følgende filformater et tilladt: <strong>png, jpg.</strong>
			</p>
			<form action="./" method="post" enctype="multipart/form-data">
			<input type="hidden" name="MAX_FILE_SIZE" value="5000000000">
			<input type="file" name="upload_file" class='form-control'>
			<br>
			<label>
				<input type="checkbox" name="use_gravatar"> Brug <a href='https://gravatar.com' target='_blank'>gravatar</a> i stedet
			</label>
			<br>
			<strong class='text-center'><?=$resulting_message?></strong>
			<br>
			<input type="submit" class='btn btn-primary' value='Upload'>
			<a href='#' class='pull-right btn btn-default' onclick='window.close()'>Luk</a>
			</form>

		</div>
	</div>
	</div>

</body>
</html>
